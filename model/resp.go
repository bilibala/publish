package model


type PubTaskHttp struct {
	AccountId  int            `json:"account_id"`
	ArticleCat string             `json:"article_cat"`
	Type       string             `json:"type"`
	ArticleUrl    string 		  `json:"article_url"`
	Title 	string `json:"title"`
}

type PubResult struct {
	TaskId  int  `json:"task_id"`
	Result  string `json:"result"`
	Type    int    `json:"type"`
	Success bool   `json:"success"`
}
