package model

import (
	"time"
	_ "github.com/go-sql-driver/mysql"
)


const (
	Pub_Status_Nopub     = 0
	Pub_Status_Fail      = -1
	Pub_Status_Pubing    = 1
	Pub_Status_Pub_Start = 2
	Pub_Status_Success   = 3
	Pub_Status_Max       = 10000
)

type PubTask struct {
	Id     int  `orm:"column(id);auto" json:"id"`

	AccountId int `orm:"column(account_id)" json:"account_id"`

	ArticleCat string `orm:"column(article_cat)" json:"article_cat"`
	Article    string `orm:"column(article);type(text)" json:"article"`
	Type       string `orm:"column(type)" json:"type"`
	Tag        string `orm:"column(tag);null" json:"tag"`

	Status     int    `orm:"column(status);index" json:"status"` // 0 未执行 -1 发布失败 1 下发中 2 开始发布 3 发布成功
	Msg        string `orm:"column(msg)" json:"msg"`             // 发布结果消息
	ErrorCount int    `orm:"column(error_count)" json:"error_count"`

	CreateTime time.Time `orm:"column(create_time)" json:"create_time"`
	EndTime    time.Time `orm:"column(end_time);null" json:"end_time"`
	StatusTime time.Time `orm:"column(status_time);null" json:"-"` //状态改变时间
}


//func PubTaskToTask(pub PubTask) (*Task, error) {
//	platParams := make(map[string]string)
//	if err := json.Unmarshal([]byte(pub.PlatParams), &platParams); err != nil {
//		beego.Error(err)
//		return nil, err
//	}
//	article := baijia.FeedArticle{}
//	if err := json.Unmarshal([]byte(pub.Article), &article); err != nil {
//		beego.Error(err)
//		return nil, err
//	}
//
//	task := &Task{
//		Id:         pub.Id,
//		Plat:       pub.Plat,
//		PlatParams: platParams,
//
//		ArticleCat: pub.ArticleCat,
//		Article:    article,
//		Tag:        pub.Tag,
//		Type:       pub.Type,
//
//	}
//
//	return task, nil
//}
