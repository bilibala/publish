package pub_lib

import (
	"errors"
	"runtime/debug"
	"strings"

	"github.com/astaxie/beego"
	"publish/pub-lib/baijia"
	"publish/pub-lib/utils"
	//"fmt"
)

const (
	TYPE_NEWS    = "ARTICLE"
	TYPE_GALLERY = "GALLERY"
	TYPE_VEDIO   = "VIDEO"
)


type Task struct {
	Id int `json:"id"`
	Article baijia.FeedArticle `json:"article"`
	Type string `json:"type"`
	Category string `json:"category"`

	Plat string `json:"plat"`
	Cookies string `json:"cookies"`
	PlatParams map[string]string `json:"plat_params"`
}

func Publish(pub Task) (err error) {
	defer func() {
		if r := recover(); r != nil {
			debug.PrintStack()
			beego.Error("[recover]", r)
			err = errors.New("[recover] error")
		}
	}()

	pub.Type = strings.ToUpper(pub.Type)
	if pub.Article, err = ParseFeeds(pub.Type, pub.Article); err != nil {
		return err
	}

	beego.Info("开始发布任务:", pub)

	platform := NewPlatform(pub.Plat,pub.Cookies, pub.PlatParams)

	if err := platform.CheckCookie(); err != nil {
		beego.Error("Cookie检查：[失败] cookie 无效")
		return err
	}
	beego.Info("Cookie检查：[通过]")

	switch pub.Type {
	case TYPE_NEWS:
		err = platform.PublishArticle(pub.Category, "", pub.Article)
	case TYPE_GALLERY:
		err = platform.PublishGallery(pub.Category, "", pub.Article)
	case TYPE_VEDIO:
		err = platform.PublishVideo(pub.Category, "", pub.Article)
	}

	if err != nil {
		beego.Error("发表失败:[平台]", pub.Plat, "[类型]", pub.Type, "[标题]", pub.Article.Title)
		return err
	}

	beego.Info("发表成功:[平台]", pub.Plat, "[类型]", pub.Type, "[标题]", pub.Article.Title)
	return nil
}

func ParseFeeds(tp string, article baijia.FeedArticle) (baijia.FeedArticle, error) {
	newarticle := baijia.FeedArticle{Title: article.Title, Feed: []baijia.Feed{}}

	covers := []string{}

	for i, feed := range article.Feed {
		if err := CheckWord(feed.Data);err != nil{
			return newarticle,err
		}

		article.Feed[i].Type = strings.ToLower(feed.Type)
		if article.Feed[i].Type == "html" {
			newarticle.Feed = append(newarticle.Feed, ParseFeed(feed.Data)...)
			continue
		}

		if article.Feed[i].Type == "image" {
			article.Feed[i].Data = utils.GetRealAddr(article.Feed[i].Data)
			newarticle.Feed = append(newarticle.Feed, article.Feed[i])
			continue
		}

		if article.Feed[i].Type == "cover" {
			article.Feed[i].Data = utils.GetRealAddr(article.Feed[i].Data)
			if tp != TYPE_VEDIO {
				covers = append(covers, article.Feed[i].Data)
			} else {
				newarticle.Feed = append(newarticle.Feed, article.Feed[i])
			}
			continue
		}

		if article.Feed[i].Type == "video" {
			realAddr, err := utils.GetVideoRealAddr(article.Feed[i].Data)
			if err != nil {
				return newarticle, errors.New(`视频地址解析失败`)
			}
			article.Feed[i].Data = realAddr
			newarticle.Feed = append(newarticle.Feed, article.Feed[i])
			continue
		}

		newarticle.Feed = append(newarticle.Feed, article.Feed[i])
	}

	for _, cover := range covers {
		for j, feed := range newarticle.Feed {
			if cover == feed.Data {
				newarticle.Feed[j].Type = "cover"
			}
		}
	}

	return newarticle, nil
}

var (
	WhiteWord = beego.AppConfig.Strings("white_words")
)

func CheckWord(text string) error{
	if len(WhiteWord) < 10{
		return errors.New("没有配置敏感词汇库")
	}

	for _,w := range WhiteWord{
		if strings.Contains(text,w){
			return errors.New("含有敏感词汇:"+w)
		}
	}
	return nil
}
