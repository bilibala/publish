package baijia

/*
{
	"error_code": 20000,
	"error_msg": "preuploadVideo success",
	"upload_key": "8985a2be518dbca70e870109562e400c",
	"mediaId": "T4VxZdVxYPo\/jVUxkquPJVTv8KwlPphA"
}
*/
type PreUploadResult struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
	UploadKey string `json:"upload_key"`
	MediaId   string `json:"mediaId"`
}

/*
{
  "uploadId": "81067289cc5ef1ad1535ca3efabeed5a",
  "error_code": 20000,
  "error_msg": "1 part upload success"
}
*/

type UploadResult struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
	UploadId  string `json:"uploadId"`
}

/*
{
  "error_code": 0,
  "error_msg": "success",
  "ret": {
    "name": "1_scythe666.jpg",
    "size": 27858,
    "mime": "image\/png",
    "type": null,
    "bos_url": "http:\/\/timg01.bdimg.com\/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=a229a5249d1b76105e2fec22be80ee4c&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F6adcc6fbb4819cfa2adde410ccd44bb9.png"
  }
}
*/

type UploadFileResult struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
	Ret       struct {
		Name   string `json:"name"`
		BosUrl string `json:"bos_url"`
	} `json:"ret"`
}

/*
{
  "name": "a16e2ae6fe004ab8d19ef347db77e679.mp4",
  "app_id": "1558681573319139",
  "type": "video",
  "mime": "video",
  "size": "2737418",
  "bos_url": "{\"mediaId\":\"A4gdDlGsO3LIdGo3ZjBIYXAVqL5qNvv8\"}",
  "mediaId": "A4gdDlGsO3LIdGo3ZjBIYXAVqL5qNvv8",
  "error_code": 0
}
*/
type UploadVideoResult struct {
	ErrorCode int    `json:"error_code"`
	MediaId   string `json:"mediaId"`
}

type PublishVideoResult struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
}

type FeedArticle struct {
	Title string `json:"title"`
	Feed  []Feed `json:"feeds"`
}

type Feed struct {
	Data    string `json:"data"`
	Type    string `json:"type"`
	Desc    string `json:"desc,omitempty"`
	Index   int    `json:"-"`
	IsImage bool   `json:"-"`
}

type Feeds []Feed

func (a Feeds) Len() int {
	return len(a)
}
func (a Feeds) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
func (a Feeds) Less(i, j int) bool {
	return a[j].Index > a[i].Index
}
