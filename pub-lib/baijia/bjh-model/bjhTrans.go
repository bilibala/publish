package bjh_model

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

func ImageToHtml(src string, width int, height int) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	id := int64(math.Floor(1e8 * r.Float64()))
	tmp := `<p data-bjh-caption-id="cap-%d"><img src="%s" class="" data-w="%d" data-h="%d"></p>`
	return fmt.Sprintf(tmp, id, src, width, height)
}

func ImageToHtmlGallery(src string, desc string, width int, height int) string {
	tmp := `<img src="%s" class="" data-w="%d" data-h="%d"><p>%s</p>`
	return fmt.Sprintf(tmp, src, width, height, desc)
}

func TextToHtml(text string) string {
	tmp := `<p>%s</p>`
	return fmt.Sprintf(tmp, text)
}

func brToHtml() string {
	return `<br>`
}

func brToFeedcontentData() string {
	return `<span class=\"bjh-br\"></span><span class=\"bjh-br\"></span>`
}
