package bjh_model

import (
	"fmt"
	"net/url"
	"testing"
)

func TestArticle_urlencode(t *testing.T) {
	result := url.QueryEscape(`{"goods_info":[]}`)
	if result != `%7B%22goods_info%22%3A%5B%5D%7D` {
		t.Fatal(result)
	}
}

func TestArticle_urldecode(t *testing.T) {
	result, _ := url.QueryUnescape(`%7B%22goods_info%22%3A%5B%5D%7D`)
	if result != `{"goods_info":[]}` {
		t.Fatal(result)
	}
}

func TestImageToHtml(t *testing.T) {
	fmt.Println(ImageToHtml("http://dsad", 32, 32))
}
