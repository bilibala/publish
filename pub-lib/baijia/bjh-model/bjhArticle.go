package bjh_model

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strings"
)

const BaiJia_ARTICLE_TYPE = "news"
const BaiJia_GALLERY_TYPE = "gallery"

type Article struct {
	Type            string           `json:"type"`
	AppId           string           `json:"app_id"`
	Title           string           `json:"title"`
	Subtitle        string           `json:"subtitle"` // ""
	Content         string           `json:"content"`
	FeedCat         int              `json:"feed_cat"`          //分类
	OriginalStatus  int              `json:"original_status"`   //0
	CoverLayout     string           `json:"cover_layout"`      //auto  three
	CoverImages     []CoverImage     `json:"cover_images"`      //[]       %5B%5D
	CoverImagesMap  []CoverImagesMap `json:"_cover_images_map"` //[]      %5B%5D
	CoverAutoImages []CoverAutoImage `json:"cover_auto_images"`
	FeedContent     FeedContent      `json:"feed_content"`
	SpdInfo         string           `json:"spd_info"`         // {"goods_info":[]}    %7B%22goods_info%22%3A%5B%5D%7D
	Tag             string           `json:"tag"`              //;号分隔
	ErrorCorrection int              `json:"error_correction"` //0
}

type CoverImage struct {
	Src string `json:"src"`
}

type CoverImagesMap struct {
	Src       string `json:"src"`
	OriginSrc string `json:"origin_src"`
}

type CoverAutoImage struct {
	// "src": "http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=287d11a51fa97cd8c1f54cbca5aa4dbf&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fd2b8417ca5da817140da1a80c1a7816e.jpeg"
	Src        string `json:"src"`
	Width      int    `json:"width"`       //600
	Height     int    `json:"height"`      //600
	IsSuitable bool   `json:"is_suitable"` //true
}

type FeedContent struct {
	Type     int    `json:"type"` //1
	ImageNum int    `json:"image_num"`
	VideoNum int    `json:"video_num"` //0
	Items    []Item `json:"items"`
}

type Item struct {
	Type string      `json:"type"` // text image segment
	Data interface{} `json:"data"`
}

type ImageData struct {
	Original Original `json:"original"`
	Caption  string   `json:"caption,omitempty"`
	Desc     string   `json:"desc,omitempty"`
}

type Original struct {
	Src    string `json:"src"`
	Height string `json:"height"` // "600"
	Width  string `json:"width"`  // "600"
}

type Data struct {
	BosUrl string `json:"bos_url"`
}

type DumpImageResp struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
	Data      Data   `json:"data"`
}

func NewDefaultArticle(appId string, ty string, title string, content string, feedCat int, layout string, coverImages []CoverImage, coverImagesMap []CoverImagesMap, coverAutoImages []CoverAutoImage, feedContent FeedContent, tag string) *Article {
	artice := &Article{
		Type:            ty,
		AppId:           appId,
		Title:           title,
		Subtitle:        "",
		Content:         content,
		FeedCat:         feedCat,
		OriginalStatus:  0,
		CoverLayout:     layout,
		CoverImages:     coverImages,
		CoverImagesMap:  coverImagesMap,
		CoverAutoImages: coverAutoImages,
		FeedContent:     feedContent,
		SpdInfo:         `{"goods_info":[]}`,
		Tag:             tag,
		ErrorCorrection: 0,
	}
	return artice
}

// 文章
func (a *Article) ToUrlencoded() (string, error) {
	bCoverImages, err := json.Marshal(a.CoverImages)
	if err != nil {
		return "", err
	}

	bCoverImagesMap, err := json.Marshal(a.CoverImagesMap)
	if err != nil {
		return "", err
	}

	bCoverAutoImages, err := json.Marshal(a.CoverAutoImages)
	if err != nil {
		return "", err
	}
	bFeedContent, err := json.Marshal(a.FeedContent)
	if err != nil {
		return "", err
	}

	title := `看点：` + a.Title
	title = strings.Replace(title, "。", ",", -1)
	title = strings.Replace(title, ".", ",", -1)
	if title[len(title)-1] == ',' {
		title = title[0 : len(title)-1]
	}

	template := `type=news&app_id=%s&title=%s&subtitle=%s&content=%s&feed_cat=%d&original_status=%d&cover_layout=%s&cover_images=%s&_cover_images_map=%s&cover_auto_images=%s&feed_content=%s&spd_info=%s&tag=%s&error_correction=%d`
	encodeArticle := fmt.Sprintf(template, a.AppId, title, a.Subtitle, url.QueryEscape(a.Content), a.FeedCat, a.OriginalStatus, url.QueryEscape(a.CoverLayout), url.QueryEscape(string(bCoverImages)), url.QueryEscape(string(bCoverImagesMap)), url.QueryEscape(string(bCoverAutoImages)), url.QueryEscape(string(bFeedContent)), url.QueryEscape(a.SpdInfo), a.Tag, a.ErrorCorrection)
	return encodeArticle, nil
}

func (a *Article) ToParams() (map[string]string, error) {
	bCoverImages, err := json.Marshal(a.CoverImages)
	if err != nil {
		return nil, err
	}

	bCoverImagesMap, err := json.Marshal(a.CoverImagesMap)
	if err != nil {
		return nil, err
	}

	bCoverAutoImages, err := json.Marshal(a.CoverAutoImages)
	if err != nil {
		return nil, err
	}
	bFeedContent, err := json.Marshal(a.FeedContent)
	if err != nil {
		return nil, err
	}

	title := `看点：` + a.Title
	title = strings.Replace(title, "。", ",", -1)
	title = strings.Replace(title, ".", ",", -1)
	if title[len(title)-1] == ',' {
		title = title[0 : len(title)-1]
	}

	params := make(map[string]string)
	params["type"] = "news"
	params["app_id"] = a.AppId
	params["title"] = title
	params["subtitle"] = a.Subtitle
	params["content"] = a.Content
	params["feed_cat"] = fmt.Sprintf("%d", a.FeedCat)
	params["original_status"] = fmt.Sprintf("%d", a.OriginalStatus)
	params["cover_layout"] = string(a.CoverLayout)
	params["cover_images"] = string(bCoverImages)
	params["_cover_images_map"] = string(bCoverImagesMap)
	params["cover_auto_images"] = string(bCoverAutoImages)
	params["feed_content"] = string(bFeedContent)
	params["spd_info"] = a.SpdInfo
	params["tag"] = a.Tag
	params["error_correction"] = fmt.Sprintf("%d", a.ErrorCorrection)

	return params, nil
}

// 图集
func (a *Article) ToGalleryUrlencoded() (string, error) {
	bCoverImages, err := json.Marshal(a.CoverImages)
	if err != nil {
		return "", err
	}

	bCoverImagesMap, err := json.Marshal(a.CoverImagesMap)
	if err != nil {
		return "", err
	}

	bCoverAutoImages, err := json.Marshal(a.CoverAutoImages)
	if err != nil {
		return "", err
	}
	bFeedContent, err := json.Marshal(a.FeedContent)
	if err != nil {
		return "", err
	}

	//bFeedContentStr,_ := strconv.Unquote(string(bFeedContent))
	//bFeedContentStr := strings.Replace(string(bFeedContent),`\"`,`"`,-1)
	//bFeedContentStr = strings.Replace(bFeedContentStr,`"{`,`{`,-1)
	//bFeedContentStr = strings.Replace(bFeedContentStr,`}"`,`}`,-1)

	title := a.Title
	title = strings.Replace(title, "。", ",", -1)
	title = strings.Replace(title, ".", ",", -1)
	if title[len(title)-1] == ',' {
		title = title[0 : len(title)-1]
	}

	template := `type=gallery&app_id=%s&title=%s&subtitle=%s&goddess=2&content=%s&feed_cat=%d&cover_layout=%s&cover_images=%s&_cover_images_map=%s&cover_auto_images=%s&feed_content=%s`
	encodeArticle := fmt.Sprintf(template, a.AppId, title, a.Subtitle, url.QueryEscape(a.Content), a.FeedCat, url.QueryEscape(a.CoverLayout), url.QueryEscape(string(bCoverImages)), url.QueryEscape(string(bCoverImagesMap)), url.QueryEscape(string(bCoverAutoImages)), url.QueryEscape(string(bFeedContent)))
	return encodeArticle, nil
}

type ErrorResponse struct {
	Errno  int    `json:"errno"`
	Errmsg string `json:"errmsg"`
}

/*
  feedcat  分类信息

  {
  "errno": 0,
  "errmsg": "success",
  "ret": [
    {
      "id": "125",
      "cateid": "1",
      "name": "国际"
    },
    {
      "id": "127",
      "cateid": "3",
      "name": "体育"
    },
    {
      "id": "128",
      "cateid": "4",
      "name": "娱乐"
    },
    {
      "id": "129",
      "cateid": "5",
      "name": "社会"
    },
    {
      "id": "130",
      "cateid": "6",
      "name": "财经"
    },
    {
      "id": "131",
      "cateid": "7",
      "name": "互联网"
    },
    {
      "id": "132",
      "cateid": "8",
      "name": "科技"
    },
    {
      "id": "133",
      "cateid": "9",
      "name": "房产"
    },
    {
      "id": "134",
      "cateid": "10",
      "name": "汽车"
    },
    {
      "id": "135",
      "cateid": "11",
      "name": "教育"
    },
    {
      "id": "136",
      "cateid": "12",
      "name": "时尚"
    },
    {
      "id": "137",
      "cateid": "13",
      "name": "游戏"
    },
    {
      "id": "138",
      "cateid": "14",
      "name": "军事"
    },
    {
      "id": "139",
      "cateid": "15",
      "name": "旅游"
    },
    {
      "id": "140",
      "cateid": "16",
      "name": "生活"
    },
    {
      "id": "142",
      "cateid": "18",
      "name": "创意"
    },
    {
      "id": "143",
      "cateid": "19",
      "name": "搞笑"
    },
    {
      "id": "144",
      "cateid": "20",
      "name": "美图"
    },
    {
      "id": "145",
      "cateid": "21",
      "name": "女人"
    },
    {
      "id": "146",
      "cateid": "22",
      "name": "美食"
    },
    {
      "id": "147",
      "cateid": "23",
      "name": "家居"
    },
    {
      "id": "148",
      "cateid": "24",
      "name": "健康"
    },
    {
      "id": "149",
      "cateid": "25",
      "name": "两性"
    },
    {
      "id": "150",
      "cateid": "26",
      "name": "情感"
    },
    {
      "id": "152",
      "cateid": "28",
      "name": "育儿"
    },
    {
      "id": "153",
      "cateid": "29",
      "name": "文化"
    },
    {
      "id": "154",
      "cateid": "30",
      "name": "历史"
    },
    {
      "id": "155",
      "cateid": "31",
      "name": "宠物"
    },
    {
      "id": "156",
      "cateid": "32",
      "name": "科学"
    },
    {
      "id": "157",
      "cateid": "33",
      "name": "动漫"
    },
    {
      "id": "158",
      "cateid": "34",
      "name": "职场"
    },
    {
      "id": "159",
      "cateid": "35",
      "name": "三农"
    },
    {
      "id": "161",
      "cateid": "37",
      "name": "悦读"
    },
    {
      "id": "198",
      "cateid": "38",
      "name": "辟谣"
    }
  ]
}
*/
