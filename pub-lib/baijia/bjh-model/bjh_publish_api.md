#百家号接口

## 自动裁剪图片

request 

```
Request URL:http://baijiahao.baidu.com/builder/author/article/autocutpic
Request Method:POST
Status Code:200 OK
Remote Address:202.108.23.182:80
Referrer Policy:no-referrer-when-downgrade

Accept:*/*
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Connection:keep-alive
Content-Length:309
Content-Type:application/x-www-form-urlencoded; charset=UTF-8
Cookie:BAIDUID=81C7090442B72B5FAF29654684588E6C:FG=1; BDUSS=lB6MVFxM0hMZmJkcXU5ZGZUUTZ4RE9VZ2NvNVc2bEJ6NE9GVGRpRWVPMVcxLWxaTVFBQUFBJCQAAAAAAAAAAAEAAAC8o6zAMTczMzU4OTI2NzVhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFZKwllWSsJZR; FP_UID=4853208d4a610b41b927a9f3c6091369; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1509677773; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1509685023
Host:baijiahao.baidu.com
Origin:http://baijiahao.baidu.com
Referer:http://baijiahao.baidu.com/builder/article/edit?type=news
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
X-DevTools-Request-Id:34647.228
X-Requested-With:XMLHttpRequest

Form Data
image=http%3A%2F%2Ftimg01.bdimg.com%2Ftimg%3Fpacompress%26imgtype%3D1%26sec%3D1439619614%26autorotate%3D1%26di%3Dfb0dc5ed9463dabf685165e699ecda14%26quality%3D90%26size%3Db870_10000%26src%3Dhttp%253A%252F%252Fbos.nj.bpc.baidu.com%252Fv1%252Fmediaspot%252F0fe0cab3455622ffaf17eb105262e304.jpeg&article_type=news
Name

{errno: 20011005, errmsg: "自动裁图失败，请使用默认尺寸"}

```

## 裁剪图片

```
Request URL:http://baijiahao.baidu.com/builderinner/api/content/file/cuttingPic
Request Method:POST
Status Code:200 OK
Remote Address:202.108.23.182:80
Referrer Policy:no-referrer-when-downgrade

Accept:*/*
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Connection:keep-alive
Content-Length:309
Content-Type:application/x-www-form-urlencoded; charset=UTF-8
Cookie:BAIDUID=81C7090442B72B5FAF29654684588E6C:FG=1; BDUSS=lB6MVFxM0hMZmJkcXU5ZGZUUTZ4RE9VZ2NvNVc2bEJ6NE9GVGRpRWVPMVcxLWxaTVFBQUFBJCQAAAAAAAAAAAEAAAC8o6zAMTczMzU4OTI2NzVhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFZKwllWSsJZR; FP_UID=4853208d4a610b41b927a9f3c6091369; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1509677773; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1509685023
Host:baijiahao.baidu.com
Origin:http://baijiahao.baidu.com
Referer:http://baijiahao.baidu.com/builder/article/edit?type=news
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
X-DevTools-Request-Id:34647.234
X-Requested-With:XMLHttpRequest

x:0
y:6
w:519
h:348
src:http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=fb0dc5ed9463dabf685165e699ecda14&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F0fe0cab3455622ffaf17eb105262e304.jpeg

x=0&y=6&w=519&h=348&src=http%3A%2F%2Ftimg01.bdimg.com%2Ftimg%3Fpacompress%26imgtype%3D1%26sec%3D1439619614%26autorotate%3D1%26di%3Dfb0dc5ed9463dabf685165e699ecda14%26quality%3D90%26size%3Db870_10000%26src%3Dhttp%253A%252F%252Fbos.nj.bpc.baidu.com%252Fv1%252Fmediaspot%252F0fe0cab3455622ffaf17eb105262e304.jpeg


{
  "error_code": 0,
  "error_msg": "success",
  "data": {
    "origin_src": "http:\/\/timg01.bdimg.com\/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=fb0dc5ed9463dabf685165e699ecda14&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F0fe0cab3455622ffaf17eb105262e304.jpeg",
    "src": "http:\/\/timg01.bdimg.com\/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=9bf9e819790d618461a5f1bea8f037f2&quality=90&size=b519_10094&cut_x=0&cut_y=6&cut_w=519&cut_h=348&src=http%3A%2F%2Ftimg01.bdimg.com%2Ftimg%3Fpacompress%26imgtype%3D1%26sec%3D1439619614%26autorotate%3D1%26di%3Dfb0dc5ed9463dabf685165e699ecda14%26quality%3D90%26size%3Db870_10000%26src%3Dhttp%253A%252F%252Fbos.nj.bpc.baidu.com%252Fv1%252Fmediaspot%252F0fe0cab3455622ffaf17eb105262e304.jpeg"
  }
}

```

## 标题检查

```
Request URL:http://baijiahao.baidu.com/builderinner/api/content/titleCheck
Request Method:POST
Status Code:200 OK
Remote Address:202.108.23.182:80
Referrer Policy:no-referrer-when-downgrade

Accept:*/*
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Connection:keep-alive
Content-Length:167
Content-Type:application/x-www-form-urlencoded; charset=UTF-8
Cookie:BAIDUID=C27272844286B5E8DC674F07B9C96CAD:FG=1; BDUSS=HJodUVmcklNcGUyZ1VnRFJlRGRteUFoeWhoSkdVVW5zQzZiTVBCWGN5ODA1T2xaSVFBQUFBJCQAAAAAAAAAAAEAAAB6UuHAMTczMzU4OTI2NTNhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADRXwlk0V8JZU; FP_UID=a51e2c0d05aebda7b597262701d6dd35; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1509677447; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1509686220
Host:baijiahao.baidu.com
Origin:http://baijiahao.baidu.com
Referer:http://baijiahao.baidu.com/builder/article/edit?type=news
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
X-DevTools-Request-Id:34687.160
X-Requested-With:XMLHttpRequest

content:搞笑段子：我来自拉丁美洲，嗷呜
app_id:1577047949453554

content=%E6%90%9E%E7%AC%91%E6%AE%B5%E5%AD%90%EF%BC%9A%E6%88%91%E6%9D%A5%E8%87%AA%E6%8B%89%E4%B8%81%E7%BE%8E%E6%B4%B2%EF%BC%8C%E5%97%B7%E5%91%9C&app_id=1577047949453554

{
  "logid": 4009251022111620104,
  "errno": 0,
  "result": {
    "jiucuo": {
      "feed_content": [
        
      ]
    },
    "biaotidang": {
      "clickbait": 0
    }
  },
  "errmsg": "ok"
}

```

## 图片上传

```
Request URL:http://baijiahao.baidu.com/builderinner/api/content/file/image/dump?usage=content&is_waterlog=1&url=https%3A%2F%2Fp3.pstatp.com%2Flarge%2F435500043c70d30f3d57
Request Method:GET
Status Code:200 OK
Remote Address:202.108.23.182:80
Referrer Policy:no-referrer-when-downgrade

Accept:application/json, text/javascript, */*; q=0.01
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Connection:keep-alive
Cookie:BAIDUID=C27272844286B5E8DC674F07B9C96CAD:FG=1; BDUSS=HJodUVmcklNcGUyZ1VnRFJlRGRteUFoeWhoSkdVVW5zQzZiTVBCWGN5ODA1T2xaSVFBQUFBJCQAAAAAAAAAAAEAAAB6UuHAMTczMzU4OTI2NTNhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADRXwlk0V8JZU; FP_UID=a51e2c0d05aebda7b597262701d6dd35; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1509677447; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1509686220
Host:baijiahao.baidu.com
Referer:http://baijiahao.baidu.com/builder/article/edit?type=news
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
X-DevTools-Request-Id:34687.172
X-Requested-With:XMLHttpRequest

url Query String Parameters
usage=content&is_waterlog=1&url=https%3A%2F%2Fp3.pstatp.com%2Flarge%2F435500043c70d30f3d57

{
  "error_code": 0,
  "error_msg": "success",
  "data": {
    "bos_url": "http:\/\/timg01.bdimg.com\/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=287d11a51fa97cd8c1f54cbca5aa4dbf&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fd2b8417ca5da817140da1a80c1a7816e.jpeg"
  }
}
```

## 发布文章

``` 
Request URL:http://baijiahao.baidu.com/builder/author/article/publish
Request Method:POST
Status Code:200 OK
Remote Address:202.108.23.182:80
Referrer Policy:no-referrer-when-downgrade

Accept:application/json, text/javascript, */*; q=0.01
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Connection:keep-alive
Content-Length:14910
Content-Type:application/x-www-form-urlencoded; charset=UTF-8
Cookie:BAIDUID=C27272844286B5E8DC674F07B9C96CAD:FG=1; BDUSS=HJodUVmcklNcGUyZ1VnRFJlRGRteUFoeWhoSkdVVW5zQzZiTVBCWGN5ODA1T2xaSVFBQUFBJCQAAAAAAAAAAAEAAAB6UuHAMTczMzU4OTI2NTNhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADRXwlk0V8JZU; FP_UID=a51e2c0d05aebda7b597262701d6dd35; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1509677447; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1509686220
Host:baijiahao.baidu.com
Origin:http://baijiahao.baidu.com
Referer:http://baijiahao.baidu.com/builder/article/edit?type=news
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
X-DevTools-Request-Id:34687.213
X-Requested-With:XMLHttpRequest

格式 ：application/x-www-form-urlencoded
type:news
app_id:1577047949453554
title:搞笑段子：我来自拉丁美洲，嗷呜
subtitle:
content:<p data-bjh-caption-id="cap-36597010"><img src="http://timg01.bdimg.com/timg?pacompress&amp;imgtype=1&amp;sec=1439619614&amp;autorotate=1&amp;di=287d11a51fa97cd8c1f54cbca5aa4dbf&amp;quality=90&amp;size=b870_10000&amp;src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fd2b8417ca5da817140da1a80c1a7816e.jpeg" class="" data-w="600" data-h="600"></p><p>在淘宝上一件商品的一条评价：“我穿着这件新买的斗篷去面包店，因为冷，把胳膊缩在里面。面包店老板以为我是失去双臂的残疾人，坚决不收钱，而且很贴心地把面包袋挂在我的脖子上…为了不让店主失望，我用头顶开了门，走了出来…”</p><p data-bjh-caption-id="cap-30872913"><img src="http://timg01.bdimg.com/timg?pacompress&amp;imgtype=1&amp;sec=1439619614&amp;autorotate=1&amp;di=4848b77829025de3fdc27f2fd222e3c0&amp;quality=90&amp;size=b870_10000&amp;src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fec7cb2108f6cccb83561b7a8f16a19e5.jpeg" class="" data-w="600" data-h="600"></p><p>夫妻俩测试儿子将来的从业意向，</p><p>在客厅放了四样东西：书 （科学家）、钞票（经商）、美女照 （好色）、茅台酒 （嗜酒），隐门后窥视。儿子放学回家，环顾四周，确认无人后，将钞票夹入书本，揣上美人照，拎起酒瓶，若无其事地进入自己的房间。</p><p>夫妇惊叹：这孩子完了...</p><p data-bjh-caption-id="cap-67255051"><img src="http://timg01.bdimg.com/timg?pacompress&amp;imgtype=1&amp;sec=1439619614&amp;autorotate=1&amp;di=1db2e5f0b120e677db7eb409272ffd85&amp;quality=90&amp;size=b870_10000&amp;src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F5fb9b09876df253e9b27b5e70bcb4dac.jpeg" class="" data-w="600" data-h="600"></p><p>我表弟23岁了，但还没交女朋友，一次在亲戚聚会上，我们问他对女孩子有什么要求，提出来我们也给他物色一个，他还没开口，他妈就跟我们说，他要找空姐，我们说你小子要求还蛮高哟，这时我表妹说了一句经典的：日本的吧…</p><p data-bjh-caption-id="cap-44060225"><img src="http://timg01.bdimg.com/timg?pacompress&amp;imgtype=1&amp;sec=1439619614&amp;autorotate=1&amp;di=5689fe40516cd1a5ff326d7be8a7372e&amp;quality=90&amp;size=b870_10000&amp;src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F6850cc97b8d1a304ea0fea4ef7a8477e.jpeg" class="" data-w="600" data-h="600"></p><p>听哥们儿说他的女友是通过玩游戏认识的，我立刻付诸行动，开始玩游戏！</p><p>昨天哥们儿来家里玩，碰见我玩游戏。</p><p>哥们儿：上瘾了？</p><p>我：啥呀？我还不是为找女朋友！</p><p>哥们儿看了看我的游戏后直摇头：你玩的是单机游戏，是交不到女友的</p><p><br></p>
feed_cat:19
original_status:0
cover_layout:auto
cover_images:[]
_cover_images_map:[]
cover_auto_images:[{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=287d11a51fa97cd8c1f54cbca5aa4dbf&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fd2b8417ca5da817140da1a80c1a7816e.jpeg","width":600,"height":600,"is_suitable":true},{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=4848b77829025de3fdc27f2fd222e3c0&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fec7cb2108f6cccb83561b7a8f16a19e5.jpeg","width":600,"height":600,"is_suitable":true},{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=1db2e5f0b120e677db7eb409272ffd85&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F5fb9b09876df253e9b27b5e70bcb4dac.jpeg","width":600,"height":600,"is_suitable":true},{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=5689fe40516cd1a5ff326d7be8a7372e&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F6850cc97b8d1a304ea0fea4ef7a8477e.jpeg","width":600,"height":600,"is_suitable":true}]
feed_content:{"type":1,"image_num":4,"video_num":0,"items":[{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=287d11a51fa97cd8c1f54cbca5aa4dbf&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fd2b8417ca5da817140da1a80c1a7816e.jpeg","height":"600","width":"600"},"caption":""}},{"type":"segment"},{"type":"text","data":"在淘宝上一件商品的一条评价：“我穿着这件新买的斗篷去面包店，因为冷，把胳膊缩在里面。面包店老板以为我是失去双臂的残疾人，坚决不收钱，而且很贴心地把面包袋挂在我的脖子上…为了不让店主失望，我用头顶开了门，走了出来…”"},{"type":"segment"},{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=4848b77829025de3fdc27f2fd222e3c0&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Fec7cb2108f6cccb83561b7a8f16a19e5.jpeg","height":"600","width":"600"},"caption":""}},{"type":"segment"},{"type":"text","data":"夫妻俩测试儿子将来的从业意向，"},{"type":"segment"},{"type":"text","data":"在客厅放了四样东西：书 （科学家）、钞票（经商）、美女照 （好色）、茅台酒 （嗜酒），隐门后窥视。儿子放学回家，环顾四周，确认无人后，将钞票夹入书本，揣上美人照，拎起酒瓶，若无其事地进入自己的房间。"},{"type":"segment"},{"type":"text","data":"夫妇惊叹：这孩子完了..."},{"type":"segment"},{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=1db2e5f0b120e677db7eb409272ffd85&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F5fb9b09876df253e9b27b5e70bcb4dac.jpeg","height":"600","width":"600"},"caption":""}},{"type":"segment"},{"type":"text","data":"我表弟23岁了，但还没交女朋友，一次在亲戚聚会上，我们问他对女孩子有什么要求，提出来我们也给他物色一个，他还没开口，他妈就跟我们说，他要找空姐，我们说你小子要求还蛮高哟，这时我表妹说了一句经典的：日本的吧…"},{"type":"segment"},{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=5689fe40516cd1a5ff326d7be8a7372e&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F6850cc97b8d1a304ea0fea4ef7a8477e.jpeg","height":"600","width":"600"},"caption":""}},{"type":"segment"},{"type":"text","data":"听哥们儿说他的女友是通过玩游戏认识的，我立刻付诸行动，开始玩游戏！"},{"type":"segment"},{"type":"text","data":"昨天哥们儿来家里玩，碰见我玩游戏。"},{"type":"segment"},{"type":"text","data":"哥们儿：上瘾了？"},{"type":"segment"},{"type":"text","data":"我：啥呀？我还不是为找女朋友！"},{"type":"segment"},{"type":"text","data":"哥们儿看了看我的游戏后直摇头：你玩的是单机游戏，是交不到女友的"},{"type":"segment"},{"type":"text","data":"<span class=\"bjh-br\"></span><span class=\"bjh-br\"></span>"},{"type":"segment"}]}
spd_info:{"goods_info":[]}
tag:老虎
error_correction:0

返回
{
  "errno": 0,
  "errmsg": "success",
  "ret": {
    "id": "1583564881932506538",
    "app_id": "1577048634072775",
    "type": "news",
    "publish_type": "immediate",
    "publish_at": "2017-11-09 13:25:56",
    "source_type": "creation",
    "source_reprinted_allow": "0",
    "source_reprinted_astrict": "all",
    "source_name": "百家号",
    "url": "http:\/\/baijiahao.baidu.com\/builder\/preview\/s?id=1583564881932506538",
    "origin_id": "0",
    "origin_app_id": "0",
    "origin_url": "",
    "title": "雷军：我比马云早10年还勤奋，为什么不如马云？2005年才想明白",
    "subtitle": "",
    "abstract": "说到互联网，雷军是不得不提的大佬，在做小米前，雷军已经非常成功。他1988年创办金山，是最早做IT的一批人，雷军自称是爷爷级的公司。但雷军一直有一个心结，自己比马云勤奋，做互联网比马云早，为什么比不过",
    "cover_layout": "one",
    "cover_images": "[{\"src\":\"http:\\\/\\\/timg01.bdimg.com\\\/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=cef4d322ed90aa121e17e6395bb21164&quality=100&size=b600_402&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F93f49fa345fb1c24690445fe7f8f0ad8.jpeg\"}]",
    "content": "http:\/\/bos.nj.bpc-internal.baidu.com\/v1\/mediaspot\/2ac9ff945acc7c7e34d3e8615ccac0a9.html",
    "file_name": "2ac9ff945acc7c7e34d3e8615ccac0a9",
    "tag": "",
    "domain": "美食",
    "status": "analyze",
    "issue_status": "1",
    "midway_issue_status": "0",
    "bad_status": "",
    "is_tagged": "0",
    "is_published": "1",
    "open_comment": "1",
    "open_front_page": "0",
    "is_reported": "0",
    "feed_id": "",
    "feed_rs": "",
    "feed_content": "http:\/\/bos.nj.bpc-internal.baidu.com\/v1\/mediaspot\/2ac9ff945acc7c7e34d3e8615ccac0a9feed.json",
    "feed_cat": "19",
    "feed_sub_cat": "-1",
    "site": "",
    "feed_push_amount": "0",
    "created_at": "2017-11-09 13:25:56",
    "updated_at": "2017-11-09 13:25:57",
    "deleted_at": "0000-00-00 00:00:00",
    "publish_time": "2017-11-09 13:25:56",
    "weixin_import_type": "0",
    "platform_source": "0",
    "tp_id": "0",
    "tag_detail": "",
    "shoubai_c_articleid": "1583564881932506538",
    "video_tag": "{}",
    "create_ip": "2086286670",
    "is_good": "0",
    "audit_msg": "",
    "antispam_code": "0",
    "original_status": "0",
    "ext_info": "{\"feed\":{\"auto_cover\":1,\"audit_type\":\"00000000\",\"mthvideo\":0}}",
    "beforeStatus": "analyze",
    "syncUpdateRes": "succ"
  },
  "ie": "utf-8"
}


```


```
type:gallery
app_id:1577047949453554
title:那些最美的设计产品
subtitle:
goddess:2
feed_cat:18
cover_layout:auto
cover_images:[]
_cover_images_map:[]
cover_auto_images:[{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=15b4b3a2f681cd95c1d3719f685aef3d&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F66521eecd4bf38fa4f30689979875825.jpeg","width":"800","height":"450","is_suitable":true},{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=8bded1cb4baf9d2715111393fffe02bd&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Ff00069688c96a3a5ffef6a777cb138ba.jpeg","width":"800","height":"450","is_suitable":true},{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=820662c7e3dbca42e75fa69cf2648992&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F0d78c815815741456e122311194ae7ac.png","width":"870","height":"574","is_suitable":true},{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=9f2803feb86ddeb4239e6a381acfd9ac&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F4bbe2fbd0eb8b416a396dae7afc97be0.png","width":"640","height":"797","is_suitable":true}]
feed_content:{"type":4,"image_num":4,"video_num":0,"items":[{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=15b4b3a2f681cd95c1d3719f685aef3d&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F66521eecd4bf38fa4f30689979875825.jpeg","height":"450","width":"800"},"desc":"一个手机APP UI设计"}},{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=8bded1cb4baf9d2715111393fffe02bd&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Ff00069688c96a3a5ffef6a777cb138ba.jpeg","height":"450","width":"800"},"desc":"一个平板APP UI设计"}},{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=820662c7e3dbca42e75fa69cf2648992&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F0d78c815815741456e122311194ae7ac.png","height":"574","width":"870"},"desc":"一个桌面 UI 设计"}},{"type":"image","data":{"original":{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=9f2803feb86ddeb4239e6a381acfd9ac&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F4bbe2fbd0eb8b416a396dae7afc97be0.png","height":"797","width":"640"},"desc":"一系列 UI 设计 "}}]}
content:<img src="http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=15b4b3a2f681cd95c1d3719f685aef3d&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F66521eecd4bf38fa4f30689979875825.jpeg" data-w="800" data-h="450" /><p>一个手机APP UI设计</p><img src="http://timg01.bdimg.com/timg?pacompress&imgtype=1&sec=1439619614&autorotate=1&di=8bded1cb4baf9d2715111393fffe02bd&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2Ff00069688c96a3a5ffef6a777cb138ba.jpeg" data-w="800" data-h="450" /><p>一个平板APP UI设计</p><img src="http://timg01.bdimg.com/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=820662c7e3dbca42e75fa69cf2648992&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F0d78c815815741456e122311194ae7ac.png" data-w="870" data-h="574" /><p>一个桌面 UI 设计</p><img src="http://timg01.bdimg.com/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=9f2803feb86ddeb4239e6a381acfd9ac&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F4bbe2fbd0eb8b416a396dae7afc97be0.png" data-w="640" data-h="797" /><p>一系列 UI 设计 </p>
```

# 视频上传
```
视频分类
Request URL:http://baijiahao.baidu.com/builder/author/video/videoCategory
Request Method:GET
{"errno":0,"errmsg":"success","ret":[{"id":1001,"name":"\u5f71\u89c6","child":[{"id":2000,"name":"\u7231\u60c5"},{"id":2001,"name":"\u52a8\u4f5c"},{"id":2002,"name":"\u6b66\u4fa0"},{"id":2003,"name":"\u529f\u592b"},{"id":2004,"name":"\u72af\u7f6a"},{"id":2005,"name":"\u641e\u7b11"},{"id":2006,"name":"\u5bb6\u5ead"},{"id":2007,"name":"\u79d1\u5e7b"},{"id":2008,"name":"\u50f5\u5c38"},{"id":2009,"name":"\u6050\u6016\u60ca\u609a"},{"id":2010,"name":"\u4f26\u7406"},{"id":2011,"name":"\u5947\u5e7b\u9b54\u5e7b"},{"id":2012,"name":"\u795e\u8bdd"},{"id":2013,"name":"\u9752\u6625"},{"id":2014,"name":"\u60ac\u7591\u63a8\u7406"},{"id":2015,"name":"\u519b\u65c5"},{"id":2016,"name":"\u4e2d\u56fd\u8fd1\u4ee3\u6218\u4e89"},{"id":2017,"name":"\u4e16\u754c\u8fd1\u4ee3\u6218\u4e89"},{"id":2018,"name":"\u804c\u573a"},{"id":2019,"name":"\u5bab\u6597"},{"id":2020,"name":"\u5386\u53f2"},{"id":2021,"name":"\u5bab\u5ef7"},{"id":2022,"name":"\u7a7f\u8d8a"},{"id":2023,"name":"\u5f71\u89c6\u5468\u8fb9"}]},{"id":1002,"name":"\u5a31\u4e50","child":[{"id":2024,"name":"\u8131\u53e3\u79c0"},{"id":2025,"name":"\u76f8\u4eb2"},{"id":2026,"name":"\u6781\u9650\u6311\u6218"},{"id":2027,"name":"\u804c\u573a\u7c7b"},{"id":2028,"name":"\u6587\u7269\u9274\u5b9d"},{"id":2029,"name":"\u6587\u5316"},{"id":2030,"name":"\u76ca\u667a"},{"id":2031,"name":"\u5c11\u513f\/\u4eb2\u5b50"},{"id":2032,"name":"\u60c5\u611f\/\u4f26\u7406"},{"id":2033,"name":"\u9b54\u672f\u6742\u6280"},{"id":2034,"name":"\u76f8\u58f0\u5c0f\u54c1"},{"id":2035,"name":"\u660e\u661f\u5a31\u4e50"},{"id":2036,"name":"\u5a31\u4e50\u5468\u8fb9"}]},{"id":1003,"name":"\u6e38\u620f","child":[{"id":2037,"name":"MOBA"},{"id":2038,"name":"\u6c99\u76d2\u7c7b"},{"id":2039,"name":"\u5361\u724c\u7c7b"},{"id":2040,"name":"\u5c04\u51fb\u7c7b"},{"id":2041,"name":"\u89d2\u8272\u626e\u6f14\u7c7b"},{"id":2042,"name":"\u97f3\u4e50\u7c7b"},{"id":2043,"name":"\u52a8\u4f5c\u5192\u9669\u7c7b"},{"id":2044,"name":"\u751f\u5b58\u7c7b"},{"id":2045,"name":"\u7ecf\u8425\u7b56\u7565\u7c7b"},{"id":2046,"name":"\u4f11\u95f2\u7c7b"},{"id":2047,"name":"\u771f\u4eba\u7eb8\u724c\u7c7b"},{"id":2048,"name":"\u6e38\u620f\u8d44\u8baf"},{"id":2049,"name":"\u6e38\u620f\u5468\u8fb9"}]},{"id":1004,"name":"\u97f3\u4e50","child":[{"id":2050,"name":"\u6d41\u884c\u4e50"},{"id":2051,"name":"\u6447\u6eda\u4e50"},{"id":2052,"name":"\u6c11\u8c23"},{"id":2053,"name":"\u563b\u54c8\u4e50"},{"id":2054,"name":"\u7535\u5b50\u4e50"},{"id":2055,"name":"DJ\u821e\u66f2"},{"id":2056,"name":"\u5668\u4e50"},{"id":2057,"name":"\u97f3\u4e50\u5468\u8fb9"},{"id":2058,"name":"\u6c11\u65cf"}]},{"id":1005,"name":"\u641e\u7b11\u8da3\u5473","child":[{"id":2059,"name":"\u6bb5\u5b50\u5267"},{"id":2060,"name":"\u6076\u641e\u8da3\u73a9"},{"id":2061,"name":"\u7cd7\u4e8b\u56e7\u6599"},{"id":2062,"name":"\u9b3c\u755c"},{"id":2063,"name":"\u5410\u69fd"},{"id":2064,"name":"\u8857\u5934\u91c7\u8bbf"}]},{"id":1006,"name":"\u4f53\u80b2","child":[{"id":2065,"name":"\u7bee\u7403"},{"id":2066,"name":"\u8db3\u7403"},{"id":2067,"name":"\u4e52\u4e53\u7403"},{"id":2068,"name":"\u7f51\u7403"},{"id":2069,"name":"\u7fbd\u6bdb\u7403"},{"id":2070,"name":"\u53f0\u7403"},{"id":2071,"name":"\u6392\u7403"},{"id":2072,"name":"\u6e38\u6cf3"},{"id":2073,"name":"\u5c04\u7bad"},{"id":2074,"name":"\u7530\u5f84"},{"id":2075,"name":"\u81ea\u884c\u8f66"},{"id":2076,"name":"\u51fb\u5251"},{"id":2077,"name":"\u4f53\u64cd"},{"id":2078,"name":"\u5c04\u51fb"},{"id":2079,"name":"\u8d5b\u8247"},{"id":2080,"name":"\u4e3e\u91cd"},{"id":2081,"name":"\u4fdd\u9f84\u7403"},{"id":2082,"name":"\u68d2\u5792\u7403"},{"id":2083,"name":"\u8c61\u68cb"},{"id":2084,"name":"\u56f4\u68cb"},{"id":2085,"name":"\u9ad8\u5c14\u592b\u7403"},{"id":2086,"name":"\u8f6e\u6ed1"},{"id":2087,"name":"\u6cf0\u62f3"},{"id":2088,"name":"\u62f3\u51fb"},{"id":2089,"name":"\u640f\u51fb"},{"id":2090,"name":"\u67d4\u9053"},{"id":2091,"name":"\u8dc6\u62f3\u9053"},{"id":2092,"name":"\u6454\u89d2"},{"id":2093,"name":"\u6b66\u672f"},{"id":2094,"name":"\u957f\u677f"},{"id":2095,"name":"\u6ed1\u677f"},{"id":2096,"name":"\u6500\u5ca9"},{"id":2097,"name":"\u8dd1\u9177"},{"id":2098,"name":"\u51b2\u6d6a"},{"id":2099,"name":"\u8d5b\u8f66"},{"id":2100,"name":"\u94c1\u4eba\u4e09\u9879"},{"id":2101,"name":"\u4f53\u80b2\u8da3\u95fb"},{"id":2102,"name":"\u4f53\u80b2\u5468\u8fb9"}]},{"id":1007,"name":"\u751f\u6d3b","child":[{"id":2103,"name":"\u5065\u8eab\u8fd0\u52a8"},{"id":2104,"name":"\u751f\u6d3b\u5c0f\u6280\u5de7"},{"id":2105,"name":"\u56ed\u827a"},{"id":2106,"name":"\u5065\u5eb7\u517b\u751f"},{"id":2107,"name":"\u5a5a\u5ac1"},{"id":2108,"name":"\u4e24\u6027"},{"id":2109,"name":"\u661f\u5ea7"},{"id":2110,"name":"\u547d\u7406"},{"id":2111,"name":"\u98ce\u6c34"},{"id":2112,"name":"\u6237\u5916\u4f11\u95f2"},{"id":2113,"name":"\u5bb6\u5c45"}]},{"id":1008,"name":"\u5927\u81ea\u7136","child":[{"id":2114,"name":"\u5446\u840c"},{"id":2115,"name":"\u91ce\u6027"},{"id":2116,"name":"\u73af\u7403\u81ea\u7136"}]},{"id":1009,"name":"\u79d1\u6280","child":[{"id":2117,"name":"\u673a\u68b0"},{"id":2118,"name":"\u79d1\u6280\u5947\u8da3"},{"id":2119,"name":"\u592a\u7a7a\u63a2\u7d22"},{"id":2120,"name":"\u822a\u5929\u822a\u7a7a"},{"id":2121,"name":"\u79d1\u666e"},{"id":2122,"name":"\u4e92\u8054\u7f51"},{"id":2123,"name":"\u6570\u7801"},{"id":2124,"name":"\u4eba\u5de5\u667a\u80fd"},{"id":2125,"name":"\u79d1\u5b66\u5b9e\u9a8c"}]},{"id":1010,"name":"\u793e\u4f1a","child":[{"id":2126,"name":"\u707e\u96be\u4e8b\u6545"},{"id":2127,"name":"\u4e09\u519c"},{"id":2128,"name":"\u6c11\u751f"},{"id":2129,"name":"\u6b63\u80fd\u91cf"},{"id":2130,"name":"\u6cd5\u5236"},{"id":2131,"name":"\u5947\u95fb"},{"id":2132,"name":"\u5730\u533a\u53d1\u5c55"}]},{"id":1011,"name":"\u821e\u8e48","child":[{"id":2133,"name":"\u5e7f\u573a\u821e"},{"id":2134,"name":"\u6027\u611f\u70ed\u821e"},{"id":2135,"name":"\u4e8c\u6b21\u5143\u821e\u8e48"},{"id":2136,"name":"\u5c11\u513f\u821e\u8e48"},{"id":2137,"name":"\u827a\u672f\u821e\u8e48"},{"id":2138,"name":"\u70ab\u9177\u821e\u8e48"}]},{"id":1012,"name":"\u7f8e\u98df","child":[{"id":2139,"name":"\u820c\u5c16\u4e0a\u7684\u7f8e\u98df"},{"id":2140,"name":"\u7f8e\u5473\u98df\u8c31"},{"id":2141,"name":"\u5403\u64ad\u5927\u80c3\u738b"},{"id":2142,"name":"\u7f8e\u98df\u730e\u5947"}]},{"id":1013,"name":"\u6559\u80b2","child":[{"id":2143,"name":"\u5b66\u6821\u6559\u80b2"},{"id":2144,"name":"\u804c\u4e1a\u6559\u80b2"},{"id":2145,"name":"\u5174\u8da3\u5b66\u4e60"},{"id":2146,"name":"\u4eba\u751f\u5bfc\u5e08"},{"id":2147,"name":"\u8003\u8bd5\u5468\u8fb9"}]},{"id":1014,"name":"\u6587\u5316\u5386\u53f2","child":[{"id":2148,"name":"\u4e2d\u56fd\u6b63\u53f2"},{"id":2149,"name":"\u4e16\u754c\u6b63\u53f2"},{"id":2150,"name":"\u91ce\u53f2\u79d8\u95fb"},{"id":2151,"name":"\u6c11\u4fd7"},{"id":2152,"name":"\u6587\u5316\u827a\u672f"},{"id":2153,"name":"\u5b97\u6559"},{"id":2154,"name":"\u620f\u66f2"},{"id":2155,"name":"\u8003\u53e4"}]},{"id":1015,"name":"\u52a8\u6f2b","child":[{"id":2156,"name":"\u56fd\u4ea7\u52a8\u6f2b"},{"id":2157,"name":"\u65e5\u672c\u52a8\u6f2b"},{"id":2158,"name":"\u97e9\u56fd\u52a8\u6f2b"},{"id":2159,"name":"\u6b27\u7f8e\u52a8\u6f2b"},{"id":2160,"name":"\u52a8\u6f2b\u8d44\u8baf"}]},{"id":1016,"name":"\u519b\u4e8b","child":[{"id":2161,"name":"\u519b\u53f2"},{"id":2162,"name":"\u519b\u4e8b\u8bc4\u8bba"},{"id":2163,"name":"\u5175\u5668\u88c5\u5907"},{"id":2164,"name":"\u8bad\u7ec3\u6f14\u4e60"},{"id":2165,"name":"\u9605\u5175"},{"id":2166,"name":"\u519b\u4e8b\u79d1\u666e"},{"id":2167,"name":"\u519b\u4e8b\u5468\u8fb9"}]},{"id":1017,"name":"\u6c7d\u8f66","child":[{"id":2168,"name":"\u8f66\u8bc4"},{"id":2169,"name":"\u7528\u8f66"},{"id":2170,"name":"\u73a9\u8f66"},{"id":2171,"name":"\u8f66\u5c55"},{"id":2172,"name":"\u5ba3\u4f20\u7247"},{"id":2173,"name":"\u9a7e\u8003"},{"id":2174,"name":"\u6c7d\u8f66\u5468\u8fb9"},{"id":2175,"name":"\u5b89\u5168\u51fa\u884c"}]},{"id":1018,"name":"\u65f6\u5c1a","child":[{"id":2176,"name":"\u7f8e\u5986\u7f8e\u53d1"},{"id":2177,"name":"\u7ea2\u6bef\u79c0\u573a"},{"id":2178,"name":"\u9ad8\u7aef\u5962\u54c1"},{"id":2179,"name":"\u65f6\u5c1a\u7efc\u827a"},{"id":2180,"name":"\u98ce\u5c1a\u5927\u7247"},{"id":2181,"name":"\u6f6e\u6d41\u7a7f\u642d"},{"id":2182,"name":"\u65f6\u5c1a\u8d44\u8baf"}]},{"id":1019,"name":"\u4eb2\u5b50","child":[{"id":2183,"name":"\u840c\u5b9d"},{"id":2184,"name":"\u65e9\u6559"},{"id":2185,"name":"\u5582\u517b"},{"id":2186,"name":"\u5b55\u4ea7"},{"id":2187,"name":"\u513f\u7ae5\u65f6\u5c1a"}]},{"id":1020,"name":"\u65f6\u653f","child":[{"id":2188,"name":"\u56fd\u9645\u65f6\u653f"},{"id":2189,"name":"\u56fd\u5185\u65f6\u653f"}]},{"id":1021,"name":"\u65c5\u6e38","child":[{"id":2190,"name":"\u65c5\u884c\u653b\u7565"},{"id":2191,"name":"\u65c5\u9014\u8da3\u95fb"},{"id":2192,"name":"\u65c5\u9014\u98ce\u5149"}]},{"id":1022,"name":"\u8d22\u7ecf","child":[{"id":2193,"name":"\u6295\u8d44\u7406\u8d22"},{"id":2194,"name":"\u516c\u53f8\u7ecf\u7ba1"},{"id":2195,"name":"\u98ce\u4e91\u4eba\u7269"},{"id":2196,"name":"\u6c11\u751f\u81f4\u5bcc"},{"id":2197,"name":"\u623f\u4ea7"},{"id":2198,"name":"\u5b8f\u89c2\u7ecf\u6d4e"}]},{"id":1023,"name":"\u5176\u4ed6","child":[{"id":2199,"name":"\u5e7f\u544a"}]}],"ie":"utf-8"}

准备上传
http://rsbjh.baidu.com/builder/author/video/preuploadVideo
Method:POST
Content-Type:application/x-www-form-urlencoded; charset=UTF-8
app_id=1558681573319139
{"error_code":20000,"error_msg":"preuploadVideo success","upload_key":"a687823a13f1c6f21b009c3bf495f7f4","mediaId":"A4gdDlGsO3LIdGo3ZjBIYXAVqL5qNvv8"}


上传1
Request URL:http://rsbjh.baidu.com/builder/author/video/uploadVideo
Request Method:POST
Content-Type:multipart/form-data; boundary=----WebKitFormBoundaryDyZWr8BKfBKm78Eq

------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="app_id"

1558681573319139
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="timestamp"

1514190288999
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="id"

WU_FILE_0
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="name"

a16e2ae6fe004ab8d19ef347db77e679.mp4
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="type"

video/mp4
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="lastModifiedDate"

Mon Dec 25 2017 16:24:07 GMT+0800 (CST)
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="size"

2737418
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="chunks"

2
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="chunk"

0
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="upload_key"

a687823a13f1c6f21b009c3bf495f7f4
------WebKitFormBoundaryDyZWr8BKfBKm78Eq
Content-Disposition: form-data; name="file"; filename="a16e2ae6fe004ab8d19ef347db77e679.mp4"
Content-Type: application/octet-stream


------WebKitFormBoundaryDyZWr8BKfBKm78Eq--
{"uploadId":"83178d676bf5bc0069d97de5769d1d3c","error_code":20000,"error_msg":"1 part upload success"}

上传2
http://rsbjh.baidu.com/builder/author/video/uploadVideo

------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="app_id"

1558681573319139
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="timestamp"

1514190288999
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="id"

WU_FILE_0
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="name"

a16e2ae6fe004ab8d19ef347db77e679.mp4
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="type"

video/mp4
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="lastModifiedDate"

Mon Dec 25 2017 16:24:07 GMT+0800 (CST)
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="size"

2737418
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="chunks"

2
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="chunk"

1
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="upload_key"

a687823a13f1c6f21b009c3bf495f7f4
------WebKitFormBoundary2oVJHikCYnr60B1V
Content-Disposition: form-data; name="file"; filename="a16e2ae6fe004ab8d19ef347db77e679.mp4"
Content-Type: application/octet-stream


------WebKitFormBoundary2oVJHikCYnr60B1V--

{"uploadId":"83178d676bf5bc0069d97de5769d1d3c","error_code":20000,"error_msg":"2 part upload success"}

完成上传
http://rsbjh.baidu.com/builder/author/video/compuploadVideo
Content-Type:application/x-www-form-urlencoded; charset=UTF-8
upload_key:a687823a13f1c6f21b009c3bf495f7f4
chunks:2
name:a16e2ae6fe004ab8d19ef347db77e679.mp4
app_id:1558681573319139
size:2737418
type:video
{"name":"a16e2ae6fe004ab8d19ef347db77e679.mp4","app_id":"1558681573319139","type":"video","mime":"video","size":"2737418","bos_url":"{\"mediaId\":\"A4gdDlGsO3LIdGo3ZjBIYXAVqL5qNvv8\"}","mediaId":"A4gdDlGsO3LIdGo3ZjBIYXAVqL5qNvv8","error_code":0}


视频封面上传
Request URL:http://baijiahao.baidu.com/builderinner/api/content/file/upload?fileapi15141901321138
Request Method:POST

------WebKitFormBoundary1LQ5J9KMrCZAD1JK
Content-Disposition: form-data; name="_media"

bg.png
------WebKitFormBoundary1LQ5J9KMrCZAD1JK
Content-Disposition: form-data; name="media"; filename="bg.png"
Content-Type: image/png

------WebKitFormBoundary1LQ5J9KMrCZAD1JK--

{"error_code":0,"error_msg":"success","ret":{"name":"bg.png","size":1149457,"mime":"image\/png","type":null,"bos_url":"http:\/\/timg01.bdimg.com\/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=c940fb028f3eb5cb1ca202663690a987&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F4e4c43058b9866dd142b66c0d583c58a.png"}}

视频发布
http://baijiahao.baidu.com/builder/author/article/publish
Request Method:POST
Content-Type:application/x-www-form-urlencoded; charset=UTF-8

type:video
app_id:1558681573319139
title:今天，让小美女教你一款超精致的妆容！
subtitle:
feed_cat:1005
feed_sub_cat:2060
cover_images:[{"src":"http://timg01.bdimg.com/timg?pacompress&imgtype=0&sec=1439619614&autorotate=1&di=c940fb028f3eb5cb1ca202663690a987&quality=90&size=b870_10000&src=http%3A%2F%2Fbos.nj.bpc.baidu.com%2Fv1%2Fmediaspot%2F4e4c43058b9866dd142b66c0d583c58a.png"}]
content:[{"title":"今天，让小美女教你一款超精致的妆容！","desc":"今天，让小美女教你一款超精致的妆容！","local":1,"mediaId":"A4gdDlGsO3LIdGo3ZjBIYXAVqL5qNvv8"}]
cover_layout:one
tag:搞笑
spd_info:{"goods_info":[]}
changeSpdData:false
original_status:0

200 OK

```

### 获取分类信息
cateid 为分类ID 对应文章 feedcat字段

```
GET 
http://baijiahao.baidu.com/builderinner/api/content/inner/newscategory/get


 {
  "errno": 0,
  "errmsg": "success",
  "ret": [
    {
      "id": "125",
      "cateid": "1",
      "name": "国际"
    },
    {
      "id": "127",
      "cateid": "3",
      "name": "体育"
    },
    {
      "id": "128",
      "cateid": "4",
      "name": "娱乐"
    },
    {
      "id": "129",
      "cateid": "5",
      "name": "社会"
    },
    {
      "id": "130",
      "cateid": "6",
      "name": "财经"
    },
    {
      "id": "131",
      "cateid": "7",
      "name": "互联网"
    },
    {
      "id": "132",
      "cateid": "8",
      "name": "科技"
    },
    {
      "id": "133",
      "cateid": "9",
      "name": "房产"
    },
    {
      "id": "134",
      "cateid": "10",
      "name": "汽车"
    },
    {
      "id": "135",
      "cateid": "11",
      "name": "教育"
    },
    {
      "id": "136",
      "cateid": "12",
      "name": "时尚"
    },
    {
      "id": "137",
      "cateid": "13",
      "name": "游戏"
    },
    {
      "id": "138",
      "cateid": "14",
      "name": "军事"
    },
    {
      "id": "139",
      "cateid": "15",
      "name": "旅游"
    },
    {
      "id": "140",
      "cateid": "16",
      "name": "生活"
    },
    {
      "id": "142",
      "cateid": "18",
      "name": "创意"
    },
    {
      "id": "143",
      "cateid": "19",
      "name": "搞笑"
    },
    {
      "id": "144",
      "cateid": "20",
      "name": "美图"
    },
    {
      "id": "145",
      "cateid": "21",
      "name": "女人"
    },
    {
      "id": "146",
      "cateid": "22",
      "name": "美食"
    },
    {
      "id": "147",
      "cateid": "23",
      "name": "家居"
    },
    {
      "id": "148",
      "cateid": "24",
      "name": "健康"
    },
    {
      "id": "149",
      "cateid": "25",
      "name": "两性"
    },
    {
      "id": "150",
      "cateid": "26",
      "name": "情感"
    },
    {
      "id": "152",
      "cateid": "28",
      "name": "育儿"
    },
    {
      "id": "153",
      "cateid": "29",
      "name": "文化"
    },
    {
      "id": "154",
      "cateid": "30",
      "name": "历史"
    },
    {
      "id": "155",
      "cateid": "31",
      "name": "宠物"
    },
    {
      "id": "156",
      "cateid": "32",
      "name": "科学"
    },
    {
      "id": "157",
      "cateid": "33",
      "name": "动漫"
    },
    {
      "id": "158",
      "cateid": "34",
      "name": "职场"
    },
    {
      "id": "159",
      "cateid": "35",
      "name": "三农"
    },
    {
      "id": "161",
      "cateid": "37",
      "name": "悦读"
    },
    {
      "id": "198",
      "cateid": "38",
      "name": "辟谣"
    }
  ]
}
```






