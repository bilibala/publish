package baijia

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	"code.whyyou.me/jiyue/goutils"
	"publish/app/account/model"
	article "code.whyyou.me/yixiangguo/pub-platform/pub-lib/baijia/bjh-model"
	"code.whyyou.me/yixiangguo/pub-platform/pub-lib/utils"
	"github.com/astaxie/beego"
	"github.com/cihub/seelog"
	"github.com/lauyoume/gohttp"
	"gopkg.in/bufio.v1"
)

func TestBaijiaPlat_DumpImage(t *testing.T) {
	shuapi.InitDefault(&shuapi.ShuApiConfig{
		Host:     "http://duke.geeyue.com",
		User:     "sq001",
		Pass:     "666999",
		TokenDay: 25,
	})
	accounts, err := shuapi.Default().GetAccounts(false)
	if err != nil {
		seelog.Errorf("BjhStatistic run , GetAccounts error = %v", err)
	}

	for _, account := range accounts {
		if !account.IsLogin() {
			seelog.Infof("[BjhStatistic run], Account is not login, account_id = %d", account.Id)
			continue
		}
		p := NewAppIdBaijiaPlat(account.Appid, account.GetCookie())
		imageUrl, err := p.dumpImage("http://p1.pstatp.com/large/436500004bc5a15520da")
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println(imageUrl)
		break
	}
}

func getAccounts() ([]model.Account, error) {
	api := shuapi.NewShuApi(&shuapi.ShuApiConfig{
		Host:     "http://duke.geeyue.com",
		User:     "sq001",
		Pass:     "666999",
		TokenDay: 0,
	})

	return api.GetAccountsByTag([]int{40})
}

func TestBaijiaPlat_AccountPublish(t *testing.T) {
	accounts, err := getAccounts()
	if err != nil {
		beego.Error("getAccounts Error:", err)
		return
	}

	for _, account := range accounts {
		fmt.Println(account.Nick, account.Platform)
		if !account.IsLogin() {
			seelog.Infof("[BjhStatistic run], Account is not login, account_id = %d", account.Id)
			continue
		}

		u := fmt.Sprintf(`http://111.230.24.49:3333/article/feed?url=%s`, url.QueryEscape(`https://www.toutiao.com/a6501839603186532878/`))

		req := gohttp.New()
		req = req.Get(u)
		body, _, err := goutils.GetBodyFromReq(req, http.StatusOK)
		if err != nil {
			t.Fatal(err)
		}
		feedArticle := FeedArticle{}
		err = json.Unmarshal(body, &feedArticle)
		if err != nil {
			t.Fatal(err)
		}

		p := NewAppIdBaijiaPlat(account.Appid, account.GetCookie())

		body, err = p.PublishArticle_Http(19, "", feedArticle)
		if err != nil {
			t.Fatal(err)
		}
		res := article.ErrorResponse{}
		err = json.Unmarshal(body, &res)
		if err != nil || res.Errno > 0 {
			beego.Error("PublishByUrl Error:", res.Errmsg, err)
		} else {
			beego.Info("发布成功:", account.Username, account.Nick, u)
		}
	}
}

func TestBaijiaPlat_SimplePublish(t *testing.T) {
	url := `http://111.230.24.49:3333/article/feed?url=http://news.163.com/17/1222/01/D67MOFJ20001885B.html`
	req := gohttp.New()
	req = req.Get(url)
	body, _, err := goutils.GetBodyFromReq(req, http.StatusOK)
	if err != nil {
		t.Fatal(err)
	}
	feedArticle := FeedArticle{}
	err = json.Unmarshal(body, &feedArticle)
	if err != nil {
		t.Fatal(err)
	}

	appId := "1585378671773543"

	ck := `BAIDUID=A1244C9CAE708A6B0135DCDA7081D3D0:FG=1; BDUSS=0tsSDIwN1REeWNVWldSRTBFYzNzSU5vMGxJM3RQWnJqdS1vaXZmOVZUQTRXa2RhQVFBQUFBJCQAAAAAAAAAAAEAAADN1e7LyKbA77XExMfQqcrCNTUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADjNH1o4zR9aT; FP_UID=5d24d2c8b252b8aa8b5552c230d5bcb7`

	p := NewAppIdBaijiaPlat(appId, ck)

	body, err = p.PublishArticle_Http(19, "", feedArticle)
	if err != nil {
		t.Fatal(err)
	}
	res := article.ErrorResponse{}
	err = json.Unmarshal(body, &res)
	if err != nil || res.Errno > 0 {
		beego.Error("PublishByUrl Error:", res.Errmsg, err)
	} else {
		beego.Info("发布成功")
	}
}

func TestBaijiaPlat_SimplePublishGallery(t *testing.T) {
	url := `http://111.230.24.49:3333/article/feed?url=https://www.toutiao.com/a6492634472284946958/`
	req := gohttp.New()
	req = req.Get(url)
	body, _, err := goutils.GetBodyFromReq(req, http.StatusOK)
	if err != nil {
		t.Fatal(err)
	}
	feedArticle := &FeedArticle{}
	err = json.Unmarshal(body, feedArticle)
	if err != nil {
		t.Fatal(err)
	}

	for i, _ := range feedArticle.Feed {
		feedArticle.Feed[i].Desc = "这样的猛兽杰森·斯坦森可不是只有一台，据说他是个性能车爱好者，家里还有十余辆高性能豪车以及超跑。简直是男人中的偶像典范！"
	}

	appId := "1558678428644773"
	ck := `BAIDUID=04F8D5CE6B92AEB9C937BD9792BA07D2:FG=1; BDUSS=F5ZndDa1BCMUJZUnN4a1JIcWNEbDVRTnE4eU0xNDJKZFczYk1FYmFtTy13U05hSVFBQUFBJCQAAAAAAAAAAAEAAADTNbestqvO973M0~0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL40~Fm-NPxZb3; FP_UID=c7c9470bfb02023a83da33e2d835d5e2; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1510048649; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1513921345; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1508306811,1509416283,1509771242,1510048649; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1513393773; PMS_JT=%28%7B%22s%22%3A1511091101681%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/builder/app/message%3Fapp_id%3D1558678428644773%22%7D%29`

	p := NewAppIdBaijiaPlat(appId, ck)

	err = p.PublishGallery("电影", "", *feedArticle)
	if err != nil {
		t.Fatal(err)
	}
	res := article.ErrorResponse{}
	err = json.Unmarshal(body, &res)
	if err != nil || res.Errno > 0 {
		beego.Error("PublishByUrl Error:", res.Errmsg, err)
	} else {
		beego.Info("发布成功:")
	}
}

func TestBaijiaPlat_UploadFile(t *testing.T) {
	url := `http://avatar.csdn.net/7/0/F/1_scythe666.jpg`
	req := utils.Get(url)
	bytes, err := req.Bytes()
	if err != nil {
		t.Fatal(err)
	}

	appId := `1556753269917967`
	ck := `BAIDUID=E2208499D5D4E0AA6A210AF0C9A7E1A6:FG=1; BDUSS=ZtQjBIbGNPWW5YVXBIN01yZ09Tay1kaU5QeDVkbVVyWExza3JDcGFWWWYxaU5hSVFBQUFBJCQAAAAAAAAAAAEAAAA6OZGqtPzK88rzwujC6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9J~FkfSfxZcH; FP_UID=46bea79ac03572ceb2dc501f1cb318c5; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1511770435; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1511056232,1511058455,1511169985,1511770435; PMS_JT=%28%7B%22s%22%3A1511595014087%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/%22%7D%29; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1512640340; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1514957477`
	p := NewAppIdBaijiaPlat(appId, ck)
	fmt.Println(p.uploadFile(ioutil.NopCloser(bufio.NewBuffer(bytes)), "1_scythe666.jpg"))
}

func TestBaijiaPlat_PublishVideo(t *testing.T) {
	url := `http://aliqncdn.miaopai.com/stream/VqQ4ihF--wrwR-qNKOAIcAFm7qWJvFKdLJJgvQ___16_0_1509366257.mp4?ssig=ad69b2db833e21fc5b7c83529e754326&time_stamp=1515655083078`
	req := utils.Get(url)
	bytes, err := req.Bytes()
	if err != nil {
		t.Fatal(err)
	}
	imageUrl := `http://bsyimg1.cdn.krcom.cn/images/VqQ4ihF--wrwR-qNKOAIcAFm7qWJvFKdLJJgvQ___3s6y.jpg`
	ireq := utils.Get(imageUrl)
	imagebytes, err := ireq.Bytes()
	if err != nil {
		t.Fatal(err)
	}
	//
	//err = ioutil.WriteFile("./video.mp4", bytes, 0666)
	//if err != nil {
	//	t.Fatal(err)
	//}

	appId := `1558678428644773`
	ck := `BAIDUID=04F8D5CE6B92AEB9C937BD9792BA07D2:FG=1; BDUSS=F5ZndDa1BCMUJZUnN4a1JIcWNEbDVRTnE4eU0xNDJKZFczYk1FYmFtTy13U05hSVFBQUFBJCQAAAAAAAAAAAEAAADTNbestqvO973M0~0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL40~Fm-NPxZb3; FP_UID=c7c9470bfb02023a83da33e2d835d5e2`
	p := NewAppIdBaijiaPlat(appId, ck)
	beego.Info(p.publishVideo("极限大挑战_盯到你害羞为止", "极限大挑战_盯到你害羞为止", bytes, "video.mp4", int64(len(bytes)), ioutil.NopCloser(bufio.NewBuffer(imagebytes))))
}

func Test_Type_Parse(t *testing.T) {
	jtypes := `{"errno":0,"errmsg":"success","ret":[{"id":"125","cateid":"1","name":"\u56fd\u9645"},{"id":"127","cateid":"3","name":"\u4f53\u80b2"},{"id":"128","cateid":"4","name":"\u5a31\u4e50"},{"id":"129","cateid":"5","name":"\u793e\u4f1a"},{"id":"130","cateid":"6","name":"\u8d22\u7ecf"},{"id":"131","cateid":"7","name":"\u4e92\u8054\u7f51"},{"id":"132","cateid":"8","name":"\u79d1\u6280"},{"id":"133","cateid":"9","name":"\u623f\u4ea7"},{"id":"134","cateid":"10","name":"\u6c7d\u8f66"},{"id":"135","cateid":"11","name":"\u6559\u80b2"},{"id":"136","cateid":"12","name":"\u65f6\u5c1a"},{"id":"137","cateid":"13","name":"\u6e38\u620f"},{"id":"138","cateid":"14","name":"\u519b\u4e8b"},{"id":"139","cateid":"15","name":"\u65c5\u6e38"},{"id":"140","cateid":"16","name":"\u751f\u6d3b"},{"id":"142","cateid":"18","name":"\u521b\u610f"},{"id":"143","cateid":"19","name":"\u641e\u7b11"},{"id":"144","cateid":"20","name":"\u7f8e\u56fe"},{"id":"145","cateid":"21","name":"\u5973\u4eba"},{"id":"146","cateid":"22","name":"\u7f8e\u98df"},{"id":"147","cateid":"23","name":"\u5bb6\u5c45"},{"id":"148","cateid":"24","name":"\u5065\u5eb7"},{"id":"149","cateid":"25","name":"\u4e24\u6027"},{"id":"150","cateid":"26","name":"\u60c5\u611f"},{"id":"152","cateid":"28","name":"\u80b2\u513f"},{"id":"153","cateid":"29","name":"\u6587\u5316"},{"id":"154","cateid":"30","name":"\u5386\u53f2"},{"id":"155","cateid":"31","name":"\u5ba0\u7269"},{"id":"156","cateid":"32","name":"\u79d1\u5b66"},{"id":"157","cateid":"33","name":"\u52a8\u6f2b"},{"id":"158","cateid":"34","name":"\u804c\u573a"},{"id":"159","cateid":"35","name":"\u4e09\u519c"},{"id":"161","cateid":"37","name":"\u60a6\u8bfb"},{"id":"198","cateid":"38","name":"\u8f9f\u8c23"}]}`
	type Types struct {
		Ret []struct {
			Name string `json:"name"`
		} `json:"ret"`
	}
	types := Types{}
	err := json.Unmarshal([]byte(jtypes), &types)
	if err != nil {
		t.Fatal(err)
	}

	for i, ty := range types.Ret {
		fmt.Println(fmt.Sprintf(`"%s":%d,`, ty.Name, i+1))
	}
}

func TestTitle_Check(t *testing.T) {
	appId := `1558678428644773`
	ck := `BAIDUID=04F8D5CE6B92AEB9C937BD9792BA07D2:FG=1; BDUSS=F5ZndDa1BCMUJZUnN4a1JIcWNEbDVRTnE4eU0xNDJKZFczYk1FYmFtTy13U05hSVFBQUFBJCQAAAAAAAAAAAEAAADTNbestqvO973M0~0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL40~Fm-NPxZb3; FP_UID=c7c9470bfb02023a83da33e2d835d5e2`
	p := NewAppIdBaijiaPlat(appId, ck)
	fmt.Println(p.CheckCookie())
}
