package baijia

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	bjhArticle "publish/pub-lib/baijia/bjh-model"
	"publish/pub-lib/utils"
)


func (p *BaijiaPlat) PublishArticle(cat string,tag string,feedArticle FeedArticle) error{
	return PubArticleByChrome(p.Cookie, cat, feedArticle)
}


func (p *BaijiaPlat) PublishArticle_Http(cat int, tag string, feedArticle FeedArticle) ([]byte, error) {
	content := ""
	coverImages := []bjhArticle.CoverAutoImage{}
	feedContent := bjhArticle.FeedContent{}
	feedContent.Type = 1

	p.titleCheck(feedArticle.Title)

	for _, feed := range feedArticle.Feed {
		if feed.Type == "image" {

			url, err := p.dumpImage(feed.Data)
			if err != nil {
				fmt.Println("DumpImage:", err.Error())
				continue
			}

			coverImages = append(coverImages, bjhArticle.CoverAutoImage{Src: url, Width: 600, Height: 600, IsSuitable: true})

			content += bjhArticle.ImageToHtml(url, 600, 600)

			feedContent.ImageNum++
			imageData := bjhArticle.ImageData{Original: bjhArticle.Original{Src: url, Width: "600", Height: "600"}, Caption: "", Desc: "  "}
			feedContent.Items = append(feedContent.Items, bjhArticle.Item{Data: imageData, Type: "image"})
		}

		if feed.Type == "text" {
			content += bjhArticle.TextToHtml(feed.Data)
			feedContent.Items = append(feedContent.Items, bjhArticle.Item{Data: feed.Data, Type: "text"})
		}
	}

	article := bjhArticle.NewDefaultArticle(p.AppId, bjhArticle.BaiJia_ARTICLE_TYPE, feedArticle.Title, content, cat, "auto", []bjhArticle.CoverImage{}, []bjhArticle.CoverImagesMap{}, coverImages, feedContent, tag)

	body, err := p.publish2(*article)
	if err != nil {
		return body, err
	}
	return body, nil
}

func (p *BaijiaPlat) ComplexPublish(ty string, cat int, layout string, tag string, coverImagesMap []bjhArticle.CoverImagesMap, feedArticle FeedArticle) ([]byte, error) {
	content := ""
	coverImages := []bjhArticle.CoverImage{}

	feedContent := bjhArticle.FeedContent{}
	feedContent.Type = 1
	for _, coverImageMap := range coverImagesMap {
		coverImages = append(coverImages, bjhArticle.CoverImage{Src: coverImageMap.Src})
	}

	for _, feed := range feedArticle.Feed {
		if feed.Type == "image" {
			url := feed.Data

			content += bjhArticle.ImageToHtml(url, 600, 600)

			feedContent.ImageNum++
			imageData := bjhArticle.ImageData{Original: bjhArticle.Original{Src: url, Width: "600", Height: "600"}, Caption: "", Desc: "  "}
			feedContent.Items = append(feedContent.Items, bjhArticle.Item{Data: imageData, Type: "image"})
		}

		if feed.Type == "text" {
			content += bjhArticle.TextToHtml(feed.Data)
			feedContent.Items = append(feedContent.Items, bjhArticle.Item{Data: feed.Data, Type: "text"})
		}
	}

	article := bjhArticle.NewDefaultArticle(p.AppId, ty, feedArticle.Title, content, cat, layout, coverImages, coverImagesMap, []bjhArticle.CoverAutoImage{}, feedContent, tag)

	body, err := p.publish(*article)
	if err != nil {
		return body, err
	}

	return body, nil
}

func (p *BaijiaPlat) PublishGallery(cat string, tag string, feedArticle FeedArticle)  error {
	catId, ok := CatNameToID(cat)
	if !ok {
		return errors.New("没找到百家分类ID：" + cat)
	}

	content := ""
	coverImages := []bjhArticle.CoverAutoImage{}
	feedContent := bjhArticle.FeedContent{}
	feedContent.Type = 4
	for _, feed := range feedArticle.Feed {
		if feed.Type == "image" {

			url, err := p.dumpImage(feed.Data)
			if err != nil {
				fmt.Println("DumpImage:", err.Error())
				continue
			}

			coverImages = append(coverImages, bjhArticle.CoverAutoImage{Src: url, Width: 600, Height: 600, IsSuitable: true})

			content += bjhArticle.ImageToHtmlGallery(url, feed.Desc, 600, 600)

			feedContent.ImageNum++
			imageData := bjhArticle.ImageData{Original: bjhArticle.Original{Src: url, Width: "600", Height: "600"}, Caption: "", Desc: feed.Desc}
			feedContent.Items = append(feedContent.Items, bjhArticle.Item{Data: imageData, Type: "image"})
		}

	}

	article := bjhArticle.NewDefaultArticle(p.AppId, bjhArticle.BaiJia_GALLERY_TYPE, feedArticle.Title, content, catId, "auto", []bjhArticle.CoverImage{}, []bjhArticle.CoverImagesMap{}, coverImages, feedContent, tag)

	_, err := p.publish(*article)
	if err != nil {
		return err
	}
	return nil
}

func (p *BaijiaPlat) PublishVideo_Http(feeds FeedArticle) error {
	videoBytes := []byte{}
	imageBytes := []byte{}
	var err error
	desc := ""
	for _, feed := range feeds.Feed {
		if feed.Type == "video" {
			req := utils.Get(feed.Data)
			videoBytes, err = req.Bytes()
			if err != nil {
				return errors.New("视频请求失败:" + err.Error())
			}
			desc = feed.Desc
		}
		if feed.Type == "cover" {
			req := utils.Get(feed.Data)
			imageBytes, err = req.Bytes()
			if err != nil {
				return errors.New("封面请求失败:" + err.Error())
			}
		}

	}

	return p.publishVideo(feeds.Title, desc, videoBytes, "video.mp4", int64(len(videoBytes)), ioutil.NopCloser(bytes.NewBuffer(imageBytes)))
}

func (p *BaijiaPlat) PublishVideo( ty string, tag string,feeds FeedArticle) error {
	videoPath := ""
	imagePath := ""
	for _, feed := range feeds.Feed {
		if feed.Type == "video" {
			videoPath = feed.Data
		}
		if feed.Type == "cover" {
			imagePath = feed.Data
		}
	}

	return PubVideo(p.Cookie, ty, videoPath, imagePath, feeds.Title, tag)
}
