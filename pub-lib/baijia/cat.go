package baijia

var (
	CatMap    map[string]int
	ChromeMap map[string]int
	VideoMap  map[string]int
	VideoChromeMap map[string]int
)

func init() {
	CatMap = map[string]int{
		"国际":  1,
		"体育":  3,
		"娱乐":  4,
		"社会":  5,
		"财经":  6,
		"互联网": 7,
		"科技":  8,
		"房产":  9,
		"汽车":  10,
		"教育":  11,
		"时尚":  12,
		"游戏":  13,
		"军事":  14,
		"旅游":  15,
		"生活":  16,
		"创意":  18,
		"搞笑":  19,
		"美图":  20,
		"女人":  21,
		"美食":  22,
		"家居":  23,
		"健康":  24,
		"两性":  25,
		"情感":  26,
		"育儿":  28,
		"文化":  29,
		"历史":  30,
		"宠物":  31,
		"科学":  32,
		"动漫":  33,
		"职场":  34,
		"三农":  35,
		"悦读":  37,
		"辟谣":  38,
	}

	ChromeMap = map[string]int{
		"国际":  1,
		"体育":  2,
		"娱乐":  3,
		"社会":  4,
		"财经":  5,
		"互联网": 6,
		"科技":  7,
		"房产":  8,
		"汽车":  9,
		"教育":  10,
		"时尚":  11,
		"游戏":  12,
		"军事":  13,
		"旅游":  14,
		"生活":  15,
		"创意":  16,
		"搞笑":  17,
		"美图":  18,
		"女人":  19,
		"美食":  20,
		"家居":  21,
		"健康":  22,
		"两性":  23,
		"情感":  24,
		"育儿":  25,
		"文化":  26,
		"历史":  27,
		"宠物":  28,
		"科学":  29,
		"动漫":  30,
		"职场":  31,
		"三农":  32,
		"悦读":  33,
		"辟谣":  34,
	}

	VideoMap = map[string]int{
		"影视":  1001,
		"娱乐":  1002,
		"游戏":  1003,
		"音乐":  1004,
		"搞笑":  1005,
		"体育":  1006,
		"生活":  1007,
		"大自然": 1008,
		"科技":  1009,
		"社会":  1010,
		"舞蹈":  1011,
		"美食":  1012,
		"教育":  1013,
		"文化":  1014,
		"历史":  1014,
		"动漫":  1015,
		"军事":  1016,
		"汽车":  1017,
		"时尚":  1018,
		"亲子":  1019,
		"时政":  1020,
		"旅游":  1021,
		"财经":  1022,
		"其他":  1023,
	}

	VideoChromeMap = map[string]int{
		"影视":  1,
		"娱乐":  2,
		"游戏":  3,
		"音乐":  4,
		"搞笑":  5,
		"体育":  6,
		"生活":  7,
		"大自然": 8,
		"科技":  9,
		"社会":  10,
		"舞蹈":  11,
		"美食":  12,
		"教育":  13,
		"文化":  14,
		"历史":  15,
		"动漫":  16,
		"军事":  17,
		"汽车":  18,
		"时尚":  19,
		"亲子":  20,
		"时政":  21,
		"旅游":  22,
		"财经":  23,
		"其他":  24,
	}
}

func CatNameToID(name string) (int, bool) {
	catId, ok := CatMap[name]
	return catId, ok
}

func ChromeCatNameToID(name string) (int, bool) {
	catId, ok := ChromeMap[name]
	return catId, ok
}

func ChromeVideoCatNameToID(name string) (int, bool) {
	catId, ok := VideoChromeMap[name]
	return catId, ok
}
