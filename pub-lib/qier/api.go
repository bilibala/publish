package qier

import (
	"fmt"
	"io"
	"time"

	"publish/pub-lib/utils"
	"github.com/astaxie/beego"
	"github.com/kataras/go-errors"
)

type QierApi struct {
	ck string
}

func NewQierApi(ck string) *QierApi {

	return &QierApi{
		ck: ck,
	}
}

func (q *QierApi) CheckCookie() error {
	req := utils.Get(`https://om.qq.com/article/categoryList?relogin=1`)

	req.Header("Cookie", q.ck)
	req.Header("Accept-Encoding", "gzip, deflate")
	req.Header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	req.SetTimeout(5*time.Second, 5*time.Second)

	res := CookieCheckRes{}
	if err := req.ToJSON(&res); err != nil {
		return errors.New("cookie 无效")
	}

	if res.Response.Code == -10403 {
		return errors.New("cookie 无效")
	}

	return nil
}

func (q *QierApi) UploadImageByUrl(url string) (*UrlResponse, error) {
	req := utils.Post(`https://om.qq.com/image/archscaleupload?isRetImgAttr=1&relogin=1`)

	req.Header("Cookie", q.ck)
	req.Header("Accept-Encoding", "gzip, deflate")
	req.Header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	req.Header("X-Requested-With", "XMLHttpRequest")
	req.Header("Referer", "https://om.qq.com/article/articlePublish")

	req.Param("url", url)
	 req.SetTimeout(10*time.Second, 10*time.Second)

	urlImage := UrlImageResponse{}
	if err := req.ToJSON(&urlImage); err != nil {
		return nil, err
	}
	urlResponse, ok := urlImage.Data[url]
	if !ok {
		return nil, errors.New("not find image url ")
	}
	urlResponse.Url += `/641`
	return urlResponse, nil
}

func (q *QierApi) UploadImageByUrlAll(urls string) (map[string]*UrlResponse, error) {
	//req := utils.Post(`https://om.qq.com/image/imageInfoNoWater?relogin=1`)
	req := utils.Post(`https://om.qq.com/image/archscaleupload?isRetImgAttr=1&relogin=1`)

	req.Header("Cookie", q.ck)
	req.Header("Accept-Encoding", "gzip, deflate")
	req.Header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	req.Header("X-Requested-With", "XMLHttpRequest")
	req.Header("Referer", "https://om.qq.com/article/articlePublish")

	req.Param("url", urls)
	req.SetTimeout(15*time.Second, 15*time.Second)

	urlImage := UrlImageResponse{}
	if err := req.ToJSON(&urlImage); err != nil {
		return nil, err
	}

	for _, v := range urlImage.Data {
		v.Url += `/641`
	}

	return urlImage.Data, nil
}

func (q *QierApi) UploadImageByFile(reader io.ReadCloser, filename string) (*UrlResponse, error) {
	req := utils.Post(`https://om.qq.com/image/archscaleupload?isRetImgAttr=1&relogin=1`)

	req.Header("Cookie", q.ck)
	req.Header("Accept-Encoding", "gzip, deflate")
	req.Header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	req.Header("X-Requested-With", "XMLHttpRequest")
	req.Header("Referer", "https://om.qq.com/article/articlePublish")

	req.Param("subModule", "normal_zhengwen")
	req.Param("id", "WU_FILE_0")
	req.Param("name", filename)
	req.Param("type", "image/png")
	req.Param("lastModifiedDate", time.Now().String())
	req.Param("Filename", filename)
	req.PostFileByStream("Filedata", filename, reader)
	req.SetTimeout(10*time.Second, 10*time.Second)

	fileImage := UrlImageResponse{}
	if err := req.ToJSON(&fileImage); err != nil {
		return nil, err
	}
	urlResponse, ok := fileImage.Data["url"]
	if !ok {
		return nil, errors.New("not find image url ")
	}
	urlResponse.Url += `/641`
	return urlResponse, nil
}

func (q *QierApi) UploadCoverImage(url string) (*CoverImage, error) {
	req := utils.Post(`https://om.qq.com/image/exactupload?relogin=1`)

	req.Header("Cookie", q.ck)
	req.Header("Accept-Encoding", "gzip, deflate")
	req.Header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	req.Header("Referer", "https://om.qq.com/article/articlePublish")

	req.Param("url", url)
	req.Param("opCode", "151")
	req.Param("isUpOrg", "1")
	req.Param("subModule", "normal_cover")
	req.SetTimeout(10*time.Second, 10*time.Second)

	cover := CoverImage{}
	if err := req.ToJSON(&cover); err != nil {
		return nil, err
	}

	return &cover, nil
}

func (q *QierApi) publish(article *QArticle) error {
	req := utils.Post(`https://om.qq.com/article/publish?relogin=1`)

	req.Header("Cookie", q.ck)
	req.Header("Accept-Encoding", "gzip, deflate")
	req.Header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	req.Header("X-Requested-With", "XMLHttpRequest")
	req.Header("Referer", "https://om.qq.com/article/articlePublish")
	req.Header("Accept", "application/json, text/javascript, */*; q=0.01")

	req.Param("title", article.Title)
	req.Param("imgurl_ext", article.CoverImage)
	req.Param("content", article.Content)

	req.Param("cover_type", article.CoverType)
	req.Param("category_id", fmt.Sprintf("%d", article.Cat))

	req.Param("orignal", "0")
	req.Param("title2", "0")
	req.Param("tag", "")
	req.Param("video", "")
	req.Param("orignal", "0")
	req.Param("user_original", "0")
	req.Param("music", "")
	req.Param("activity", "")
	req.Param("apply_olympic_flag", "0")
	req.Param("apply_push_flag", "0")
	req.Param("apply_reward_flag", "0")
	req.Param("reward_flag", "0")
	req.Param("survey_id", "")
	req.Param("survey_name", "")
	req.Param("type", "0")
	req.Param("commodity", "")
	req.Param("articleId", "")
	req.Param("temp", "")
	req.Param("video_source_data", "")
	req.SetTimeout(15*time.Second, 15*time.Second)

	pubRes := PubResponse{}
	err := req.ToJSON(&pubRes)
	if err != nil {
		return err
	}

	if pubRes.Response.Code != 0 {
		beego.Error("企鹅发文失败:", pubRes.Response.Msg)
		return errors.New(fmt.Sprintf("企鹅发文失败:%s", pubRes.Response.Msg))
	}

	beego.Info("企鹅发文成功:", pubRes.Response.Msg)

	return nil
}

/*
title:Jeep第四代牧马人将上市 定位高端SUV
imgurl_ext:[{"1":"http://inews.gtimg.com/newsapp_ls/0/2568737940/0","150120":"http://inews.gtimg.com/newsapp_ls/0/2568737940_150120/0","192152":"http://inews.gtimg.com/newsapp_ls/0/2568737940_192152/0","196130":"http://inews.gtimg.com/newsapp_ls/0/2568737940_196130/0","196140":"http://inews.gtimg.com/newsapp_ls/0/2568737940_196140/0","240180":"http://inews.gtimg.com/newsapp_ls/0/2568737940_240180/0","294195":"http://inews.gtimg.com/newsapp_ls/0/2568737940_294195/0","354264":"http://inews.gtimg.com/newsapp_ls/0/2568737940_354264/0","src":"http://inews.gtimg.com/newsapp_bt/0/2568633999/641"},{"1":"http://inews.gtimg.com/newsapp_ls/0/2568742333/0","150120":"http://inews.gtimg.com/newsapp_ls/0/2568742333_150120/0","192152":"http://inews.gtimg.com/newsapp_ls/0/2568742333_192152/0","196130":"http://inews.gtimg.com/newsapp_ls/0/2568742333_196130/0","196140":"http://inews.gtimg.com/newsapp_ls/0/2568742333_196140/0","240180":"http://inews.gtimg.com/newsapp_ls/0/2568742333_240180/0","294195":"http://inews.gtimg.com/newsapp_ls/0/2568742333_294195/0","354264":"http://inews.gtimg.com/newsapp_ls/0/2568742333_354264/0","src":"http://inews.gtimg.com/newsapp_bt/0/2568634002/641","index":0},{"1":"http://inews.gtimg.com/newsapp_ls/0/2568737943/0","150120":"http://inews.gtimg.com/newsapp_ls/0/2568737943_150120/0","192152":"http://inews.gtimg.com/newsapp_ls/0/2568737943_192152/0","196130":"http://inews.gtimg.com/newsapp_ls/0/2568737943_196130/0","196140":"http://inews.gtimg.com/newsapp_ls/0/2568737943_196140/0","240180":"http://inews.gtimg.com/newsapp_ls/0/2568737943_240180/0","294195":"http://inews.gtimg.com/newsapp_ls/0/2568737943_294195/0","354264":"http://inews.gtimg.com/newsapp_ls/0/2568737943_354264/0","src":"http://inews.gtimg.com/newsapp_bt/0/2568634001/641"}]
content::<p class="">Jeep第四代牧马人将上市 定位高端SUV。Jeep的牧马人车型给国内的小伙伴们带去的是十分多的乐趣，在2018年的1月29日，又会即将上市第四代了车型，这也是新车全球首发环节。对于改革每代的牧马人都是很出色的，第四代官方定位于高端SUV车型。</p><p class="empty"></p><p class=""><p style="margin-bottom:1px;padding-bottom:1px;" class="empty"><img src="http://inews.gtimg.com/newsapp_bt/0/2568633999/641" desc="请输入图片描述" ori_src="http://07.imgmini.eastday.com/mobile/20171225/20171225214114_660d541cde52e7a6d9a4a85d83c8232b_1.jpeg"/></p><p type="om-image-desc" class=""></p></p><p class="empty"></p><p class="empty"></p><p class="empty"></p><p class="empty"></p><p class="">Jeep第四代牧马人侧正照</p><p class="">Jeep牧马人车型是非常受欢迎的在国内的市场中，很多小伙伴们都被它的越野性能所折服。并且每代车型都是有着各自的亮点，在2018年的年初Jeep官方将会正式上市第四代牧马人车型，这对于喜欢牧马人车型的粉丝们来说，应该十分开心吧。</p><p class="empty"></p><p class=""><p style="margin-bottom:1px;padding-bottom:1px;" class="empty"><img src="http://inews.gtimg.com/newsapp_bt/0/2568634000/641" desc="请输入图片描述" ori_src="http://07.imgmini.eastday.com/mobile/20171225/20171225214114_660d541cde52e7a6d9a4a85d83c8232b_2.jpeg"/></p><p type="om-image-desc" class=""></p></p><p class="empty"></p><p class="empty"></p><p class="empty"></p><p class="empty"></p><p class="">Jeep第四代牧马人侧照</p><p class="">第四代的全新车型还是在整体上体现着Jeep集团对人们的承诺，依旧是霸气的外观设计和精致的内部风格。新车的外观上还得保留着牧马人系列的经典设计语言，以此来显示自己独特的SUV气息。</p><p class="empty"></p><p class=""><p style="margin-bottom:1px;padding-bottom:1px;" class="empty"><img src="http://inews.gtimg.com/newsapp_bt/0/2568634001/641" desc="请输入图片描述" ori_src="http://07.imgmini.eastday.com/mobile/20171225/20171225214114_660d541cde52e7a6d9a4a85d83c8232b_3.jpeg"/></p><p type="om-image-desc" class=""></p></p><p class="empty"></p><p class="empty"></p><p class="empty"></p><p class="empty"></p><p class="">Jeep第四代牧马人正照</p><p class="">这次定位于高端的车型之后，官方负责人则是赋予它越看越豪华的气息，进气式格栅还是之前那的七竖设计造型，中间空着的结构是符合一定的空气动力学的。</p><p class="empty"></p><p class=""><p style="margin-bottom:1px;padding-bottom:1px;" class="empty"><img src="http://inews.gtimg.com/newsapp_bt/0/2568634002/641" desc="请输入图片描述" ori_src="http://07.imgmini.eastday.com/mobile/20171225/20171225214114_660d541cde52e7a6d9a4a85d83c8232b_4.jpeg"/></p><p type="om-image-desc" class=""></p></p>
*/

func (q *QierApi) publishGallery(article *QArticle) error {
	req := utils.Post(`https://om.qq.com/article/publish?relogin=1`)

	req.Header("Cookie", q.ck)
	req.Header("Accept-Encoding", "gzip, deflate")
	req.Header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	req.Header("X-Requested-With", "XMLHttpRequest")
	req.Header("Referer", "https://om.qq.com/article/articlePublish")
	req.Header("Accept", "application/json, text/javascript, */*; q=0.01")

	req.Param("title", article.Title)
	req.Param("imgurl_ext", article.CoverImage)
	req.Param("content", article.Content)

	req.Param("cover_type", article.CoverType)
	req.Param("category_id", fmt.Sprintf("%d", article.Cat))

	req.Param("orignal", "0")
	req.Param("title2", "0")
	req.Param("tag", "")
	req.Param("video", "")
	req.Param("user_original", "0")
	req.Param("music", "")
	req.Param("activity", "")
	req.Param("apply_olympic_flag", "0")
	req.Param("apply_push_flag", "0")
	req.Param("apply_reward_flag", "0")
	req.Param("reward_flag", "0")
	req.Param("survey_id", "")
	req.Param("survey_name", "")
	req.Param("type", "1")
	req.Param("urlParams[typeName]", "images")
	req.Param("articleId", "")
	req.Param("pushInfo", "images")
	req.Param("temp", "")
	req.Param("video_source_data", "")
	req.SetTimeout(20*time.Second, 20*time.Second)

	pubRes := PubResponse{}
	err := req.ToJSON(&pubRes)
	if err != nil {
		return err
	}

	if pubRes.Response.Code != 0 {
		beego.Error("企鹅发图集失败:", pubRes.Response.Msg)
		return errors.New(fmt.Sprintf("企鹅发图集失败:%s", pubRes.Response.Msg))
	}

	beego.Info("企鹅发图集成功:", pubRes.Response.Msg)

	return nil
}
