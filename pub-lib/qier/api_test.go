package qier

import (
	"bytes"
	"fmt"
	"io/ioutil"
	url2 "net/url"
	"testing"

	"publish/pub-lib/baijia"
	"publish/pub-lib/utils"
)

func TestQierApi_UploadImageByUrl(t *testing.T) {
	api := NewQierApi(qiertest_ck)
	res, err := api.UploadImageByUrlAll("http://p3.pstatp.com/large/6c2e000275cf2b39d446")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(res)
}

func TestQierApi_UploadImageByFile(t *testing.T) {
	ck := `9e67236d07bdc7152e6e2b42b7f00f43=764b59251f8ec213b8ee54198c8eead006a19425a%253A4%253A%257Bi%253A0%253Bs%253A7%253A%25225838951%2522%253Bi%253A1%253Bs%253A19%253A%252218071567524%2540sina.cn%2522%253Bi%253A2%253Bi%253A43200%253Bi%253A3%253Ba%253A13%253A%257Bs%253A6%253A%2522status%2522%253Bi%253A2%253Bs%253A5%253A%2522email%2522%253Bs%253A19%253A%252218071567524%2540sina.cn%2522%253Bs%253A9%253A%2522logintype%2522%253Bi%253A1%253Bs%253A3%253A%2522uin%2522%253BN%253Bs%253A5%253A%2522phone%2522%253BN%253Bs%253A6%253A%2522imgurl%2522%253Bs%253A55%253A%2522http%253A%252F%252Finews.gtimg.com%252Fnewsapp_ls%252F0%252F2082201467_200200%252F0%2522%253Bs%253A4%253A%2522name%2522%253Bs%253A15%253A%2522%25E5%25B0%258F%25E7%25BE%258E%25E7%25BE%258E%25E5%25A8%25B1%25E4%25B9%2590%2522%253Bs%253A10%253A%2522isVerified%2522%253Bb%253A1%253Bs%253A10%253A%2522isRejected%2522%253Bb%253A0%253Bs%253A9%253A%2522agreeAcpt%2522%253Bb%253A0%253Bs%253A6%253A%2522pwdChg%2522%253Bb%253A0%253Bs%253A9%253A%2522avatarChg%2522%253Bb%253A0%253Bs%253A2%253A%2522id%2522%253Bs%253A7%253A%25225838951%2522%253B%257D%257D; expires=Tue, 26-Dec-2017 18:05:48 GMT; path=/`
	url := `http://cs.autotimes.com.cn/upload/d1/9668/newsimg/1712181655496030419.jpg`
	req := utils.Get(url)
	byte, err := req.Bytes()
	if err != nil {
		t.Fatal(err)
	}
	api := NewQierApi(ck)
	res, err := api.UploadImageByFile(ioutil.NopCloser(bytes.NewBuffer(byte)), "test.png")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(res)
}

func TestQierApi_UploadCoverImage(t *testing.T) {
	api := NewQierApi(qiertest_ck)
	res, err := api.UploadCoverImage("http://inews.gtimg.com/newsapp_bt/0/2785054229/641")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(res)
}

func TestQierApi_Publish(t *testing.T) {
	au := `https://www.toutiao.com/a6541898625700594189/`
	url := `http://127.0.0.1:3333/article/feed?url=`+url2.QueryEscape(au)
	feedArticle := baijia.FeedArticle{}
	if err := utils.Get(url).ToJSON(&feedArticle); err != nil {
		t.Fatal(err)
	}
	//feedArticle.Feed[1].Type = "cover"
	fmt.Println("feedArticle:",feedArticle)
	api := NewQierApi(qiertest_ck)
	//fmt.Println(api.CheckCookie())
	if err := api.PublishArticle("情感","", feedArticle); err != nil {
		t.Fatal(err)
	}
}

func TestQierApi_PublishGallery(t *testing.T) {
	au := `https://www.toutiao.com/a6526476437040398856/`
	url := `http://127.0.0.1:3333/article/feed?url=`+url2.QueryEscape(au)

	feedArticle := baijia.FeedArticle{}
	if err := utils.Get(url).ToJSON(&feedArticle); err != nil {
		t.Fatal(err)
	}
	api := NewQierApi(qiertest_ck)
	if err := api.PublishGallery("搞笑","", feedArticle); err != nil {
		t.Fatal(err)
	}
}

func TestQierApi_ChromeVideo(t *testing.T) {
	video := baijia.FeedArticle{
		Title: "新郎疯了，拿屎往自己身上倒，新娘差点气疯不上车！",
		Feed: []baijia.Feed{
			baijia.Feed{
				Type: "video",
				Data: "http://ugcydzd.qq.com/v05666oyvv5.mp4?vkey=B922895E90AFF851F9DD1D6900A84CE643D4A833ABF2617810165C24D12A3892AAD5E4387ED2703BD60C2802804F10871A48609CE611FFC288FB73A97D21E4CA1C47FCE7619D4CC20CD2D045859153B34069AC2CC1D74C79FAF3920C9D52051B96DB0B4C3E0480E1",
			},
			baijia.Feed{
				Type: "cover",
				Data: "http://inews.gtimg.com/newsapp_ls/0/2972023018_640360/0",
			},
		},
	}
	api := NewQierApi(qiertest_ck)
	api.PublishVideo("搞笑", "",video )
}

func TestQierApi_CheckCookie(t *testing.T) {
	ck := `9e67236d07bdc7152e6e2b42b7f00f43=c426969ee6116857b8602e6d98e4b2fe5dfc42ffa%253A4%253A%257Bi%253A0%253Bs%253A7%253A%25225726507%2522%253Bi%253A1%253Bs%253A19%253A%252217335892751%2540163.com%2522%253Bi%253A2%253Bi%253A43200%253Bi%253A3%253Ba%253A12%253A%257Bs%253A6%253A%2522status%2522%253Bs%253A1%253A%25222%2522%253Bs%253A5%253A%2522email%2522%253Bs%253A19%253A%252217335892751%2540163.com%2522%253Bs%253A6%253A%2522imgurl%2522%253Bs%253A55%253A%2522http%253A%252F%252Finews.gtimg.com%252Fnewsapp_ls%252F0%252F1919153277_200200%252F0%2522%253Bs%253A3%253A%2522uin%2522%253Bs%253A0%253A%2522%2522%253Bs%253A5%253A%2522phone%2522%253BN%253Bs%253A4%253A%2522name%2522%253Bs%253A18%253A%2522%25E5%258D%258A%25E5%25BC%25A6%25E6%259C%2588%25E7%25BE%258E%25E5%25B0%2591%25E5%25A5%25B3%2522%253Bs%253A10%253A%2522isVerified%2522%253Bb%253A1%253Bs%253A10%253A%2522isRejected%2522%253Bb%253A0%253Bs%253A26%253A%2522agreementAcceptingRequired%2522%253Bb%253A0%253Bs%253A29%253A%2522initialPasswordChangeRequired%2522%253Bb%253A0%253Bs%253A27%253A%2522initialAvatarChangeRequired%2522%253Bb%253A0%253Bs%253A2%253A%2522id%2522%253Bs%253A7%253A%25225726507%2522%253B%257D%257D; alertclicked=%7C%7C; cookie_test=1; fimgurl=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_ls%2F0%2F1919153277_200200%2F0; fname=%E5%8D%8A%E5%BC%A6%E6%9C%88%E7%BE%8E%E5%B0%91%E5%A5%B3; OM_EMAIL=17335892751@163.com; omtoken=da98f39e0e; omtoken_expire=1521701075; pgv_info=ssid=s2225067256; pgv_pvi=15327232; pgv_pvid=8203612256; pgv_si=s9298397184; rmod=1; TSID=h308r7uer78kvce0ki9al59no7; tvfe_boss_uuid=b70a4d5e06d764b8; userid=5726507; TSID=gkshqbuql4bso4sqj19nd87ms5; 9e67236d07bdc7152e6e2b42b7f00f43=c426969ee6116857b8602e6d98e4b2fe5dfc42ffa%253A4%253A%257Bi%253A0%253Bs%253A7%253A%25225726507%2522%253Bi%253A1%253Bs%253A19%253A%252217335892751%2540163.com%2522%253Bi%253A2%253Bi%253A43200%253Bi%253A3%253Ba%253A12%253A%257Bs%253A6%253A%2522status%2522%253Bs%253A1%253A%25222%2522%253Bs%253A5%253A%2522email%2522%253Bs%253A19%253A%252217335892751%2540163.com%2522%253Bs%253A6%253A%2522imgurl%2522%253Bs%253A55%253A%2522http%253A%252F%252Finews.gtimg.com%252Fnewsapp_ls%252F0%252F1919153277_200200%252F0%2522%253Bs%253A3%253A%2522uin%2522%253Bs%253A0%253A%2522%2522%253Bs%253A5%253A%2522phone%2522%253BN%253Bs%253A4%253A%2522name%2522%253Bs%253A18%253A%2522%25E5%258D%258A%25E5%25BC%25A6%25E6%259C%2588%25E7%25BE%258E%25E5%25B0%2591%25E5%25A5%25B3%2522%253Bs%253A10%253A%2522isVerified%2522%253Bb%253A1%253Bs%253A10%253A%2522isRejected%2522%253Bb%253A0%253Bs%253A26%253A%2522agreementAcceptingRequired%2522%253Bb%253A0%253Bs%253A29%253A%2522initialPasswordChangeRequired%2522%253Bb%253A0%253Bs%253A27%253A%2522initialAvatarChangeRequired%2522%253Bb%253A0%253Bs%253A2%253A%2522id%2522%253Bs%253A7%253A%25225726507%2522%253B%257D%257D; pgv_info=ssid=s2225067256; ts_last=om.qq.com/; pgv_pvid=8203612256; ts_uid=4227031630`
	api := NewQierApi(ck)
	fmt.Println(api.CheckCookie())
}

var (
	qiertest_ck = `pt2gguin=o0793123055; RK=wH5dL+AVc3; ptcz=782acdb1b5801224a60373133980e00eec50e1134b4b073512330f2b87493b45; tvfe_boss_uuid=566c32fc337724f6; UM_distinctid=161f5ef42718c-021bd208fc0118-326e7a05-13c680-161f5ef4272260; pgv_pvid=3037799300; o_cookie=793123055; pgv_pvi=4683840512; pac_uid=1_793123055; 3g_guest_id=-8766453964058066944; g_ut=2; ptui_loginuin=793123055; pgv_si=s9084339200; ptisp=cnc; pgv_info=ssid=s6859735851; ts_refer=www.baidu.com/link; ts_uid=9982400256; rmod=1; uin=; skey=; fname=%E6%9C%BA%E5%99%A8%E7%8C%AB%E6%83%85%E6%84%9F%E4%B8%93%E5%AE%B6; fimgurl=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_ls%2F0%2F2593018430_200200%2F0; userid=6318196; omtoken=e943fb19c4; omtoken_expire=1523561293; alertclicked=%7C%7C; ts_last=om.qq.com/article/messages; TSID=mml1e7ntnrlbtmbsu4jjhsqnp4; 9e67236d07bdc7152e6e2b42b7f00f43=4b7009425b874da68bd254a2dd0bd8e860fd4edba%253A4%253A%257Bi%253A0%253Bi%253A6318196%253Bi%253A1%253Bs%253A5%253A%2522Guest%2522%253Bi%253A2%253Bi%253A43200%253Bi%253A3%253Ba%253A14%253A%257Bs%253A6%253A%2522status%2522%253Bi%253A2%253Bs%253A5%253A%2522phone%2522%253Bs%253A11%253A%252218779116093%2522%253Bs%253A2%253A%2522id%2522%253Bi%253A6318196%253Bs%253A9%253A%2522logintype%2522%253Bi%253A2%253Bs%253A5%253A%2522email%2522%253BN%253Bs%253A3%253A%2522uin%2522%253BN%253Bs%253A4%253A%2522wxid%2522%253BN%253Bs%253A6%253A%2522imgurl%2522%253Bs%253A55%253A%2522http%253A%252F%252Finews.gtimg.com%252Fnewsapp_ls%252F0%252F2593018430_200200%252F0%2522%253Bs%253A4%253A%2522name%2522%253Bs%253A21%253A%2522%25E6%259C%25BA%25E5%2599%25A8%25E7%258C%25AB%25E6%2583%2585%25E6%2584%259F%25E4%25B8%2593%25E5%25AE%25B6%2522%253Bs%253A10%253A%2522isVerified%2522%253Bb%253A1%253Bs%253A10%253A%2522isRejected%2522%253Bb%253A0%253Bs%253A9%253A%2522agreeAcpt%2522%253Bb%253A0%253Bs%253A6%253A%2522pwdChg%2522%253Bb%253A0%253Bs%253A9%253A%2522avatarChg%2522%253Bb%253A0%253B%257D%257D`
)
