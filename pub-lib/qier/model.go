package qier

import "fmt"

type UrlImageResponse struct {
	Response Response               `json:"response"`
	Data     map[string]*UrlResponse `json:"data"`
}

/*
"response": {
    "code": 0,
    "msg": "success!",
    "cost": "652ms"
  }
*/

type CookieCheckRes struct {
	Response struct{
		Code int    `json:"code"`
		Msg  string `json:"msg"`
	} `json:"response"`
}
type Response struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Cose string `json:"cose"`
}

type UrlResponse struct {
	Url    string `json:"url"`
	Srcurl string `json:"srcurl"`
}

type CoverImage struct {
	Response Response          `json:"response"`
	Data     map[string]string `json:"data"`
}

type QArticle struct {
	Title      string `json:"title"`
	Content    string `json:"content"`
	CoverImage string `json:"cover_image"`
	Cat        int    `json:"cat"`
	CoverType string `json:"cover_type"`
}

type PubResponse struct {
	Response struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
	} `json:"response"`
}

type GalleryContent struct {
	Image string `json:"image"`
	Desc  string `json:"desc"`
}

func Pwrapper(text string) string {
	return `<p class="">` + text + `</p><p class="empty"></p>`
}

func Imgwrapper(url string, originUrl string) string {
	return fmt.Sprintf(`<p style="margin-bottom:1px;padding-bottom:1px;" class="empty"><img src="%s" desc="请输入图片描述" ori_src="%s"/></p><p type="om-image-desc" class=""></p><p class="empty"></p>`, url, originUrl)
}
