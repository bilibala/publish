package utils

import (
	"fmt"
	"testing"
)

func TestGetVideoRealAddr(t *testing.T) {
	fmt.Println(GetVideoRealAddr(`https://reflow.huoshan.com/share/item/6515056909769248013/?tag=0&timestamp=1516935218&share_ht_uid=69971742182&did=39381032916&utm_medium=huoshan_android&tt_from=mobile_qq&iid=24531131157&app=live_stream`))
	fmt.Println(GetVideoRealAddr(`https://kuaibao.qq.com/s/20160519V06XXE00?refer=kb_news`))
	fmt.Println(GetVideoRealAddr(`http://hotsoon.snssdk.com/hotsoon/item/video/_playback/?video_id=cc9946dad76043f591b0581dec7137af&line=0&watermark=0&app_id=1112`))
}

func TestTitleCount(t *testing.T) {
	fmt.Println(TitleCount("四大天王差点就凑齐了的经典电影，开头竟然还出现了周星驰的海报！", false))
	fmt.Println(TitleCount("四大天王差点就凑齐了的经典电影，开头竟然还出现了周星驰的海报！", true))
}
