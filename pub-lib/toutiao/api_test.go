package toutiao

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/astaxie/beego"
	"publish/pub-lib/baijia"
	"publish/pub-lib/utils"
)

func TestToutiaoApi_UploadImageByUrl(t *testing.T) {
	ck := `_ba=BA0.2-20170911-5110e-6MJLGPlODmQQERelrvJz; _ga=GA1.2.173216.1505188283; _gat=1; _gid=GA1.2.2033531589.1505188283; gr_cs1_97b3e1c5-449a-451a-b6d7-a39d4004364c=advertiser_id%3A66534060690; gr_session_id_aefa4e5d2593305f=97b3e1c5-449a-451a-b6d7-a39d4004364c; gr_user_id=1e1cef5b-b472-49d6-b368-50fbb3582940; login_flag=89afa9bfc3a5be3b70121605a381a599; part=stable; sessionid=2e3c56adabeb321c9d646d4beba559dc; sessionid=2e3c56adabeb321c9d646d4beba559dc; sid_guard="2e3c56adabeb321c9d646d4beba559dc|1510566775|2591998|Wed\054 13-Dec-2017 09:52:53 GMT"; sid_tt=2e3c56adabeb321c9d646d4beba559dc; sid_tt=2e3c56adabeb321c9d646d4beba559dc; sso_login_status=1; tt_im_token=1505188344702501260936097684570064450211792689333992529740234534; tt_webid=62782867241; uid_tt=f18ce42d467d576f505be07af7a969c1; UM_distinctid=15e7436863b0-0b2c307e291338-31637e01-144000-15e7436863c899; uuid="w:d468ce280c74498ab801ad964bda0aa5"; currentMediaId=1575982219312141; _ga=GA1.3.173216.1505188283; _gid=GA1.3.2033531589.1505188283; UM_distinctid=15fbd7dc78391b-0d9de579b6d125-31637e01-13c680-15fbd7dc784b7d; tt_webid=6488461451240523277`
	url := `http://b4-q.mafengwo.net/s10/M00/47/2D/wKgBZ1l5r4SAJC6VABQdBb9hbQI27.jpeg?imageView2%2F2%2Fw%2F700%2Fh%2F600%2Fq%2F90%7CimageMogr2%2Fstrip%2Fquality%2F90`
	api := NewToutiaoApi(ck,0,"")
	res, err := api.UploadImageByUrl(url)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(res)
}

func TestToutiaoApi_UploadImageByFile(t *testing.T) {
	ck := `_ba=BA0.2-20170911-5110e-6MJLGPlODmQQERelrvJz; _ga=GA1.2.173216.1505188283; _gat=1; _gid=GA1.2.2033531589.1505188283; gr_cs1_97b3e1c5-449a-451a-b6d7-a39d4004364c=advertiser_id%3A66534060690; gr_session_id_aefa4e5d2593305f=97b3e1c5-449a-451a-b6d7-a39d4004364c; gr_user_id=1e1cef5b-b472-49d6-b368-50fbb3582940; login_flag=89afa9bfc3a5be3b70121605a381a599; part=stable; sessionid=2e3c56adabeb321c9d646d4beba559dc; sessionid=2e3c56adabeb321c9d646d4beba559dc; sid_guard="2e3c56adabeb321c9d646d4beba559dc|1510566775|2591998|Wed\054 13-Dec-2017 09:52:53 GMT"; sid_tt=2e3c56adabeb321c9d646d4beba559dc; sid_tt=2e3c56adabeb321c9d646d4beba559dc; sso_login_status=1; tt_im_token=1505188344702501260936097684570064450211792689333992529740234534; tt_webid=62782867241; uid_tt=f18ce42d467d576f505be07af7a969c1; UM_distinctid=15e7436863b0-0b2c307e291338-31637e01-144000-15e7436863c899; uuid="w:d468ce280c74498ab801ad964bda0aa5"; currentMediaId=1575982219312141; _ga=GA1.3.173216.1505188283; _gid=GA1.3.2033531589.1505188283; UM_distinctid=15fbd7dc78391b-0d9de579b6d125-31637e01-13c680-15fbd7dc784b7d; tt_webid=6488461451240523277`
	url := `http://b1-q.mafengwo.net/s10/M00/4F/48/wKgBZ1nFIAKAQWi-ABbKpmy5y-w33.jpeg?imageView2%2F2%2Fw%2F1360%2Fq%2F90%7Cwatermark%2F1%2Fimage%2FaHR0cDovL3AxLXEubWFmZW5nd28ubmV0L3MxMC9NMDAvMDgvQjkvd0tnQloxaDExQ0tBRjRUeEFBQXYzcVNMWmRvOTA4LnBuZz9pbWFnZU1vZ3IyJTJGdGh1bWJuYWlsJTJGMjQxeCUyRnF1YWxpdHklMkY5MA%3D%3D%2Fgravity%2FSouthEast%2Fdx%2F10%2Fdy%2F15%2Fdissolve%2F40`
	req := utils.Get(url)
	byte, err := req.Bytes()
	if err != nil {
		t.Fatal(err)
	}
	api := NewToutiaoApi(ck,0,"")
	res, err := api.UploadImageByFile(ioutil.NopCloser(bytes.NewBuffer(byte)), "test.png", int64(len(byte)))
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(res)
}

func TestToutiaoApi_UrlPublish(t *testing.T) {
	ck := `__tea_sdk__ssid=0;__tea_sdk__user_unique_id=66495046712;__tea_sdk__web_id=3513092206;_ba=BA0.2-20170811-5110e-4LWx69Sl1Xu0ihfgg9uE;_ga=GA1.2.1941511440.1506408040;_ga=GA1.3.1941511440.1506408040;_gat=1;_gid=GA1.3.1142158948.1506408040;_gid=GA1.2.1142158948.1506408040;_mp_test_key_1=25b24d6080ef3cceeb7bd75ce93f8f10;currentMediaId=1575958146116621;sessionid=de084e345284a0a12b2942fe8ac735f3;sid_tt=de084e345284a0a12b2942fe8ac735f3;sso_login_status=1;tt_im_token=1506408057352420702310756309867463183816751797857549724054195806;tt_webid=63306834177;uid_tt=d937e2dc6b42ea45ec5c5f2120a24a3a;UM_distinctid=15ebcea93f7124-064b34dffdf0c5-31637e01-1fa400-15ebcea93f8405;uuid="w:c9de73d409db438e9d719d227cf01e8b"`
	url := `https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_401757726939591334%22%7D&n_type=0&p_from=1`
	api := NewToutiaoApi(ck,0,"")
	res, err := api.UrlPublish("搞笑",url)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(res)
}

func TestPublish_Gallery(t *testing.T) {
	url := `http://111.230.24.49:3333/article/feed?url=http%3a%2f%2fwww.autotimes.com.cn%2fnews%2f201712%2f686554.html`

	feedArticle := baijia.FeedArticle{}
	err := utils.Get(url).ToJSON(&feedArticle)
	if err != nil {
		t.Fatal(err)
	}

	for i, _ := range feedArticle.Feed {
		feedArticle.Feed[i].Desc = "年轻态jeep自由侠报价让我们去旅行吧"
	}

	ck := `_ba=BA0.2-20171117-5110e-8UNEkbDbhFpbjx7iqlpE; _ga=GA1.3.1222590907.1512735184; _ga=GA1.2.1222590907.1512735184; _gid=GA1.3.576592213.1512735184; _gid=GA1.2.576592213.1512735184; _mp_test_key_1=44a795887bd9635514b6c818da88b085; currentMediaId=1584122181923854; sessionid=b00a37fbc4a728c0b1bf386b7b3f8abb; sid_tt=b00a37fbc4a728c0b1bf386b7b3f8abb; sso_login_status=1; tt_im_token=1512735137181787964100735290150342123314515170562563173216310410; tt_webid=6497147806703257102; UM_distinctid=160360b15da0-008e548519ca91-31637e01-1fa400-160360b15db3c6; uuid="w:9060a44e2230413d97e87eac09649e3b"`
	api := NewToutiaoApi(ck,0,"")
	err = api.PublishGallery("","",feedArticle)
	if err != nil {
		t.Fatal(err)
	}
}
func TestName(t *testing.T) {
	id := getGalleryId()
	fmt.Println(fmt.Sprintf("%d", id))
}

func TestMd5(t *testing.T) {
	md5Ctx := md5.New()
	md5Ctx.Write([]byte("test md5 encrypto"))
	cipherStr := md5Ctx.Sum(nil)
	fmt.Println(hex.EncodeToString(cipherStr))
}

func TestToutiao_SimplePublishVideo(t *testing.T) {
	mid := int64(1575972737903629)
	username := `情感话篓子`
	video := `http://mvvideo10.meitudata.com/5a54959fb63e01717_H264_3.mp4?k=a93223166116aeab677ce08fd7219c8c&t=5a59a8de`
	ck := `BAIDUID=E2208499D5D4E0AA6A210AF0C9A7E1A6:FG=1; BDUSS=ZtQjBIbGNPWW5YVXBIN01yZ09Tay1kaU5QeDVkbVVyWExza3JDcGFWWWYxaU5hSVFBQUFBJCQAAAAAAAAAAAEAAAA6OZGqtPzK88rzwujC6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9J~FkfSfxZcH; FP_UID=46bea79ac03572ceb2dc501f1cb318c5; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1511770435; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1511056232,1511058455,1511169985,1511770435; PMS_JT=%28%7B%22s%22%3A1511595014087%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/%22%7D%29; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1515641575`
	p := NewToutiaoApi(ck,mid,username)
	feeds := baijia.FeedArticle{Title: "小朋友来来来，跟不跟我走", Feed: []baijia.Feed{{Data: video, Type: "video", Desc: "小朋友跟不跟我走"}}}
	beego.Info(p.PublishVideo("","" , feeds))
}

func TestToutiaoApi_GetVideoImage(t *testing.T) {
	vid := `e5c75c4f4480498eb0c9094474bd94a1`
	ck := `_ba=BA0.2-20171025-5110e-xcQqn2t2ZRgFOBgucg5a; _ga=GA1.2.354736039.1511945259; _gid=GA1.2.1524197835.1511945259; _mp_test_key_1=c6dc4bc5ee0a245e6778d96d55641da5; sessionid=c9e10c4a59702d5818fe51deeef275e0; sid_tt=c9e10c4a59702d5818fe51deeef275e0; sso_login_status=1; tt_im_token=1511945825767828447232978133810516468190086256629331031611294043; tt_webid=6493755456402015758; UM_distinctid=16006f5ccaa0-01cfadafc9f766-31637e01-1fa400-16006f5ccab337; uuid="w:dcc020bf7f25458b9ce4bb7fe563459e"; currentMediaId=1584119757744142; _ga=GA1.3.354736039.1511945259; _gid=GA1.3.1524197835.1511945259; ptcn_no=b52ac5af03e18ef736bb7d9db7fd8953`
	p := NewToutiaoApi(ck,0,"")
	beego.Info(p.getVideoImage(vid))
}

func TestToutiaoApi_CheckCookie(t *testing.T) {
	ck := `sessionid=39e22928786c1a370b39bad57617e9dc`

	//ck := `_mp_test_key_1=187f337e3af24fe6fa00e4c5ec6cb59a; login_flag=bb4cc4a9605147ac94aeb7c620f0f8c7; sessionid=cead1c52b831e90e4685437dc46d0d17; sid_guard="9a84596a68dd1b9527e7ff73641b5a35|1521616660|15552000|Mon\054 17-Sep-2018 07:17:40 GMT"; sid_tt=9a84596a68dd1b9527e7ff73641b5a35; sso_login_status=1; tt_im_token=1521616666195979810334144665278165574058340127825375129579908128; tt_webid=6535291568394520068; UM_distinctid=1624765aabc4c5-05e4afdc041cfd-31637e01-1fa400-1624765aabd938; uuid="w:8e935b9a74e34cc8aec7f6b3f19a2373"; __tea_sdk__user_unique_id=6535291568394520068; __tea_sdk__ssid=5d7d3836-fd31-455c-9658-8e28a80967d0; ptcn_no=c69a5d2d151a4fe541f68128fc60d910; uid_tt=cd85ed4c9938a46ffd47e2aad72f9f4e`
	p := NewToutiaoApi(ck,0,"")
	fmt.Println(p.CheckCookie())
	fmt.Println(p.CheckCookie())
	fmt.Println(p.CheckCookie())
	fmt.Println(p.CheckCookie())

	fmt.Println(p.CheckCookie())
	fmt.Println(p.CheckCookie())

}

const toutiao_ck = `UM_distinctid=162066540ce422-0952a88698350e-32607b05-13c680-162066540cf47c; tt_webid=6530608237027640846; _ga=GA1.2.1933610257.1520925359; uuid="w:283ce57018a64340ba4712f9ea8071dd"; _gid=GA1.2.1532000179.1523438483; login_flag=6b4d323f2814e0b5b4506b7f8cc662b1; sid_tt=02cc66d5d1f8a096eed66ac90e24d799; sid_guard="02cc66d5d1f8a096eed66ac90e24d799|1523516236|15552000|Tue\054 09-Oct-2018 06:57:16 GMT"; _ba=BA0.2-20180412-5110e-HrHuIRCQOLLuER8Z3vCS; currentMediaId=1588102840041486; _mp_auth_key=8f24c37d1a4ef0317115cdbc9dd6a096; sso_login_status=1; sessionid=ec472531e27bc77ecea8a2b4a5a91d2d; _mp_test_key_1=4a74a8539ce95735a10ab10b207f71eb; uid_tt=00a4c09cc6cfb06428564706bb41de05; __tea_sdk__user_unique_id=83269262954; tt_im_token=1523536054873689360853732601445327247911790388397070662565823516; __tea_sdk__ssid=41706e35-9c60-4b0d-82e1-00cedce97797; ptcn_no=5db48a22ec056a0b758570115d4a5627`
func TestToutiaoApi_UrlPublishMore(t *testing.T) {
	url := `http://www.huabian.com/shenghuo/20180411/224771.html`
	api := NewToutiaoApi(toutiao_ck,0,"")
	res, err := api.UrlPublish("",url)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(res)
}
