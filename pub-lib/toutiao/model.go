package toutiao

type UrlImageResponse struct {
	Url    string `json:"url"`
	SrcUrl string `json:"srcurl"`
}

type FileImageResponse struct {
	Title    string `json:"title"`
	Url      string `json:"url"`
	Height   int    `json:"height"`
	Width    int    `json:"width"`
	State    string `json:"state"`
	Original string `json:"original"`
}

type PostArticleResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Data    Data   `json:"data"`
}

type Data struct {
	PgcId string `json:"pgc_id"`
}

type TtArticle struct {
	Title       string       `json:"title"`
	Content     string       `json:"content"`
	ImageCovers []CoverImage `json:"pgc_feed_covers"`
}

type CoverImage struct {
	Id          int    `json:"id"`
	Url         string `json:"url"`
	Uri         string `json:"Uri"`
	OriginUri   string `json:"origin_uri"`
	IcUri       string `json:"ic_uri"`
	ThumbWidth  int    `json:"thumb_width"`
	ThumbHeight int    `json:"thumb_height"`
}

type CookieCheckRes struct {
	Message string `json:"message"`
}

type TtGallery struct {
	Title       string `json:"title"`
	Content     string `json:"content"`
	GalleryData []KV   `json:"gallery_data"`
	GalleryInfo []KV   `json:"gallery_info"`
}

type KV struct {
	K string
	V string
}

type GalleryContent struct {
	List []Gallery `json:"list"`
}
type Gallery struct {
	Url        string            `json:"url"`
	Uri        string            `json:"uri"`
	IcUri      string            `json:"ic_uri"`
	Product    map[string]string `json:"product"`
	Desc       string            `json:"desc"`
	WebUri     string            `json:"web_uri"`
	UrlPattern string            `json:"url_pattern"`
	GalleryId  int64             `json:"gallery_id"`
}

type ImageRes struct {
	Url      string `json:"url"`
	Original string `json:"original"`
}

/*
{
  "message": "success",
  "data": "",
  "is_uniq": true
}
*/
type CommonResponse struct {
	Message string `json:"message"`
	Data    string `json:"data"`
}

/*
{
  "code": 0,
  "message": "success",
  "reason": "success",
  "expect_bytes": -1,
  "bytes": -1,
  "poster_uri": "",
  "poster_url": "",
  "crc32": 3372599280,
  "url": ""
}
*/
type UploadResponse struct {
	Code        int    `json:"code"`
	Message     string `json:"message"`
	Reason      string `json:"reason"`
	ExpectBytes int    `json:"expect_bytes"`
	Bytes       int    `json:"bytes"`
	PosterUri   string `json:"poster_uri"`
	PosterUrl   string `json:"poster_url"`
	Crc32       int64  `json:"crc_32"`
	Url         string `json:"url"`
}

type ImagesResponse struct {
	Message string       `json:"message"`
	Now     int64        `json:"now"`
	Data    []VideoImage `json:"data"`
}

type VideoImage struct {
	Width  int    `json:"width"`
	Uri    string `json:"uri"`
	SrcUri string `json:"src_uri"`
	Height int    `json:"height"`
}

type PubVideo struct {
	Title        string `json:"title"`
	Desc         string `json:"abstract"`
	Tag          string `json:"tag"`
	Content      string `json:"content"`
	ArticleLabel string `json:"article_label"`
}

type PubVideoResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Data    struct {
		PgcId string `json:"pgc_id"`
	} `json:"data"`
}

type UploadApiResponse struct {
	Message string `json:"message"`
	Data    string `json:"data"`
}

type UploadIdUrl struct {
	UploadId  string `json:"upload_id"`
	UploadUrl string `json:"upload_url"`
}

/*
{
  "e": "开始上传",
  "code": 0,
  "ref_id": "241b7c02adea443b9f9b6bb9921d895b",
  "at": 1515484312089,
  "timestamp": 1515484312309,
  "f": "video.mp4",
  "url": "https://mp.toutiao.com/profile_v2/pure-video",
  "ubs": 0,
  "fs": 1802403,
  "log_id": null,
  "via": null,
  "text": "",
  "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
  "cookie": "_ba=BA0.2-20170904-5110e-CLLCw3dl47MBMiB6pq6o; _ga=GA1.2.2036107782.1506495296; _gid=GA1.2.484462943.1506495296; _mp_test_key_1=3b91d911faca77dbf3bb188a1726915c; sessionid=e861a703ceb877e6b4f82e6256b2d197; sid_tt=e861a703ceb877e6b4f82e6256b2d197; sso_login_status=1; tt_im_token=1506496291875471141961843798504776807301264187389659962927428561; tt_webid=63355290510; UM_distinctid=15ec21df6f3665-0ac72978743fb-31637e01-1fa400-15ec21df6f4870; uuid=\"w:1d36c55cb6474503964416cb0534accd\"; currentMediaId=1575972737903629; _ga=GA1.3.2036107782.1506495296; _gid=GA1.3.484462943.1506495296; ptcn_no=705d3950502ba97ce890de14bf2aecff",
  "log_data": [

  ],
  "username": "情感话篓子",
  "mid": 1575972737903629
}
*/
type PreUploadLog struct {
	E         string           `json:"e"`
	Code      int              `json:"code"`
	RefId     string           `json:"ref_id"`
	At        int64            `json:"at"`
	Timestamp int64            `json:"timestamp"`
	F         string           `json:"f"`
	Url       string           `json:"url"`
	Ubs       int              `json:"ubs"`
	Fs        int64            `json:"fs"`
	LogId     *string          `json:"log_id"`
	Via       *string          `json:"via"`
	Text      string           `json:"text"`
	Ua        string           `json:"ua"`
	Cookie    string           `json:"cookie"`
	LogData   []UploadResponse `json:"log_data"`
	Username  string           `json:"username"`
	Mid       int64            `json:"mid"`
}

type EndUploadLog struct {
	E         string           `json:"e"`
	Ts        int64            `json:"ts"`
	Te        int64            `json:"te"`
	RefId     string           `json:"ref_id"`
	At        int64            `json:"at"`
	Timestamp int64            `json:"timestamp"`
	F         string           `json:"f"`
	Url       string           `json:"url"`
	Ubs       int              `json:"ubs"`
	Fs        int64            `json:"fs"`
	LogId     *string          `json:"log_id"`
	Via       *string          `json:"via"`
	Text      string           `json:"text"`
	Ua        string           `json:"ua"`
	Cookie    string           `json:"cookie"`
	LogData   []UploadResponse `json:"log_data"`
	Username  string           `json:"username"`
	Mid       int64            `json:"mid"`
}
