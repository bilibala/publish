# 上传图片

```
Request URL:https://mp.toutiao.com/tools/upload_picture/?type=ueditor&pgc_watermark=1&action=uploadimage&encode=utf-8
Request Method:POST

Content-Type:multipart/form-data; boundary=----WebKitFormBoundaryJzQvxA2lQFLPnlaE

------WebKitFormBoundaryJzQvxA2lQFLPnlaE
Content-Disposition: form-data; name="upfile"; filename="bg.png"
Content-Type: image/png


------WebKitFormBoundaryJzQvxA2lQFLPnlaE--

{
  "title": "",
  "url": "https://p2.pstatp.com/large/50aa0001dc70b8e579e6",
  "height": 1586,
  "width": 2800,
  "state": "SUCCESS",
  "original": "50aa0001dc70b8e579e6"
}

```

# 发表 图集
```
https://mp.toutiao.com/core/article/edit_article_post/?source=mp&type=figure
Request Method:POST
Content-Type:application/x-www-form-urlencoded; charset=UTF-8

abstract:
authors:
self_appoint:0
save:1
pgc_id:
urgent_push:0
is_draft:
title:七个你不知道的小姑娘
content:{!-- PGC_GALLERY:{"list":[{"url":"https://p3.pstatp.com/large/50aa0001dc70b8e579e6","uri":"50aa0001dc70b8e579e6","ic_uri":"","product":{},"desc":"一个小姑娘1","web_uri":"50aa0001dc70b8e579e6","url_pattern":"{{image_domain}}","gallery_id":413361512274},{"url":"https://p3.pstatp.com/large/50a7000251e223fb9eb7","uri":"50a7000251e223fb9eb7","ic_uri":"","product":{},"desc":"一个小姑娘2","web_uri":"50a7000251e223fb9eb7","url_pattern":"{{image_domain}}","gallery_id":1330688536890},{"url":"https://p3.pstatp.com/large/50aa0001e11aa713ca65","uri":"50aa0001e11aa713ca65","ic_uri":"","product":{},"desc":"一个小姑娘3","web_uri":"50aa0001e11aa713ca65","url_pattern":"{{image_domain}}","gallery_id":831429985872},{"url":"https://p3.pstatp.com/large/50a60002617b039eb2b5","uri":"50a60002617b039eb2b5","ic_uri":"","product":{},"desc":"一个小姑娘4","web_uri":"50a60002617b039eb2b5","url_pattern":"{{image_domain}}","gallery_id":1059452555761}]} --}
gallery_data[413361512274][url]:https://p3.pstatp.com/large/50aa0001dc70b8e579e6
gallery_data[413361512274][uri]:50aa0001dc70b8e579e6
gallery_data[413361512274][ic_uri]:
gallery_data[413361512274][desc]:一个小姑娘1
gallery_data[413361512274][web_uri]:50aa0001dc70b8e579e6
gallery_data[413361512274][url_pattern]:{{image_domain}}
gallery_data[413361512274][gallery_id]:413361512274
gallery_data[1330688536890][url]:https://p3.pstatp.com/large/50a7000251e223fb9eb7
gallery_data[1330688536890][uri]:50a7000251e223fb9eb7
gallery_data[1330688536890][ic_uri]:
gallery_data[1330688536890][desc]:一个小姑娘2
gallery_data[1330688536890][web_uri]:50a7000251e223fb9eb7
gallery_data[1330688536890][url_pattern]:{{image_domain}}
gallery_data[1330688536890][gallery_id]:1330688536890
gallery_data[831429985872][url]:https://p3.pstatp.com/large/50aa0001e11aa713ca65
gallery_data[831429985872][uri]:50aa0001e11aa713ca65
gallery_data[831429985872][ic_uri]:
gallery_data[831429985872][desc]:一个小姑娘3
gallery_data[831429985872][web_uri]:50aa0001e11aa713ca65
gallery_data[831429985872][url_pattern]:{{image_domain}}
gallery_data[831429985872][gallery_id]:831429985872
gallery_data[1059452555761][url]:https://p3.pstatp.com/large/50a60002617b039eb2b5
gallery_data[1059452555761][uri]:50a60002617b039eb2b5
gallery_data[1059452555761][ic_uri]:
gallery_data[1059452555761][desc]:一个小姑娘4
gallery_data[1059452555761][web_uri]:50a60002617b039eb2b5
gallery_data[1059452555761][url_pattern]:{{image_domain}}
gallery_data[1059452555761][gallery_id]:1059452555761
gallery_info[0][url]:https://p3.pstatp.com/large/50aa0001dc70b8e579e6
gallery_info[0][uri]:50aa0001dc70b8e579e6
gallery_info[0][ic_uri]:
gallery_info[0][desc]:一个小姑娘1
gallery_info[0][web_uri]:50aa0001dc70b8e579e6
gallery_info[0][url_pattern]:{{image_domain}}
gallery_info[0][gallery_id]:413361512274
gallery_info[1][url]:https://p3.pstatp.com/large/50a7000251e223fb9eb7
gallery_info[1][uri]:50a7000251e223fb9eb7
gallery_info[1][ic_uri]:
gallery_info[1][desc]:一个小姑娘2
gallery_info[1][web_uri]:50a7000251e223fb9eb7
gallery_info[1][url_pattern]:{{image_domain}}
gallery_info[1][gallery_id]:1330688536890
gallery_info[2][url]:https://p3.pstatp.com/large/50aa0001e11aa713ca65
gallery_info[2][uri]:50aa0001e11aa713ca65
gallery_info[2][ic_uri]:
gallery_info[2][desc]:一个小姑娘3
gallery_info[2][web_uri]:50aa0001e11aa713ca65
gallery_info[2][url_pattern]:{{image_domain}}
gallery_info[2][gallery_id]:831429985872
gallery_info[3][url]:https://p3.pstatp.com/large/50a60002617b039eb2b5
gallery_info[3][uri]:50a60002617b039eb2b5
gallery_info[3][ic_uri]:
gallery_info[3][desc]:一个小姑娘4
gallery_info[3][web_uri]:50a60002617b039eb2b5
gallery_info[3][url_pattern]:{{image_domain}}
gallery_info[3][gallery_id]:1059452555761
article_ad_type:2
recommend_auto_analyse:0
article_type:3
timer_status:0
timer_time:2017-12-26 04:56
from_diagnosis:0
pgc_debut:0

{
  "message": "提交成功",
  "code": 0,
  "data": {
    "pgc_id": "6503407523708535309"
  }
}

```

# 发表视频
## 上传视频md5
```
Request URL:https://mp.toutiao.com/video/video_uniq_api/?md5=d43dec1b62083bb1412ff45edc38ca8c
Request Method:GET

{
  "message": "success",
  "data": "",
  "is_uniq": true
}
```
## 获取上传地址
```
Request URL:https://mp.toutiao.com/video/video_api/
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8

json_data:{"api":"chunk_upload_info"}

{
  "url": "http://videoapi.byted.org/video/v2/api/chunk_upload_info/1/pgc?start_transcoder=0",
  "message": "success",
  "data": "{\"code\":0,\"message\":\"success\",\"reason\":\"success\",\"upload_url\":\"http://i.snssdk.com/video/v2/chunk_upload/1/pgc/1515484312/897835754f2cac25f8b7d622168252a6/241b7c02adea443b9f9b6bb9921d895b\",\"upload_id\":\"241b7c02adea443b9f9b6bb9921d895b\"}"
}
```
## 上传前准备

```
Request URL:https://i.snssdk.com/video/fedata/1/pgc/241b7c02adea443b9f9b6bb9921d895b
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8

log:{"e":"开始上传","code":0,"ref_id":"241b7c02adea443b9f9b6bb9921d895b","at":1515484312089,"timestamp":1515484312309,"f":"video.mp4","url":"https://mp.toutiao.com/profile_v2/pure-video","ubs":0,"fs":1802403,"log_id":null,"via":null,"text":"","ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36","cookie":"_ba=BA0.2-20170904-5110e-CLLCw3dl47MBMiB6pq6o; _ga=GA1.2.2036107782.1506495296; _gid=GA1.2.484462943.1506495296; _mp_test_key_1=3b91d911faca77dbf3bb188a1726915c; sessionid=e861a703ceb877e6b4f82e6256b2d197; sid_tt=e861a703ceb877e6b4f82e6256b2d197; sso_login_status=1; tt_im_token=1506496291875471141961843798504776807301264187389659962927428561; tt_webid=63355290510; UM_distinctid=15ec21df6f3665-0ac72978743fb-31637e01-1fa400-15ec21df6f4870; uuid=\"w:1d36c55cb6474503964416cb0534accd\"; currentMediaId=1575972737903629; _ga=GA1.3.2036107782.1506495296; _gid=GA1.3.484462943.1506495296; ptcn_no=705d3950502ba97ce890de14bf2aecff","log_data":[],"username":"情感话篓子","mid":1575972737903629}

{"message": "success", "code": 0}
```

## 上传视频

```
Request URL:https://i.snssdk.com/video/v2/chunk_upload/1/pgc/1515484312/897835754f2cac25f8b7d622168252a6/241b7c02adea443b9f9b6bb9921d895b
Request Method:POST

Content-Length:1802591
Content-Range:bytes 0-1802402/1802403
Content-Type:multipart/form-data; boundary=----WebKitFormBoundaryDv6duUrLgGl7ppUT

------WebKitFormBoundaryDv6duUrLgGl7ppUT
Content-Disposition: form-data; name="video_file"; filename="video.mp4"
Content-Type: video/mp4


------WebKitFormBoundaryDv6duUrLgGl7ppUT--

{"code":0,"message":"success","reason":"success","expect_bytes":-1,"bytes":-1,"poster_uri":"","poster_url":"","crc32":3372599280,"url":""}
```
## 上传视频完成
```
Request URL:https://i.snssdk.com/video/fedata/1/pgc/241b7c02adea443b9f9b6bb9921d895b
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8

log:{"e":"文件上传成功","ts":1515484312227,"te":1515484315927,"ref_id":"241b7c02adea443b9f9b6bb9921d895b","at":1515484312089,"timestamp":1515484315927,"f":"video.mp4","url":"https://mp.toutiao.com/profile_v2/pure-video","ubs":0,"fs":1802403,"log_id":"","via":"","text":"{\"code\":0,\"message\":\"success\",\"reason\":\"success\",\"expect_bytes\":-1,\"bytes\":-1,\"poster_uri\":\"\",\"poster_url\":\"\",\"crc32\":3372599280,\"url\":\"\"}","ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36","cookie":"_ba=BA0.2-20170904-5110e-CLLCw3dl47MBMiB6pq6o; _ga=GA1.2.2036107782.1506495296; _gid=GA1.2.484462943.1506495296; _mp_test_key_1=3b91d911faca77dbf3bb188a1726915c; sessionid=e861a703ceb877e6b4f82e6256b2d197; sid_tt=e861a703ceb877e6b4f82e6256b2d197; sso_login_status=1; tt_im_token=1506496291875471141961843798504776807301264187389659962927428561; tt_webid=63355290510; UM_distinctid=15ec21df6f3665-0ac72978743fb-31637e01-1fa400-15ec21df6f4870; uuid=\"w:1d36c55cb6474503964416cb0534accd\"; currentMediaId=1575972737903629; _ga=GA1.3.2036107782.1506495296; _gid=GA1.3.484462943.1506495296; ptcn_no=705d3950502ba97ce890de14bf2aecff","log_data":[{"code":0,"message":"success","reason":"success","expect_bytes":-1,"bytes":-1,"poster_uri":"","poster_url":"","crc32":3372599280,"url":""}],"username":"情感话篓子","mid":1575972737903629}

{"message": "success", "code": 0}
```
## 获取视频中图片
```
Request URL:https://mp.toutiao.com/video/video_system_thumb/?vid=241b7c02adea443b9f9b6bb9921d895b&item_id=
Request Method:GET

可在 https://p1.pstatp.com/origin 查看

{
  "message": "success",
  "now": 1515484547,
  "data": [
    {
      "width": 480,
      "uri": "56470001626f509ca883",
      "src_uri": "5641000d5ff14abc4b80",
      "height": 480
    },
    {
      "width": 480,
      "uri": "56460001e8f9d7ff54b6",
      "src_uri": "56460001e8fa20a922b1",
      "height": 480
    }
  ],
  "reason": ""
}
```
## 确定视频封面
```
Request URL:https://mp.toutiao.com/video/video_thumb_report/?vid=241b7c02adea443b9f9b6bb9921d895b&poster_uri=56470001626f509ca883&poster_type=1
Request Method:GET

{
  "message": "success",
  "now": 1515484572,
  "data": "",
  "reason": ""
}
```
## 发表
```
Request URL:https://mp.toutiao.com/core/article/edit_article_post/?source=mp&type=purevideo
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8

article_ad_type:2
title:泰国十大头条新闻 整容、美女与旅游是主旋律
abstract:泰国十大头条新闻 整容、美女与旅游是主旋律
tag:video_funny
extern_link:
is_fans_article:0
content:<p>{!-- PGC_VIDEO:{"sp":"toutiao","vid":"241b7c02adea443b9f9b6bb9921d895b","vu":"241b7c02adea443b9f9b6bb9921d895b","thumb_url":"56470001626f509ca883","src_thumb_uri":"5641000d5ff14abc4b80","vname":"video.mp4"} --}</p>
add_third_title:0
timer_status:0
timer_time:2018-01-10 03:51
recommend_auto_analyse:0
article_label:美女
from_diagnosis:0
article_type:1
praise:0
pgc_debut:0
save:1

{
  "message": "提交成功",
  "code": 0,
  "data": {
    "pgc_id": "6508957003472175623"
  }
}
```
