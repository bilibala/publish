package dayu

import "fmt"

type UrlResponse struct {
	Status int    `json:"status"`
	Url    string `json:"url"`
	Src    string `json:"src"`
}

type ImageResponse struct {
	Result struct {
		Src    string `json:"src"` //cover 使用
		Url    string `json:"url"` // 图集 imgs[1][url]使用
		Format string `json:"format"`
		Width  string `json:"width"`
		Height string `json:"height"`
		Size   string `json:"size"`
	} `json:"result"`
}

/*
{
  "data": {
    "_created_at": "2017-12-26T17:55:11.000+0800",
    "_id": "9b0850f12856447ca1edb59e8099437d",
    "_updated_at": "2017-12-26T17:55:11.000+0800",
    "wm_update_at": "2017-12-26 17:55:11",
    "previewUrl": "https://mobile.dayu.com/preview.html?type=1&wm_aid=9b0850f12856447ca1edb59e8099437d&ts=1514282111350&acl=86cd71ee7e847f9c86fe7aa412cdb507&uc_biz_str=S:custom%7CC:iflow_wm2&uc_param_str=frdnsnpfvecpntnwprdssskt"
  }
}
*/
type PubResponse struct {
	Error string `json:"error"`
	Data struct {
		Id         string `json:"_id"`
		PreviewUrl string `json:"preview_url"`
	}
}

type DArticle struct {
	Author      string `json:"author"`
	Title       string `json:"title"`
	Content     string `json:"content"`
	CoverImages string `json:"cover_images"`
}

type Gallery struct {
	Title    string `json:"title"`
	CoverImg string `json:"cover_img"`
	Images   []KV   `json:"images"`
}

type KV struct {
	K string
	V string
}

func Imgwrapper(url string, desc string) string {
	return fmt.Sprintf(`<p class="imgbox"><img class="normal" width="500px" data-loadfunc="0" src="%s" data-loaded="0" uploaded="1" data-infoed="1" data-width="500" data-height="334" data-format="JPEG" data-size="27175"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">%s</p>`, url, desc)
}

func Pwrappper(text string) string {
	return fmt.Sprintf(`<p>%s</p>`, text)
}
