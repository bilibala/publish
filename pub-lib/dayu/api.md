
# 上传图片 通过Url [完成]

```
Request URL:https://mp.dayu.com/upload-image?src=https%3A%2F%2Fss1.baidu.com%2F6ONXsjip0QIZ8tyhnq%2Fit%2Fu%3D2451079327%2C3438915779%26fm%3D173%26s%3D6562BB4603150FD65671E50F0100B081%26w%3D640%26h%3D345%26img.JPEG&_=1514280979653
Request Method:GET

{"status":200,"url":"http://image.uc.cn/s/wemedia/s/2017/7120e4992507da50eed68af15c1c69c0x640x419x18.jpeg","src":"https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=1014629068,921022377&fm=173&s=69FE87444E432D5D14D4D99103004089&w=640&h=419&img.JPEG"}
```

# 裁剪 图片 [完成]

```
Request URL:https://mp.dayu.com/imagecut
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8
utoken:f81b25fd8d21da999f6032d9a5591b00

src:http://image.uc.cn/s/wemedia/s/2017/201643779c76a61c44d5df1552e24280x640x397x26.jpeg
x:0
y:35
ow:640
oh:361
w:640
h:361
utoken:f81b25fd8d21da999f6032d9a5591b00
（另一张）
src:https://mp.dayu.com/images/f36068d1cd0a42378b06a4f7c798d760029806422bbeaf01539b37f9b87424dd1514282298227
x:0
y:29
ow:554
oh:312
w:554
h:312
utoken:f81b25fd8d21da999f6032d9a5591b00

{"status":200,"url":"http://image.uc.cn/s/wemedia/s/upload/2017/fecfc5d6e7de4c21a39d67128e7784f0x640x361x25.jpeg"}

```

# 保存草稿（发表文章 步骤1） [完成]

```
Request URL:https://mp.dayu.com/dashboard/save-draft
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8
utoken:f81b25fd8d21da999f6032d9a5591b00

isPaste:1
draft_id:
article_id:
title:13岁谈恋爱，30多岁当奶奶！这些亮瞎你眼的奇葩事，俄罗斯才有！
content:<p class="imgbox"><img class="normal" width="500px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/6ab31b53c9c3acfa1bbef05d0700f0eex500x334x27.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="500" data-height="334" data-format="JPEG" data-size="27175"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第一怪：姑娘们身披“破麻袋”</p><p>俄罗斯无疑是个盛产美女的地方。一些去过俄罗斯的朋友总会感叹，只在那里的大街上走着走着不经意间就会屡屡惊艳得不能自拔……漂亮的披肩给美女们又添上了浓浓的斯拉夫民族风情和更多的风采。也不知道有些中国人是调侃这些美女，还是受武侠小说中丐帮的影响，</p><p>他们管如此漂亮的披肩叫做破麻袋！爱美之心，人人皆有。尽管这么说，可是越来越多的中国人也加入了丐帮的行列，都披起了“麻袋”。</p><p class="imgbox"><img class="normal" width="500px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/58e9a9cbdab60b15c226bc0aeaade745x500x422x31.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="500" data-height="422" data-format="JPEG" data-size="30730"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第二怪：帅哥们头顶大锅盖</p><p>俄罗斯正规军、内卫部队、警察以及其它强力部门的军官都戴大沿帽。特工部门的军官平时不穿军装，但每个人家里都有一套军装，正装是大锅盖是必须的。在世界各国军队中，俄军的大沿帽是最大的，特别是俄罗斯海军的大沿帽，大得有些夸张，小个子军官戴在头上就像扣了个巨大的锅盖似的，根本不成比例。</p><p class="imgbox"><img class="normal" width="500px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/adef906c6d1dfe7c8552b4d401a7a970x500x333x30.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="500" data-height="333" data-format="JPEG" data-size="29814"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第三怪：俄罗斯青草白雪盖</p><p>俄罗斯的秋天非常短，冬天却来得非常快。北部10月份就开始下雪了，这时候青草还没有变枯。而盖上这层厚厚的白雪棉被以后，整个冬天草依然是绿油油的。直到来年5月雪全部化光，我们又能见到白雪被子下面的青草。并不是青草不想变脸，而是俄罗斯的老天爷不给它们机会。</p><p class="imgbox"><img class="normal" width="490px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/555dd016a0d24a4aa40317deb434ab97x490x326x41.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="490" data-height="326" data-format="JPEG" data-size="41905"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第四怪：干活的全是老太太</p><p>在俄罗斯各地铁站、火车站、汽车站的清洁工是老太太，在商店、公园、饭店扫地的也是老太太，在博物馆、图书馆看门、值班的仍然是老太太，就连乡村路边出售自家土特产的还是老太太。</p><p class="imgbox"><img class="normal" width="360px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/c161c0879bab467e2235e45c9e54c7fex360x240x14.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="360" data-height="240" data-format="JPEG" data-size="13496"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第五怪：12、13岁谈恋爱</p><p>俄罗斯是个人口严重缺乏的国家，目前只有1.43亿居民，且新生儿出生率连年下降，人口总数每年递减75万……若再不采取紧急措施，到2050年，这个泱泱大国就只剩下8000万人口，恐怕连保证最基本的劳动力都成问题。这叫普京如何不挠头？为增加人口，俄罗斯国家杜马新通过的婚姻法规定，俄罗斯公民14岁就可结婚，并且结婚手续相当简单，甚至无需经过双方家长的同意。</p><p>法律一经颁布，立刻引起了俄罗斯社会各界的强烈反响，各式各样的说法都有。俄罗斯少年才不管那么多呢，既然14岁就可以入洞房了，12、13岁谈恋爱还早吗？</p><p class="imgbox"><img class="normal" width="450px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/98515c3a714754c061da721b80bda987x450x288x28.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="450" data-height="288" data-format="JPEG" data-size="28667"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第六怪：30多岁当奶奶</p><p>14岁结婚，15岁生女儿。14年后女儿14岁结婚，15岁生孙女。看来30岁当奶奶，数学上，法律上都是行的通的。很可能有很多人见到过穿超短裙的奶奶！</p><p class="imgbox"><img class="normal" width="500px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/330bf024e54df10b1fe29b17840b6358x500x332x30.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="500" data-height="332" data-format="JPEG" data-size="29947"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第七怪：姑娘大腿露在外</p><p>俄罗斯有三宝：伏特加，巧克力，美女。不管多冷的天，俄罗斯姑娘都喜欢穿裙子。有些耐寒美女她们裙子的长短和温度的高低成反比。见了这些露大腿的美女大吃一惊，哭笑不得，感叹不已，人和人的差距咋就这么大呢？气温降到零下20多度，在倩影，美腿，短裙，靓女，寒流的共同作用下，足以让你达到流鼻血的程度，不过这在俄罗斯以不足为怪了。</p><p class="imgbox"><img class="normal" width="500px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/e3b8c283e6ec7c3a4cc105fdab7a5ef1x500x333x19.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="500" data-height="333" data-format="JPEG" data-size="19339"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第八怪：人高马大床很窄</p><p>别看俄罗斯人普遍长得比较高大，但他们睡觉的床却特别窄。据说，彼得大帝身高二米多，睡的也是一张小床。</p><p>原来，俄罗斯人喜欢趴着睡觉。这样一来，人高马大的问题就解决了。只要上床后不乱折腾就不会出问题，但外国人却很不习惯，许多人都有过夜里掉下床的经历。</p><p class="imgbox"><img class="normal" width="361px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/6094654971c730ca680a9f1e079a0dddx361x240x21.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="361" data-height="240" data-format="JPEG" data-size="20555"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第九怪：拉达跑的比奔驰快</p><p>俄罗斯人常常埋怨国内的道路坏。既然路不好，把车开得慢一点总可以吧。可早在19世纪俄罗斯作家果戈里就说过：“哪个俄罗斯男人不喜欢开快车！”</p><p>俄罗斯人一坐到方向盘后，一脚油门下去，就是90迈以上。目前，新型拉达车在俄罗斯只卖7000美元，可有些俄罗斯人买不起，只能“坚持”用旧型号的，而新俄罗斯人又觉得开这种车寒碜，只买奔驰、奥迪等进口车。所以街头常常可以看到一副“</p><p>贫富分化”严重的汽车追逐赛。你别小看这些二三十年前产的、脏兮兮的拉达车，由于它们的主人大多车破人胆大，撞了大不了不要破车了，所以抢起路来常把豪华的奔驰车甩到后面。</p><p class="imgbox"><img class="normal" width="360px" data-loadfunc="0" src="http://image.uc.cn/s/wemedia/s/2017/b4d4c0172fc0d82b252b25f7b4a030d0x360x221x13.jpeg" data-loaded="0" uploaded="1" data-infoed="1" data-width="360" data-height="221" data-format="JPEG" data-size="12611"></p><p class="imgbox_desc" style="color: #999;text-align:center;font-size:12px">请点击此处输入图片描述</p><p>第十怪：路上要烟那是不见外</p><p>在大街上，不管认识不认识，只要口袋里没带烟，俄罗斯人就会很自然地向路上行人要烟抽。这时，俄罗斯的烟友也会乐哈哈地掏出烟来送给对方。最近几年，少女吸烟成了俄罗斯的一大时尚。别看她们打扮入时，在公园和大街上向行人（包括外国人）要烟时一点都不脸红</p><p class="empty"><br></p>
author:永恒的梅罗
coverImg:http://image.uc.cn/s/wemedia/s/upload/2017/2736afb5081b9ce98ff011f08d66fb5bx360x203x13.jpeg
article_type:1
aoyun:false
weixin_promote:false
is_original:0
article_activity_title:
article_activity_id:
open_award:0
open_reproduce:0
is_show_ad:false
defaultAuthor:false
curDaySubmit:false
simpleMode:true
is_timed_release:false
time_for_release:1514288889403
keyword:
useTime:16062
utoken:f81b25fd8d21da999f6032d9a5591b00


{"data":{"_created_at":"2017-12-26T17:55:11.000+0800","_id":"9b0850f12856447ca1edb59e8099437d","_updated_at":"2017-12-26T17:55:11.000+0800","wm_update_at":"2017-12-26 17:55:11","previewUrl":"https://mobile.dayu.com/preview.html?type=1&wm_aid=9b0850f12856447ca1edb59e8099437d&ts=1514282111350&acl=86cd71ee7e847f9c86fe7aa412cdb507&uc_biz_str=S:custom%7CC:iflow_wm2&uc_param_str=frdnsnpfvecpntnwprdssskt"}}

```

# 提交草稿  [完成]

```
Request URL:https://mp.dayu.com/dashboard/submit-article
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8
utoken:f81b25fd8d21da999f6032d9a5591b00

dataList[0][_id]:9b0850f12856447ca1edb59e8099437d
dataList[0][isDraft]:1
dataList[0][reproduce]:
curDaySubmit:false
utoken:f81b25fd8d21da999f6032d9a5591b00

{"data":{"_created_at":"2017-12-26T17:55:43.000+0800","_id":"06ae478d4396428c9a86aed527fe226b","_updated_at":"2017-12-26T17:55:43.000+0800"}}
```


# 上传图片 （图集）  [完成]

```
Request URL:https://mp.dayu.com/imageupload
Request Method:POST

Content-Type:multipart/form-data; boundary=----WebKitFormBoundary1UI7rfCVjmHSkncC

------WebKitFormBoundary1UI7rfCVjmHSkncC
Content-Disposition: form-data; name="id"

WU_FILE_0
------WebKitFormBoundary1UI7rfCVjmHSkncC
Content-Disposition: form-data; name="name"

qiche1.jpg
------WebKitFormBoundary1UI7rfCVjmHSkncC
Content-Disposition: form-data; name="type"

image/jpeg
------WebKitFormBoundary1UI7rfCVjmHSkncC
Content-Disposition: form-data; name="lastModifiedDate"

Tue Dec 26 2017 16:17:37 GMT+0800 (CST)
------WebKitFormBoundary1UI7rfCVjmHSkncC
Content-Disposition: form-data; name="size"

24292
------WebKitFormBoundary1UI7rfCVjmHSkncC
Content-Disposition: form-data; name="upfile"; filename="qiche1.jpg"
Content-Type: image/jpeg


------WebKitFormBoundary1UI7rfCVjmHSkncC--


{"result":{"format":"JPEG","height":"369","maincolor":"","md5":"","phash":"","size":"23212","status":200,"width":"554","src":"http://image.uc.cn/s/wemedia/s/upload/2017/c11505d90b6253e3905da0fe1b590f75x554x369x23.jpeg","_id":"bd0a6817469e4dc38c13dbf0d970d646","url":"https://mp.dayu.com/images/bd0a6817469e4dc38c13dbf0d970d646c4ac669fd4113db2e7d73f46dbd85fec1514282298208"}}

```

# 保存草稿 （图集）[完成]

```
Request URL:https://mp.dayu.com/dashboard/save-draft
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8
utoken:f81b25fd8d21da999f6032d9a5591b00

article_category:2
draft_id:
description_type:1
title:一定令你一生难忘的7辆车
imgs[0][url]:https://mp.dayu.com/images/bd0a6817469e4dc38c13dbf0d970d646c4ac669fd4113db2e7d73f46dbd85fec1514282298208
imgs[0][format]:JPEG
imgs[0][height]:369
imgs[0][width]:554
imgs[0][phash]:
imgs[0][size]:23212
imgs[0][description]:汽车
imgs[0][disabled]:false
imgs[1][url]:https://mp.dayu.com/images/f36068d1cd0a42378b06a4f7c798d760029806422bbeaf01539b37f9b87424dd1514282298227
imgs[1][format]:JPEG
imgs[1][height]:369
imgs[1][width]:554
imgs[1][phash]:
imgs[1][size]:25595
imgs[1][description]:汽车
imgs[1][disabled]:false
imgs[2][url]:https://mp.dayu.com/images/422a84aa48ad4395af99723f65c8279064b1b5570f1cf23b918874cb0725976c1514282298261
imgs[2][format]:JPEG
imgs[2][height]:369
imgs[2][width]:554
imgs[2][phash]:
imgs[2][size]:23920
imgs[2][description]:汽车
imgs[2][disabled]:false
imgs[3][url]:https://mp.dayu.com/images/fff1fb5c4a8e45c1b67763e96665fd1a1addd3dff10310ddf9df4fe4aeb211671514282298541
imgs[3][format]:JPEG
imgs[3][height]:369
imgs[3][width]:554
imgs[3][phash]:
imgs[3][size]:27231
imgs[3][description]:汽车
imgs[3][disabled]:false
imgs[4][url]:https://mp.dayu.com/images/1bb4942b12c3408cbda87eb5b88e396a2184d44eccfeda2942059781d5ba884c1514282298609
imgs[4][format]:JPEG
imgs[4][height]:369
imgs[4][width]:554
imgs[4][phash]:
imgs[4][size]:26786
imgs[4][description]:汽车
imgs[4][disabled]:false
imgs[5][url]:https://mp.dayu.com/images/47c99f275bda41678f0db7b785f37a50cec1e84d43316ac17d50ae57cd406d6e1514282298597
imgs[5][format]:JPEG
imgs[5][height]:369
imgs[5][width]:554
imgs[5][phash]:
imgs[5][size]:27452
imgs[5][description]:汽车
imgs[5][disabled]:false
coverImg:http://image.uc.cn/s/wemedia/s/upload/2017/f17b93ba758891b98d7a41ea94c1f9a6x554x312x23.jpeg
article_type:1
category:
utoken:f81b25fd8d21da999f6032d9a5591b00

{"data":{"_created_at":"2017-12-26T18:01:16.000+0800","_id":"7e1dca14765d43e29e01a2416b282ae4","_updated_at":"2017-12-26T18:01:16.000+0800","wm_update_at":"2017-12-26 18:01:15","previewUrl":"https://mobile.dayu.com/preview_gallery.html?type=1&wm_aid=7e1dca14765d43e29e01a2416b282ae4&ts=1514282476232&acl=33ea858fddc13a852eac2d29064e3762&uc_biz_str=S:custom%7CC:iflow_wm2&uc_param_str=frdnsnpfvecpntnwprdssskt"}}
```

# 发表草稿 （图集）

```
Request URL:https://mp.dayu.com/dashboard/submit-fastfish
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8
utoken:f81b25fd8d21da999f6032d9a5591b00

dataList[0][_id]:7e1dca14765d43e29e01a2416b282ae4
dataList[0][isDraft]:1
utoken:f81b25fd8d21da999f6032d9a5591b00
```

# 视频分类

```
Request URL:https://mp.dayu.com/video-category?_=0.5291215361294415
Request Method:GET

["社会","国内","国际","体育","科技","娱乐","军事","财经","汽车","房产","时尚","健康","两性情感","游戏","动漫","旅游","美食","历史","奇闻","科学探索","星座","育儿","教育","美女","搞笑","演讲","萌娃","萌宠","音乐","语言类","记录短片","涨姿势","劲爆体育","幽默","综艺","电视剧","电影","纪录片","少儿"]
```

# 获取上传 sign
```
Request URL:https://mp.dayu.com/get_sign?callback=jQuery321024552927665809543_1515568970928&appid=wemedia&uid=480834e921d44984bbe4153cdf85f2e8&filetype=video&_=1515568970929
Request Method:GET

jQuery321024552927665809543_1515568970928({"code":0,"message":"success","data":{"appid":"wemedia","uid":"480834e921d44984bbe4153cdf85f2e8","tm":1515568978,"filetype":"video","policy":"eyJtYXhfc2l6ZSI6MTA3Mzc0MTgyMzl9","sign":"1079c84080314f5bec84d7b42eb9ee95"}})
```

# 上传视频
```
Request URL:https://ums-origin-file.oss-cn-hangzhou.aliyuncs.com/video/wemedia/480834e921d44984bbe4153cdf85f2e8/159332aa-f5d7-11e7-93fe-6c92bf56a71c?uploads=
Request Method:POST

Accept:*/*
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Authorization:OSS ekpXlLJcqDAgShrJ:WEAgZEqecRyQkfXHrkhBEpTXsE8=
Cache-Control:no-cache
Connection:keep-alive
Content-Length:0
Content-Type:application/octet-stream
Host:ums-origin-file.oss-cn-hangzhou.aliyuncs.com
Origin:https://mp.dayu.com
Pragma:no-cache
Referer:https://mp.dayu.com/dashboard/video/write?spm=a2s0i.db_article_write.menu.4.b0c538aEkgu5N
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
utoken:32590d511a948e0a2d0e65a19eaea42f
X-DevTools-Request-Id:2959.85
x-oss-date:Wed, 10 Jan 2018 07:22:58 GMT


<?xml version="1.0" encoding="UTF-8"?>
<InitiateMultipartUploadResult>
  <Bucket>ums-origin-file</Bucket>
  <Key>video/wemedia/480834e921d44984bbe4153cdf85f2e8/159332aa-f5d7-11e7-93fe-6c92bf56a71c</Key>
  <UploadId>86F83BAE67924DC08150436D9958D7F8</UploadId>
</InitiateMultipartUploadResult>

```

```
Request URL:https://ums-origin-file.oss-cn-hangzhou.aliyuncs.com/video/wemedia/480834e921d44984bbe4153cdf85f2e8/159332aa-f5d7-11e7-93fe-6c92bf56a71c?uploadId=86F83BAE67924DC08150436D9958D7F8&partNumber=1
Request Method:PUT

Accept:*/*
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Authorization:OSS ekpXlLJcqDAgShrJ:bj+HTwPKuUuYyRZA4VuBYfm1bpQ=
Cache-Control:no-cache
Connection:keep-alive
Content-Length:2428722
Content-Type:application/octet-stream
Expires:600000000
Host:ums-origin-file.oss-cn-hangzhou.aliyuncs.com
Origin:https://mp.dayu.com
Pragma:no-cache
Referer:https://mp.dayu.com/dashboard/video/write?spm=a2s0i.db_article_write.menu.4.b0c538aEkgu5N
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
X-DevTools-Request-Id:2959.88
x-oss-date:Wed, 10 Jan 2018 07:22:58 GMT

200
```

```
Request URL:https://ums-origin-file.oss-cn-hangzhou.aliyuncs.com/video/wemedia/480834e921d44984bbe4153cdf85f2e8/159332aa-f5d7-11e7-93fe-6c92bf56a71c?&uploadId=86F83BAE67924DC08150436D9958D7F8&max-parts=1000&part-number-marker=0
Request Method:GET

Accept:*/*
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Authorization:OSS ekpXlLJcqDAgShrJ:k3lsZGH2C3QxKRrwZ6ptNrENnFc=
Cache-Control:no-cache
Connection:keep-alive
Content-Type:application/octet-stream
Host:ums-origin-file.oss-cn-hangzhou.aliyuncs.com
Pragma:no-cache
Referer:https://mp.dayu.com/dashboard/video/write?spm=a2s0i.db_article_write.menu.4.b0c538aEkgu5N
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
X-DevTools-Request-Id:2959.90
x-oss-date:Wed, 10 Jan 2018 07:23:03 GMT

<?xml version="1.0" encoding="UTF-8"?>
<ListPartsResult>
  <Bucket>ums-origin-file</Bucket>
  <Key>video/wemedia/480834e921d44984bbe4153cdf85f2e8/159332aa-f5d7-11e7-93fe-6c92bf56a71c</Key>
  <UploadId>86F83BAE67924DC08150436D9958D7F8</UploadId>
  <StorageClass>Standard</StorageClass>
  <PartNumberMarker>0</PartNumberMarker>
  <NextPartNumberMarker>1</NextPartNumberMarker>
  <MaxParts>1000</MaxParts>
  <IsTruncated>false</IsTruncated>
  <Part>
    <PartNumber>1</PartNumber>
    <LastModified>2018-01-10T07:23:02.000Z</LastModified>
    <ETag>"186FEA8C1E3CC508672B26B9B04F03AF"</ETag>
    <Size>2428722</Size>
  </Part>
</ListPartsResult>
```

```
Request URL:https://ums-origin-file.oss-cn-hangzhou.aliyuncs.com/video/wemedia/480834e921d44984bbe4153cdf85f2e8/159332aa-f5d7-11e7-93fe-6c92bf56a71c?uploadId=86F83BAE67924DC08150436D9958D7F8&callback=eyJjYWxsYmFja1VybCI6Imh0dHA6XC9cL2VuZ2luZS51bXMudWMuY25cL3ZpZGVvXC91cGxvYWRfbm90aWNlP2xvY2F0aW9uPW9zcy1jbi1oYW5nemhvdSZmaWxlX3R5cGU9dmlkZW8iLCJjYWxsYmFja0JvZHkiOiJvYmplY3Q9JHtvYmplY3R9JnNpemU9JHtzaXplfSZtaW1lVHlwZT0ke21pbWVUeXBlfSZidWNrZXQ9JHtidWNrZXR9JmV0YWc9JHtldGFnfSJ9
Request Method:POST

Accept:*/*
Accept-Encoding:gzip, deflate
Accept-Language:zh-CN
Authorization:OSS ekpXlLJcqDAgShrJ:4v5iwGXspp8BznruaM5JagMDrXw=
Cache-Control:no-cache
Connection:keep-alive
Content-Length:137
Content-Type:application/octet-stream
Host:ums-origin-file.oss-cn-hangzhou.aliyuncs.com
Origin:https://mp.dayu.com
Pragma:no-cache
Referer:https://mp.dayu.com/dashboard/video/write?spm=a2s0i.db_article_write.menu.4.b0c538aEkgu5N
User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36
utoken:32590d511a948e0a2d0e65a19eaea42f
X-DevTools-Request-Id:2959.92
x-oss-date:Wed, 10 Jan 2018 07:23:03 GMT

<CompleteMultipartUpload><Part><PartNumber>1</PartNumber><ETag>"186FEA8C1E3CC508672B26B9B04F03AF"</ETag></Part></CompleteMultipartUpload>

{"code":0,"message":"","data":{"id":"62a87ec44a98517d"}}
```

```
Request URL:https://mp.dayu.com/confirm_info
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8

videoId:62a87ec44a98517d
videoName:5a54959fb63e01717_H264_3.mp4
videoDesc:
appid:wemedia
uid:480834e921d44984bbe4153cdf85f2e8
utoken:32590d511a948e0a2d0e65a19eaea42f

{"succeed":1}
```

```
Request URL:https://mp.dayu.com/dashboard/addMaterial
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8

type:video
title:5a54959fb63e01717_H264_3.mp4
video[video_id]:62a87ec44a98517d
utoken:32590d511a948e0a2d0e65a19eaea42f

{"data":{"_created_at":"2018-01-10T15:23:03.000+0800","_id":"0ff6ed87196f462eac8f0c5221c85d84","_updated_at":"2018-01-10T15:23:03.000+0800"}}
```

```
Request URL:https://mp.dayu.com/dashboard/getVideoPlayUrl
Request Method:POST

Content-Type:application/x-www-form-urlencoded; charset=UTF-8

id:62a87ec44a98517d
utoken:32590d511a948e0a2d0e65a19eaea42f

{"playUrl":"https://v-ums.uc.cn/player/player.html?id=62a87ec44a98517d&controls=1&pb=b&tm=1515568984&appid=wemedia&sign=bebf3b3fb4a4338ff8dcb29a54b8427b"}
```

```
Request URL:https://mp.dayu.com/dashboard/getVideoResulotion?videoId=62a87ec44a98517d&_=0.34063615284542537
Request Method:GET

{"data":{"resulotion":"high"}}
```

```
Request URL:https://mp.dayu.com/dashboard/queryVideoStatus?videoId=62a87ec44a98517d&_=1515568970934
Request Method:GET

{"status":1,"thumbnail":"http://img.ums.uc.cn/smart_snapshot/1515568992-566790385-786122473","name":"5a54959fb63e01717_H264_3.mp4","duration":"15","playUrl":"https://v-ums.uc.cn/player/player.html?id=62a87ec44a98517d&controls=1&pb=b&tm=1515569001&appid=wemedia&sign=70a71fc078364006d327809dbc3da561"}
```
