package pub_lib

import (

	"publish/pub-lib/baijia"
	"publish/pub-lib/dayu"
	"publish/pub-lib/qier"
	"publish/pub-lib/toutiao"
)

const (
	Plat_Baijia  = "BAIJIA"
	Plat_Toutiao = "TOUTIAO"
	Plat_Dayu    = "DAYU"
	Plat_Qier    = "QIER"
)


type Platform interface {
	CheckCookie() error
	PublishArticle(cat string, tag string, feedArticle baijia.FeedArticle) error
	PublishGallery(cat string, tag string, feedArticle baijia.FeedArticle) error
	PublishVideo(cat string, tag string, feedArticle baijia.FeedArticle) error
}

func NewPlatform(plat string,cookie string, params map[string]string) Platform {

	switch plat {
	case Plat_Baijia:
		appid := params["appid"]
		return baijia.NewAppIdBaijiaPlat(appid, cookie)
	case Plat_Dayu:
		return dayu.NewDayuApi(cookie)
	case Plat_Qier:
		return qier.NewQierApi(cookie)
	case Plat_Toutiao:
		return toutiao.NewToutiaoApi(cookie, 0, "")
	}

	return nil
}
