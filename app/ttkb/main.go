package main

import (

	"publish/app/ttkb/http"
	"publish/app/ttkb/ttkb"
	"github.com/astaxie/beego"
	"time"
	"strconv"
)


func pullArticles(ty string,num int){
	defer func() {
		if err := recover(); err != nil {
			beego.Error("【Panic】",err)
		}
	}()

	for i := 0;i<num;i++{
		go func(){
			for{
				time.Sleep(time.Duration(SleepTime)*time.Second)
				ttkb.Ttkb(ty)
			}
		}()
	}
}

var SleepTime int

func main() {
	SleepTime = beego.AppConfig.DefaultInt("sleep_time",30)

	articleTypes,err := beego.AppConfig.GetSection("article_type")
	if err != nil{
		panic(err)
	}

	for ty,count := range articleTypes{
		countInt,err := strconv.Atoi(count)
		if err != nil{
			panic(err)
		}
		pullArticles(ty,countInt)
	}

	http.StartServer()
}