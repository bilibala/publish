package http

type Result struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

var (
	SuccessResult = Result{200, "success", nil}
)

func DataResult(data interface{}) Result {
	return Result{200, "ok", data}
}

var (
	BindErrorResult  = Result{-1, "请求参数解析错误", nil}
	ValidErrorResult = Result{-2, "参数校验错误", nil}
	UserErrorResult = Result{403, "用户未授权", nil}

	// -3 service err
)

func ErrorResult(err error) Result {
	return Result{-3, "error:" + err.Error(), nil}
}
