package http

import (
	"github.com/gin-gonic/gin"
)

func Router(engine *gin.Engine){
	router := engine.Group("/api")
	{
		group := router.Group("/article")
		group.GET("/list", Json(ListArticle))
		group.GET("/list/page", Json(ListArticleByPage))
		group.GET("/search/page", Json(SearchArticleByPage))
		group.POST("/update",Json(UpdateArticle))
	}
}
