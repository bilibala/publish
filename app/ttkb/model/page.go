package model

type Page struct {
	Page     int         `json:"page"`
	PageSize int         `json:"page_size"`
	Count    int64         `json:"count"`
	Data     interface{} `json:"data"`
}
