package ttkb

import (
	"fmt"
	"github.com/astaxie/beego/httplib"
	"publish/app/search"
	"publish/app/ttkb/model"
	"github.com/astaxie/beego"
	"strings"
)

type TtSearch struct {
	Data []model.Article `json:"newslist"`
}


func Ttkb(typ string) {
	api := fmt.Sprintf(`http://r.cnews.qq.com/getSubNewsChlidInterest?devid=860046037899335&appver=25_areading_3.3.1&chlid=%s&qn-sig=b07fe23c165c858d38f32ba972f3ccc1&qn-rid=b3f1c889-8a27-402c-9339-f87a9333546c&page=1`,typ)
	results := TtSearch{}
	err :=  httplib.Get(api).ToJSON(&results)
	if err != nil{
		beego.Error(err)
		return
	}
	if len(results.Data) == 0{
		beego.Warning(fmt.Sprintf("文章列表数量为%d   类型:",len(results.Data)),typ)
	}

	for _,result := range results.Data{
		if !CheckAuther(result.Chlname){
			beego.Warning("作者检查不通过",typ)
			continue
		}
		if result.RewardFlag == 1{
			beego.Warning("原创类型不通过",typ)
			continue
		}

		if search.ToutiaoSearch(result.Title) < 56{
			result.Type = typ
			if err := model.AddArticle(result);err != nil{
				beego.Error(err)
			}
			beego.Info("[发现新闻]：",result.Title)
		}
	}
}

var (
	WhiteWord = beego.AppConfig.Strings("white_authors")
)

func CheckAuther(text string) bool{
	if len(WhiteWord) < 1{
		return false
	}

	for _,w := range WhiteWord{
		if strings.Contains(text,w){
			beego.Error("含有敏感词汇:"+w)
			return false
		}
	}
	return true
}

