package main


import (
	"publish/pub-lib/baijia"
	"net/url"
	"github.com/astaxie/beego/httplib"
	"errors"
	"publish/account"
	"publish/pub-lib"
	"github.com/astaxie/beego"
)

func PraseArticle(articleUrl string) (*baijia.FeedArticle,error){
	api := `http://127.0.0.1:3333/article/feed?url=`+url.QueryEscape(articleUrl)
	feedArticle := baijia.FeedArticle{}
	if err := httplib.Get(api).ToJSON(&feedArticle); err != nil {
		return nil,err
	}

	if len(feedArticle.Title) == 0 || len(feedArticle.Feed) == 0{
		return nil,errors.New("解析内容为空："+api)
	}

	return &feedArticle,nil
}


func Publish(account account.Account,ty string,articleUrl string,cat string) error{
	article,err := PraseArticle(articleUrl)
	if err != nil{
		return err
	}

	task := pub_lib.Task{
		Article : *article,
		Type :ty,
		Category:cat,

		Plat:account.Plat,
		Cookies:account.Cookie,
		PlatParams:account.PlatParams,
	}

	return pub_lib.Publish(task)
}

func main() {
	users := []account.Account{
		//account.Touytiao_Pan,
		//account.Touytiao_Yi,
		account.Touytiao_Huang,
		//account.Qier_Pan,
		//account.Qier_Yi,
		//account.Dayu_Yi,
		//account.Dayu_Pan,
	}

	articles := []string{
		"https://kuaibao.qq.com/s/20180417A1OVDX00?refer=kb_news",
	}
	for _, user := range users {
		for _, article := range articles {
			if err := Publish(user, pub_lib.TYPE_NEWS, article, "娱乐"); err != nil {
				beego.Error("URL:", article)
				beego.Error("发布失败:", err)
			}
		}
	}
}