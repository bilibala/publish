package main

import (
	"publish/app/account/http"
	"publish/app/account/model"
	"publish/app/account/cron"
)

func main() {
	model.InitDB()
	cron.StartWorker()
	cron.StartAutoTaskWorker()
	cron.StartCrawlerWorker()
	http.StartServer()
}