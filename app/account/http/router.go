package http

import (
	"github.com/gin-gonic/gin"
	"publish/app/account/controller"
)

func Router(engine *gin.Engine){
	router := engine.Group("/api")

	{
		group := router.Group("/account")
		group.POST("/add", Json(controller.AddAccount))
		group.POST("/delete", Json(controller.DeleteAccount))
		group.POST("/update", Json(controller.UpdateAccount))
		group.GET("/list", Json(controller.ListAccount))
		group.GET("/list/page", Json(controller.ListAccountByPage))
		group.GET("/get", Json(controller.GetAccountById))

		group.GET("/today",Json(controller.ListAccountTodayRead))
	}

	{
		group := router.Group("/accountArticle")
		group.GET("/list", Json(controller.ListAccountArticle))
		group.GET("/list/page", Json(controller.ListAccountArticleByPage))
	}

	{
		group := router.Group("/task")
		group.POST("/add", Json(controller.AddTask))
		group.GET("/list", Json(controller.ListPubTask))
		group.GET("/list/page", Json(controller.ListPubTaskByPage))
		group.GET("/get", Json(controller.GetTask))
		group.POST("/callback", Json(controller.Callback))
		group.GET("/republish", Json(controller.RePublish))
	}

	{
		group := router.Group("/autoTask")
		group.POST("/add", Json(controller.AddAutoTask))
		group.POST("/delete", Json(controller.DeleteAutoTask))
		group.POST("/update", Json(controller.UpdateAutoTask))
		group.GET("/list", Json(controller.ListAutoTask))
		group.GET("/list/page", Json(controller.ListAutoTaskByPage))
		group.GET("/get", Json(controller.GetAutoTaskById))
	}
}
