package api_test

import (
	"testing"
	"github.com/astaxie/beego/httplib"
	"publish/model"
	"publish/pub-lib"
	"fmt"
)
var HOST = `http://127.0.0.1:7789`

func TestTask_Add(t *testing.T){
	task := model.PubTaskHttp{
		AccountId:10,
		ArticleCat:"娱乐",
		Type:pub_lib.TYPE_NEWS,
		ArticleUrl:"https://kuaibao.qq.com/s/20180418A058GX00?refer=kb_news",
	}
	req,err := httplib.Post(HOST+`/api/task/add`).JSONBody(task)
	if err != nil {
		panic(err)
	}
	fmt.Println(req.String())
}
