package controller

import (
	"github.com/gin-gonic/gin"
	"strconv"
	"publish/app/account/model"
	"publish/app/account/cron"
	"github.com/astaxie/beego"
)

func AddAccount(ctx *gin.Context) Result{

	account := &model.Account{}
	if err := ctx.BindJSON(account);err != nil{
		return BindErrorResult
	}

	err := model.AddAccount(account)
	if err != nil{
		return ErrorResult(err)
	}

	return DataResult(account)
}


func UpdateAccount(ctx *gin.Context) Result{

	account := &model.Account{}
	if err := ctx.BindJSON(account);err != nil{
		return BindErrorResult
	}

	err := model.UpdateCookie(*account)
	if err != nil{
		return ErrorResult(err)
	}

	go func(){
		account,err := model.GetAccountById(account.Id)
		if err != nil{
			return
		}
		cron.CheckAccount(*account)
	}()

	return DataResult(account)
}

func GetAccountById(ctx *gin.Context) Result{

	idQ := ctx.Query("id")
	id,_ := strconv.Atoi(idQ)
	if id <= 0 {
		return ValidErrorResult
	}

	nAccount,err := model.GetAccountById(id)
	if err != nil{
		return ErrorResult(err)
	}

	return DataResult(nAccount)
}

func DeleteAccount(ctx *gin.Context) Result{

	idReq := &IdReq{}
	if err := ctx.BindJSON(idReq);err != nil{
		return BindErrorResult
	}

	err := model.DeleteAccount(idReq.Id)
	if err != nil{
		return ErrorResult(err)
	}

	return SuccessResult
}


func ListAccount(ctx *gin.Context) Result{
	accounts,err := model.ListAccount()
	if err != nil {
		return ErrorResult(err)
	}

	return DataResult(accounts)
}

func ListAccountByPage(ctx *gin.Context) Result{

	pageQ := ctx.DefaultQuery("page","1")
	sizeQ := ctx.DefaultQuery("size","10")
	page,_ := strconv.Atoi(pageQ)
	size,_ := strconv.Atoi(sizeQ)

	result,count,err := model.ListAccountByPage(page,size)
	if err != nil{
		return ErrorResult(err)
	}

	return DataResult(model.Page{Page:page,PageSize:size,Count:count,Data:result})
}




func ListAccountArticle(ctx *gin.Context) Result{
	accountArticles,err := model.ListAccountArticle()
	if err != nil {
		return ErrorResult(err)
	}

	return DataResult(accountArticles)
}

func ListAccountArticleByPage(ctx *gin.Context) Result{

	pageQ := ctx.DefaultQuery("page","1")
	sizeQ := ctx.DefaultQuery("size","10")
	dateQ := ctx.DefaultQuery("date","")

	page,_ := strconv.Atoi(pageQ)
	size,_ := strconv.Atoi(sizeQ)

	account_idQ := ctx.DefaultQuery("account_id","0")
	account_id,_ := strconv.Atoi(account_idQ)

	result,count,err := model.ListAccountArticleByPage(dateQ,account_id,page,size)
	if err != nil{
		return ErrorResult(err)
	}

	return DataResult(model.Page{PageSize:size,Page:page,Count:count,Data:result})
}


func ListAccountTodayRead(ctx *gin.Context) Result{
	datas,err := model.TodayReads()
	if err != nil{
		beego.Error(err)
	}
	if len(datas) == 0{
		return DataResult(datas)
	}

	todays := map[string][]model.TodayRead{}

	for i,data := range datas{
		name := "头条-"+data.Nickname
		_,ok := todays[name]
		if ok{
			todays[name] = append(todays[name],datas[i])
		}else{
			todays[name] = []model.TodayRead{datas[i]}
		}
	}

	return DataResult(todays)
}


