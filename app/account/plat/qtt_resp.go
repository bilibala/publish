package plat

type QttResult struct {
	Code int `json:"code"`
	Message string `json:"message"`
}


type QttArticle struct {
	QttResult
	Data struct{
		Data []struct{
			Id string `json:"id"`
			Title string `json:"title"`
			Url string `json:"url"`
			Status string `json:"status"`   // 2 已发布
			PublishTime string `json:"publish_time"`
		} `json:"data"`
	} `json:"data"`
}


type QttStat struct {
	QttResult
	Data struct{
		Data []struct{
			Id string `json:"content_id"`
			CommentNum string `json:"comment_num"`
			ListPv string `json:"list_pv"`
			Pv string `json:"pv"`
			PublishTime string `json:"publish_time"`
		} `json:"data"`
	} `json:"data"`
}

type QttProfits struct {
	QttResult
	Data struct{
		Data struct {
			List []struct {
				Date            string `json:"date"`
				ActivityBalance string `json:"activity_balance"`
				Balance         string `json:"balance"`
			} `json:"list"`
		} `json:"data"`
	} `json:"data"`
}