package plat

import (
	"encoding/json"
)

type BaijiaResp struct {
	Errno  int
	Errmsg string
}

type BaijiaProfit struct {
	DayTime     string  `json:"day_time"`
	AdIncome    float64 `json:"ad_income"`
	VideoIncome float64 `json:"video_income"`
	TotalIncome float64 `json:"total_income"`
}

type BaijiaProfitResp struct {
	BaijiaResp
	Data struct {
		List      []BaijiaProfit `json:"list"`
		Count     int            `json:"count"`
		PageNum   json.Number    `json:"page_num"`
		PageCount int            `json:"page_count"`
	} `json:"data"`
}

type BaijiaStatistic struct {
	EventDay       string `json:"event_day"`
	RecommendCount int64  `json:"recommend_count"`
	CommentCount   int64  `json:"comment_count"`
	ViewCount      int64  `json:"view_count"`
}

type BaijiaStatisticResp struct {
	BaijiaResp
	Data struct {
		List []BaijiaStatistic `json:"list"`
	}
}

type BaijiahaoZhishu struct {
	Errno  int             `json:"errno"`
	Errmsg string          `json:"errmsg"`
	Data   json.RawMessage `json:"data"`
}
type Zhishu struct {
	Id         string `json:"id"`
	AppId      string `json:"app_id"`
	Active     string `json:"active"`
	Quality    string `json:"quality"`
	Original   string `json:"original"`
	Domain     string `json:"domain"`
	Interest   string `json:"interest"`
	Score      string `json:"score"`
	CreateTime string `json:"create_time"`
	CreateDate string `json:"create_date"`
	ScoreDate  string `json:"score_date"`
	Status     string `json:"status"`
}

type BaijiaAppuser struct {
	Userid   int64  `json:"userid"`
	Username string `json:"username"`
	Name     string `json:"name"`
	AppLevel int    `json:"app_level"`
}

type BaijiaAppinfoResp struct {
	BaijiaResp
	Data struct {
		User BaijiaAppuser `json:"user"`
	}
}

/*
	status:
		publish  已发布
		rejected 审核未通过
	bad_status:
		similar_1  内容重复
		similar_2  与百家号文章高度相似
	IsRecommend:
		1 被推荐
*/

type BaijiaList struct {
	Title       string `json:"title"`
	Url         string `json:"url"`
	Time        string `json:"publish_time"`
	Status      string `json:"status"`
	BadStatus   string `json:"bad_status"`
	IsRecommend int    `json:"is_rec"`
}

type BaijiaArticleListResp struct {
	BaijiaResp
	Data struct {
		List []BaijiaList `json:"list"`
	}
}

type BaiduFinancialInformation struct {
	RegisterName string `json:"registerName"`
	RegisterNum  string `json:"registerNumber"`
	AccTown      string `json:"accTown"`
	SubBank      string `json:"subBranchBank"`
	Bank         string `json:"payBank"`
	BankNumber   string `json:"account"`
	Contact      string `json:"contact"`
}

type BaijiaPay struct {
	Date              string  `json:"date"`
	MonthlyIncome     float64 `json:"monthlyincome"`
	ActualPay         float64 `json:"actualpay"`
	ActualPayAfterTax float64 `json:"actualpayaftertax"`
	Tax               float64 `json:"tax"`
	HavePay           float64 `json:"havepay"`
	Status            string  `json:"status"`
}

type BaijiaPays struct {
	TotalCount int         `json:"totalcount"`
	PageNo     int         `json:"pageno"`
	Results    []BaijiaPay `json:"results"`
}
