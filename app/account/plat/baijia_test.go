package plat

import (
	"fmt"
	"testing"
	"time"

	"publish/app/account/model"
)

func TestPlatbaijia_GetProfit(t *testing.T) {
	ck := `BAIDUID=44946A3AE73ED3712E0F238F0E4062F8:FG=1; BDUSS=zFsNURKSEc2TDdnaXhWYktraEg0dWxERkdCdmxyMXcyQmFjZkxyZjVLbll0aVJhSVFBQUFBJCQAAAAAAAAAAAEAAACPhpKqsLK2-cDWsabC6LfWz-0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANgp~VnYKf1ZN; FP_UID=2c391883a2743dd48b6801cf2b515f0c; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1516795108; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1516773463,1516795108; PMS_JT=%28%7B%22s%22%3A1516670101751%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/%22%7D%29; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1517807624; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1519453918`
	plat := newplat_baijia()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetProfit(account, time.Now().Add(-7*24*time.Hour), time.Now().Add(-24*time.Hour))
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}

}

func TestPlatbaijia_GetIndex(t *testing.T) {
	ck := `BAIDUID=44946A3AE73ED3712E0F238F0E4062F8:FG=1; BDUSS=zFsNURKSEc2TDdnaXhWYktraEg0dWxERkdCdmxyMXcyQmFjZkxyZjVLbll0aVJhSVFBQUFBJCQAAAAAAAAAAAEAAACPhpKqsLK2-cDWsabC6LfWz-0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANgp~VnYKf1ZN; FP_UID=2c391883a2743dd48b6801cf2b515f0c; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1516795108; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1516773463,1516795108; PMS_JT=%28%7B%22s%22%3A1516670101751%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/%22%7D%29; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1517807624; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1519453918`
	plat := newplat_baijia()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetIndex(account, time.Now().Add(-5*24*time.Hour), time.Now().Add(-24*time.Hour))
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}
}

func TestPlatbaijia_GetStatistic(t *testing.T) {
	ck := `BAIDUID=44946A3AE73ED3712E0F238F0E4062F8:FG=1; BDUSS=zFsNURKSEc2TDdnaXhWYktraEg0dWxERkdCdmxyMXcyQmFjZkxyZjVLbll0aVJhSVFBQUFBJCQAAAAAAAAAAAEAAACPhpKqsLK2-cDWsabC6LfWz-0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANgp~VnYKf1ZN; FP_UID=2c391883a2743dd48b6801cf2b515f0c; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1516795108; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1516773463,1516795108; PMS_JT=%28%7B%22s%22%3A1516670101751%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/%22%7D%29; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1517807624; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1519453918`
	plat := newplat_baijia()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetStatistic(account, time.Now().Add(-7*24*time.Hour), time.Now().Add(-24*time.Hour))
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}

}

func TestPlatbaijia_GetLevel(t *testing.T) {
	ck := `BAIDUID=44946A3AE73ED3712E0F238F0E4062F8:FG=1; BDUSS=zFsNURKSEc2TDdnaXhWYktraEg0dWxERkdCdmxyMXcyQmFjZkxyZjVLbll0aVJhSVFBQUFBJCQAAAAAAAAAAAEAAACPhpKqsLK2-cDWsabC6LfWz-0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANgp~VnYKf1ZN; FP_UID=2c391883a2743dd48b6801cf2b515f0c; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1516795108; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1516773463,1516795108; PMS_JT=%28%7B%22s%22%3A1516670101751%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/%22%7D%29; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1517807624; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1519453918`
	plat := newplat_baijia()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetLevel(account)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}

func TestPlatbaijia_GetArticles(t *testing.T) {
	ck := `BAIDUID=86C638421FF5EA82EB4DAD9B2B584FB9:FG=1; BDSTOKEN=980c351f1cd4cc78bb6ca89dbf29d3c9; BDUSS=lhxaXd0aE1GT2E0eXpYRVZtTzZDY2l0U0p1Wm10aEVvMnh6Tm9BM1BJYTMteUJhTVFBQUFBJCQAAAAAAAAAAAEAAADA7D-rwM~F7dPQwc8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALdu-Vm3bvlZQ; FP_UID=4853208d4a610b41b927a9f3c6091369; Hm_lpvt_192fa266ff34772c28e4ddb36b8f4472=1523151566; Hm_lpvt_f2ee7f5c2284ca4c112e62165bc44c75=1523157646; Hm_lvt_192fa266ff34772c28e4ddb36b8f4472=1523151566; Hm_lvt_f2ee7f5c2284ca4c112e62165bc44c75=1521963236; PMS_JT=%28%7B%22s%22%3A1513925757818%2C%22r%22%3A%22http%3A//baijiahao.baidu.com/builder/article/edit%3Ftype%3Dnews%26app_id%3D1557419590294635%22%7D%29`
	plat := newplat_baijia()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetArticles(account, time.Now().Add(-24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}

func TestPlatbaijia_GetFinancialInformation(t *testing.T) {
	ck := `BAIDUID=F99F47A95E823315F1D31B8A1E5CFC7C:FG=1; bdsjzrcg=esyysde; BDUSS=o1Z3dHd1pjYWNQRFFJa1VZOWJ5ZEh5NEU0NnZoaUI5T1ZROU5mZzQxS2J5S3RaTVFBQUFBJCQAAAAAAAAAAAEAAABroz2r1Ma2y7ChNTQzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJs7hFmbO4RZZW; FP_UID=2793c305330a4a6f8d137d5f83581756; Hm_lvt_a4e9fcdc4011da5b1276af5330f5aba2=1520325341,1520327982,1520328038,1520331456; Hm_lpvt_a4e9fcdc4011da5b1276af5330f5aba2=1520331456`
	plat := newplat_baijia()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetFinancialInformation(account)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}

func TestPlatbaijia_GetPayRecord(t *testing.T) {
	ck := `BAIDUID=F99F47A95E823315F1D31B8A1E5CFC7C:FG=1; bdsjzrcg=esyysde; BDUSS=o1Z3dHd1pjYWNQRFFJa1VZOWJ5ZEh5NEU0NnZoaUI5T1ZROU5mZzQxS2J5S3RaTVFBQUFBJCQAAAAAAAAAAAEAAABroz2r1Ma2y7ChNTQzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJs7hFmbO4RZZW; FP_UID=2793c305330a4a6f8d137d5f83581756; Hm_lvt_a4e9fcdc4011da5b1276af5330f5aba2=1520325341,1520327982,1520328038,1520331456; Hm_lpvt_a4e9fcdc4011da5b1276af5330f5aba2=1520331456`
	plat := newplat_baijia()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetPayRecord(account, time.Now().Add(-24*time.Hour*360), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}

func TestBTime(t *testing.T) {
	time, _ := time.Parse("20060102", "20171218")
	fmt.Println(time.Format("2006-01-02"))
}
