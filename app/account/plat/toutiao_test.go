package plat

import (
	"fmt"
	"testing"
	"time"

	"publish/app/account/model"
)

func TestPlattoutiao_GetIndex(t *testing.T) {
	plat := newplat_toutiao()
	ck := `_ba=BA0.2-20170811-5110e-yWPNLGHOW7wphA2sqeyc;_ga=GA1.3.2040819908.1505275146;_ga=GA1.2.1155985848.1510566076;_gid=GA1.3.1841272453.1505275146;_gid=GA1.2.1280088868.1510566076;currentMediaId=1575900376948750;login_flag=37271a5630ca5e047d1cb37326d27611;part=stable;sessionid=4633eb88928c1f8ef8863929c1eb0cc9;sessionid=4633eb88928c1f8ef8863929c1eb0cc9;sid_guard="4633eb88928c1f8ef8863929c1eb0cc9|1511752730|2592000|Wed\054 27-Dec-2017 03:18:50 GMT";sid_tt=4633eb88928c1f8ef8863929c1eb0cc9;sid_tt=4633eb88928c1f8ef8863929c1eb0cc9;sso_login_status=1;tt_im_token=1510901836118142143246666673091715539852181596743302845652312913;tt_webid=6487832135938917901;uid_tt=9c83745f014e43ea0c1edea0056541cc;UM_distinctid=15e7963f9780-0fb41cefc8125f-31637e01-1fa400-15e7963f97a34;uuid="w:df7c292a44c844a6a56f2ff814c0624d"`
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetIndex(account, time.Now().Add(-30*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}
}

func TestPlattoutiao_GetStatistic(t *testing.T) {
	plat := newplat_toutiao()
	ck := `UM_distinctid=162066540ce422-0952a88698350e-32607b05-13c680-162066540cf47c; tt_webid=6530608237027640846; _ga=GA1.2.1933610257.1520925359; uuid="w:283ce57018a64340ba4712f9ea8071dd"; login_flag=6b4d323f2814e0b5b4506b7f8cc662b1; sid_tt=02cc66d5d1f8a096eed66ac90e24d799; _ba=BA0.2-20180412-5110e-HrHuIRCQOLLuER8Z3vCS; sid_guard="02cc66d5d1f8a096eed66ac90e24d799|1523596490|15552000|Wed\054 10-Oct-2018 05:14:50 GMT"; _ga=GA1.3.1933610257.1520925359; __utma=68473421.1933610257.1520925359.1524300199.1524300199.1; __utmc=68473421; __utmz=68473421.1524300199.1.1.utmcsr=45.76.195.7:7788|utmccn=(referral)|utmcmd=referral|utmcct=/; _mp_test_key_1=8a7ce385a4662716007abe777f990ed9; CNZZDATA1272960458=1871275091-1523583039-%7C1524551480; CNZZDATA1273049018=1188707351-1523583568-%7C1524551882; ptcn_no=2069304e1c9cdb3cb852709d6fd72f54; _mp_auth_key=0aa3d1e1d363992ed4ebb113b2b4d4cd; sessionid=39e22928786c1a370b39bad57617e9dc; uid_tt=efc149191cd025ce04d7ac2d98e4be1c; __tea_sdk__user_unique_id=97300152812; __tea_sdk__ssid=3c424c61-e6c4-4d11-9c92-a82ec6fcbdb0; tt_im_token=1524553679051993198833929097322903097773768028764162494218111057`
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetEveryStatistic(account, time.Now().Add(-10*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}

	for _, data := range datas {
		fmt.Println(data)
	}
}

func TestPlattoutiao_GetTodayCount(t *testing.T) {
	plat := NewplatToutiao()
	ck := `UM_distinctid=162066540ce422-0952a88698350e-32607b05-13c680-162066540cf47c; tt_webid=6530608237027640846; _ga=GA1.2.1933610257.1520925359; uuid="w:283ce57018a64340ba4712f9ea8071dd"; login_flag=6b4d323f2814e0b5b4506b7f8cc662b1; sid_tt=02cc66d5d1f8a096eed66ac90e24d799; _ba=BA0.2-20180412-5110e-HrHuIRCQOLLuER8Z3vCS; sid_guard="02cc66d5d1f8a096eed66ac90e24d799|1523596490|15552000|Wed\054 10-Oct-2018 05:14:50 GMT"; _ga=GA1.3.1933610257.1520925359; __utma=68473421.1933610257.1520925359.1524300199.1524300199.1; __utmc=68473421; __utmz=68473421.1524300199.1.1.utmcsr=45.76.195.7:7788|utmccn=(referral)|utmcmd=referral|utmcct=/; _mp_test_key_1=8a7ce385a4662716007abe777f990ed9; CNZZDATA1272960458=1871275091-1523583039-%7C1524551480; CNZZDATA1273049018=1188707351-1523583568-%7C1524551882; ptcn_no=2069304e1c9cdb3cb852709d6fd72f54; _mp_auth_key=0aa3d1e1d363992ed4ebb113b2b4d4cd; sessionid=39e22928786c1a370b39bad57617e9dc; uid_tt=efc149191cd025ce04d7ac2d98e4be1c; __tea_sdk__user_unique_id=97300152812; __tea_sdk__ssid=3c424c61-e6c4-4d11-9c92-a82ec6fcbdb0; tt_im_token=1524553679051993198833929097322903097773768028764162494218111057`
	account := &model.Account{Id:1,Cookie:ck}
	data, err := plat.GetAllReadCount(account)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(data)
}

func TestPlattoutiao_GetLevel(t *testing.T) {
	plat := newplat_toutiao()
	ck := `_ba=BA0.2-20171018-5110e-HcxQnjO9eu9jeXHSyvMT; _ga=GA1.3.1097965537.1508293765; _ga=GA1.2.1097965537.1508293765; _gid=GA1.3.1134189261.1508293765; _gid=GA1.2.1134189261.1508293765; _mp_test_key_1=5619394a4f2055321dddcb7395db9106; sessionid=38495f1806cf18de0b1fdc0ee69b65d1; sid_tt=38495f1806cf18de0b1fdc0ee69b65d1; sso_login_status=1; tt_im_token=1508294785723839221643623948334120125109496999233381805669139181; tt_webid=64562980994; UM_distinctid=15f2d5069f8269-0f7cbd552fe0b7-31637e01-1fa400-15f2d5069f9763; uuid="w:959caae005f640f48b693bec6f56389f"; currentMediaId=1577688005306381`
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetLevel(account)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(datas)
}

func TestPlattoutiao_GetArticles(t *testing.T) {
	plat := newplat_toutiao()
	ck := `UM_distinctid=162066540ce422-0952a88698350e-32607b05-13c680-162066540cf47c; tt_webid=6530608237027640846; _ga=GA1.2.1933610257.1520925359; uuid="w:283ce57018a64340ba4712f9ea8071dd"; login_flag=6b4d323f2814e0b5b4506b7f8cc662b1; sid_tt=02cc66d5d1f8a096eed66ac90e24d799; _ba=BA0.2-20180412-5110e-HrHuIRCQOLLuER8Z3vCS; sid_guard="02cc66d5d1f8a096eed66ac90e24d799|1523596490|15552000|Wed\054 10-Oct-2018 05:14:50 GMT"; _mp_test_key_1=9fef6d5fbc47d0f65ce91c4aea93c249; CNZZDATA1272960458=1871275091-1523583039-%7C1523803981; CNZZDATA1273049018=1188707351-1523583568-%7C1523803733; _mp_auth_key=0aa3d1e1d363992ed4ebb113b2b4d4cd; sessionid=02cc66d5d1f8a096eed66ac90e24d799; uid_tt=d24c608441bca4a961d8d331b99f4502; __tea_sdk__user_unique_id=83313347361; __tea_sdk__ssid=c7ff7154-b969-4c01-b390-b898b8693113; tt_im_token=1523806808386358178113840076561724679981067645984370382232836525; ptcn_no=57ecf58a8f79b1961cea1bebd7b7121a; _mp_auth_key=5963197019c9031c75ced85a409562aa`
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetArticles(account, time.Now().Add(-30*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}

func TestTimeWeekday(t *testing.T) {
	fmt.Println(time.Now().Add(-time.Duration(time.Now().Weekday()) * 24 * time.Hour).Format("2006-01-2"))
}
func TestPlattoutiao_GetPayRecord(t *testing.T) {
	plat := newplat_toutiao()
	ck := `__tea_sdk__ssid=0; __tea_sdk__user_unique_id=66452518447; __tea_sdk__web_id=1892157440; _ba=BA0.2-20171003-5110e-4XjHcuuxjNAqoRpUJ02d; _ga=GA1.2.411019651.1518435545; _gat=1; _gid=GA1.2.1015518867.1518435545; _mp_test_key_1=187f337e3af24fe6fa00e4c5ec6cb59a; login_flag=b576d674943d17a5a1d30cc77f03b331; ptcn_no=886e9e03ea9b497449261d6a6a28133c; sessionid=673b701ec07c6b3557c0f2945a421d86; sid_guard="673b701ec07c6b3557c0f2945a421d86|1518435481|15552000|Sat\054 11-Aug-2018 11:38:01 GMT"; sid_tt=673b701ec07c6b3557c0f2945a421d86; sso_login_status=0; tt_im_token=1518435482701899365656398967999435129082314609409579568495289100; tt_webid=6521630636234688004; UM_distinctid=16189cfb6115af-030a15547ca434-31637e01-1fa400-16189cfb612a09; uuid="w:bedfcb9609ff40aaa4ce3ee232ec5b33"; currentMediaId=1575902040095758; _ga=GA1.3.411019651.1518435545; _gid=GA1.3.1015518867.1518435545; uid_tt=a50da6dd764c9c245a8a8179614fe1cc`
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetFinancialInformation(account)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}

func TestCheckCookie(t *testing.T){
	//ck := `UM_distinctid=162066540ce422-0952a88698350e-32607b05-13c680-162066540cf47c; tt_webid=6530608237027640846; _ga=GA1.2.1933610257.1520925359; uuid="w:283ce57018a64340ba4712f9ea8071dd"; login_flag=6b4d323f2814e0b5b4506b7f8cc662b1; sid_tt=02cc66d5d1f8a096eed66ac90e24d799; _ba=BA0.2-20180412-5110e-HrHuIRCQOLLuER8Z3vCS; sid_guard="02cc66d5d1f8a096eed66ac90e24d799|1523596490|15552000|Wed\054 10-Oct-2018 05:14:50 GMT"; _ga=GA1.3.1933610257.1520925359; __utma=68473421.1933610257.1520925359.1524300199.1524300199.1; __utmc=68473421; __utmz=68473421.1524300199.1.1.utmcsr=45.76.195.7:7788|utmccn=(referral)|utmcmd=referral|utmcct=/; _gid=GA1.3.504206611.1524577679; _gid=GA1.2.504206611.1524577679; CNZZDATA1273049018=1188707351-1523583568-%7C1524580627; CNZZDATA1272960458=1871275091-1523583039-%7C1524582410; _mp_test_key_1=fb0d2c565b42a5c71c0938affb6926ce; _mp_auth_key=0aa3d1e1d363992ed4ebb113b2b4d4cd; sessionid=aa062d07651dac54ad0f18838e48bda9; uid_tt=a8839cacd232e0a2188c2ccb25c1e2f0; __tea_sdk__user_unique_id=97421649153; __tea_sdk__ssid=3e29ac97-02e3-477d-8b67-b3f4f920481f; tt_im_token=1524582765075609905869124571252793983872044697524391952406885490`

}
