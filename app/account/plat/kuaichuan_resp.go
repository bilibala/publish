package plat

type KCList struct {
	Data struct{
		List []KCArticle `json:"list"`
	} `json:"data"`
	Errno int `json:"errno"`
	Msg string `json:"errmsg"`
}

type KCArticle struct {
	Title string `json:"title"`
	Status string `json:"status"`
	CreateTime string `json:"create_time"`
	PublishTime string `json:"publish_time"`
	ReadNum string `json:"read_num"`
	RecNum string `json:"rec_num"`
	Url string `json:"url"`
}

type KCStatisticResp struct {
	Data struct{
		ArticleList []KCStatistic `json:"article_list"`
	} `json:"data"`
}

type KCStatistic struct {
	Id string `json:"id"`
	ReadNum string `json:"read_num"`
	RecNum string `json:"rec_num"`
	ForwardNum string `json:"forward_num"`
	UpdateTime string `json:"update_time"`
}


type KCStatisticVideoResp struct {
	List []KCStatisticVideo `json:"list"`
}

type KCStatisticVideo struct {
	Id string `json:"id"`
	PublishTime string `json:"publish_time"`
	Score KCScore `json:"score"`
}

type KCScore struct {
	Pv int64 `json:"total_pv"`
	Read int64 `json:"total_click"`
}

type KCIndexResp struct {
	Data struct{
		Total int `json:"total"`
	}
	Errno int `json:"errno"`
	Msg string `json:"errmsg"`
}

type KCScoreResp struct {
	Data struct{
		Productivity int `json:"productivity"`
		Professional int `json:"professional"`
		Popularity int `json:"popularity"`
		Credit int `json:"credit"`
		Quality int `json:"quality"`
		UpdateTime string `json:"update_time"`
	}
	Errno int `json:"errno"`
	Msg string `json:"errmsg"`

}
