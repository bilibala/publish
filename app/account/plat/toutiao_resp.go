package plat

type ToutiaoProfit struct {
	Day           string  `json:"day"`
	Total         float64 `json:"total"`
	ActivityAward float64 `json:"activity_award"`
	TemaiIncome   float64 `json:"temai_income"`
	PraiseIncome  float64 `json:"praise_income"`
	ToutiaoAd     float64 `json:"ToutiaoAd"`
	VideoIncome   float64 `json:"video_income"`
	LiveIncome    float64 `json:"live_income"`
}

type ToutiaoProfitResp struct {
	Message string `json:"message"`
	Reason  string `json:"reason"`
	Data    struct {
		TotalNum     int             `json:"total_num"`
		TotalPagenum int             `json:"total_pagenum"`
		Detail       []ToutiaoProfit `json:"detail"`
	} `json:"data"`
}

type Level struct {
	Level string `json:"media_status_str"`
}

type ToutiaoList struct {
	Data struct {
		Articles []struct {
			Title          string `json:"title"`
			Url            string `json:"article_url"`
			Status         int    `json:"status"` //3 已发表
			StatusDesc     string `json:"status_desc"`
			WasRecommended int    `json:"was_recommended"`
			IsTopArticle   bool   `json:"is_top_article"`
			CreateTime     int64  `json:"create_time"`
			ItemId         string `json:"item_id"`

			ImpressionCount int `json:"impression_count"`// 推荐
			ReadCount int `json:"go_detail_count"`  // 阅读
			CommentCount   int `json:"comment_count"`  // 评论
			AdClickCount  int `json:"ad_click_count"`// 广告
			FavoriteCount  int `json:"favorite_count"` // 收藏
			ShareCount     int `json:"share_count"` // 分享
			SubscribeCount int `json:"subscribe_count"`  // 涨粉

		} `json:"articles"`
		LastTime int64 `json:"last_time"`
	} `json:"data"`
}

type ToutiaoPayRecord struct {
	Data struct {
		SubBankName    string `json:"bank_branch_name"`
		BankCardNumber string `json:"bank_card_no"`
		BankName       string `json:"bank_name"`
		Location       string `json:"bank_location_text"`
		PayName        string `json:"pay_name"`
	} `json:"data"`
}
