package plat


type DayuIndexs struct {
	Data []DayuIndex `json:"data"`
}

type DayuIndex struct {
	Score float64 `json:"total_score"`
	OriginalScore float64 `json:"original_score"` //原创指数
	HealthScore float64 `json:"health_score"` //质量指数
	UserScore float64 `json:"user_score"` //用户指数
	ActivityScore float64 `json:"activity_score"` //活跃指数
	VerScore float64 `json:"vertical_score"` //垂直指数
	CreateTime string `json:"created_at"`
}

type DayuStatistics struct {
	Data DayuStatisticsData `json:"data"`
}

type DayuStatisticsData struct {
	List []DayuStatistic `json:"list"`
}

type DayuStatistic struct {
	RecommendCount int64 `json:"show_pv"`
	ReadCount int64 `json:"click_pv"`
	Data string `json:"date"`
}

type DayuStatisticsVideo struct {
	Data DayuStatisticsVideaData `json:"data"`
}

type DayuStatisticsVideaData struct {
	List []DayuStatisticVideo `json:"list"`
}

type DayuStatisticVideo struct {
	ReadCount int64 `json:"video_vv"`
	Data string `json:"date"`
}


type DatyProfits struct {
	Data []DayuProfit `json:"data"`
}

type DayuProfit struct {
	Profit float64 `json:"pretax_amount"`
	Data int64 `json:"report_date"`
}

type DayuList struct {
	DataList struct{
		Data []struct{
			Title string `json:"title"`
			Time string `json:"publish_at"`
			Url string `json:"previewUrl"`
			Status int `json:"status"` // 1 已发布 2 待发布 3 审核中 4 发布失败 5 已下线
		} `json:"data"`
	} `json:"dataList"`
}

type DatyArticleList struct {
	Data []struct {
		Title  string `json:"title"`
		Time   string `json:"publish_at"`
		Url    string `json:"previewUrl"`
		Aid    string `json:"aid"`
		Status int    `json:"status"` // 1 已发布 2 待发布 3 审核中 4 发布失败 5 已下线

		ReadCount int `json:"click_pv"`
		RecommentCount int `json:"show_pv"`
		CommentCount int `json:"cmt_pv"`
		FavCount int `json:"fav_pv"`
	}
}

type DayuRealList struct {
	ArticleList  []struct {
		Title  string `json:"title"`
		Time   string `json:"publish_at"`
		ReadCount int `json:"read_times"`
	} `json:"articleList"`
}
