package plat

import (
	"fmt"
	"testing"
	"time"

	"publish/app/account/model"
)

const qttck = `aliyungf_tc=AQAAAIBbhlNuqQYApj2ee6oS0TZ1z4/l; CNZZDATA1264696607=2112618984-1522811531-%7C1523424448; token=29c7mpVWnNG4kGSAXnx5pMAfNBgHaD0BL3Ro5q1dfam5wHLskNXSOCqp55XdhMg5xLuDDAnfAwryrw; UM_distinctid=1628edc834b27e-012c4478f0ccb5-31637e01-100200-1628edc834d17a`

func TestPlatQtt_GetIndex(t *testing.T) {
	plat := newplat_qtt()
	account := &model.Account{Id:1,Cookie:qttck}
	datas, err := plat.GetIndex(account, time.Now().Add(-30*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}
}

func TestPlatQtt_GetStatistic(t *testing.T) {
	plat := newplat_qtt()
	account := &model.Account{Id:1,Cookie:qttck}
	datas, err := plat.GetStatistic(account, time.Now().Add(-10*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}
}

func TestPlatQtt_GetLevel(t *testing.T) {
	plat := newplat_qtt()
	account := &model.Account{Id:1,Cookie:qttck}
	datas, err := plat.GetLevel(account)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(datas)
}

func TestPlatQtt_GetArticles(t *testing.T) {
	plat := newplat_qtt()
	account := &model.Account{Id:1,Cookie:qttck}
	datas, err := plat.GetArticles(account, time.Now().Add(-30*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}

func TestPlatQtt_GetProfit(t *testing.T) {
	plat := newplat_qtt()
	account := &model.Account{Id:1,Cookie:qttck}
	datas, err := plat.GetProfit(account, time.Now().Add(-9*24*time.Hour), time.Now().Add(-24*time.Hour))
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}

}