package plat

import (
	"fmt"
	"testing"
	"time"

	"publish/app/account/model"
)

const ck = `__guid=65234612.1963430170157186300.1522244204503.9915; __sid=65234612.1818935457363505200.1522244205141.5752; monitor_count=2; opennews=1d085d4d99aad2e8; Q=u%3D360H3012378523%26n%3D%26le%3DoGR4ZQZ1BGZ4Zwp1WGDjZGLmYzAioD%3D%3D%26m%3DZGtkWGWOWGWOWGWOWGWOWGWOBQp3%26qid%3D3012378523%26im%3D1_t01923d359dad425928%26src%3Dpcw_so%26t%3D1; quCapStyle=2; quCryptCode=D6TqMQxFspl5hAIid28CshgIhgfOhcjV8B5iXIwuosiyEAdzarH%252B44wp%252FpVP7dIIFaAo0DNvqNo%253D; smidV2=201803282136125840735ddcccc24b0acc34c638634af100cc6568433c1dc00; T=s%3D386380c0625a21b7deee76fa5e15f9f3%26t%3D1522244227%26lm%3D%26lf%3D1%26sk%3D17daa65370cb17b3ea5a08969b488dec%26mt%3D1522244227%26rc%3D%26v%3D2.0%26a%3D1; test_cookie_enable=null; test_cookie_enable=null; monitor_count=29; __gid=65234612.43710993.1522244205151.1523414405579.39; __sid=65234612.1818935457363505200.1522244205141.5752`

func TestPlatKC_GetIndex(t *testing.T) {
	plat := newplat_kuaichuan()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetIndex(account, time.Now().Add(-30*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}
}

func TestPlatKC_GetStatistic(t *testing.T) {
	plat := newplat_kuaichuan()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetStatistic(account, time.Now().Add(-10*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	for _, data := range datas {
		fmt.Println(data)
	}
}

func TestPlatKC_GetLevel(t *testing.T) {
	plat := newplat_kuaichuan()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetLevel(account)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(datas)
}

func TestPlatKC_GetArticles(t *testing.T) {
	plat := newplat_kuaichuan()
	account := &model.Account{Id:1,Cookie:ck}
	datas, err := plat.GetArticles(account, time.Now().Add(-30*24*time.Hour), time.Now())
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(datas)
}