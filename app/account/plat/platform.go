package plat

import (
	"sync"
	"time"

	"publish/app/account/model"
)

const (
	LEVEL_NEW   = "新手"
	LEVEL_MONEY = "转正"
)

type Article struct {
	ArticleId   string `json:"article_id"`
	Title       string    `json:"title"`
	Url         string    `json:"url"`
	Time        time.Time `json:"time"`
	Status      int       `json:"status"`   // 1 已发布 2 待发布 3 审核中 4 发布失败 5 已下线
	FailMsg     string    `json:"fail_msg"` //文章相似 文章重复
	IsRecommend bool      `json:"is_recommend"`

	RecommentCount int `json:"recomment_count"`
	ReadCount int `json:"read_count"`
	CommentCount int `json:"comment_count"`
	FavCount int `json:"fav_count"`
}

type FinancialInformation struct {
	PeopleName        string `json:"people_name"`
	Code              string `json:"code"`
	BankAccountName   string `json:"bank_account_name"`
	BankAccountNumber string `json:"bank_account_number"`
	BankName          string `json:"bank_name"`
	Locaiton          string `json:"location"`
	SubBank           string `json:"sub_bank"`
}

type PayRecord struct {
	Date              string  `json:"date"`
	MonthlyIncome     float64 `json:"monthlyincome"`     //税前收入
	ActualPay         float64 `json:"actualpay"`         //实际支付金额
	ActualPayAfterTax float64 `json:"actualpayaftertax"` //实际支付金额
	Tax               float64 `json:"tax"`               //代扣税(元)
	HavePay           float64 `json:"havepay"`           //付款
	Status            string  `json:"status"`            //  |PAYED  空为计税中 PAYED为已支付
}

type Plat interface {
	GetProfit(account *model.Account, start, end time.Time) ([]AccountProfit, error)
	GetIndex(account *model.Account, start, end time.Time) ([]AccountData, error)
	GetStatistic(account *model.Account, start, end time.Time) ([]AccountStatisttic, error)
	GetEveryStatistic(account *model.Account, start, end time.Time) ([]EveryDayStatisttic, error)
	GetLevel(account *model.Account) (string, error)
	GetArticles(account *model.Account, start time.Time, end time.Time) ([]Article, error)
	GetPayRecord(account *model.Account, start time.Time, end time.Time) ([]PayRecord, error)
	GetFinancialInformation(account *model.Account) (*FinancialInformation, error)
}

var (
	global_plat = make(map[string]Plat)
	global_mtx  sync.Mutex
)

func RegisterPlat(name string, plat Plat) {
	global_mtx.Lock()
	defer global_mtx.Unlock()
	global_plat[name] = plat
}

func NewPlat(key string) Plat{
	global_mtx.Lock()
	defer global_mtx.Unlock()
	return global_plat[key]
}


type AccountProfit struct {
	AccountId int   `json:"-"`
	Profit    float64 `json:"profit"`
	Time      string  `json:"time"`
	Date      string  `json:"date"`
}

type AccountStatisttic struct {
	AccountId int  `json:"-"`
	Type      string `json:"type"`
	Num       int64  `json:"num"`
	Time      string `json:"time"`
	Date      string `json:"date"`
}

type EveryDayStatisttic struct {
	Date                         string `json:"date"`
	CommentCount                 int64  `json:"comment_count"`              //评论量
	ReadCount                    int64  `json:"read_count"`            //阅读量
	SaveCount                    int64  `json:"save_count"`                //收藏量
	RecommentCount               int64  `json:"impression_count"`                //推荐量
	ShareCount                   int64  `json:"share_count"`                     // 转发量
}

type AccountData struct {
	AccountId int       `json:"-"`
	Index     string      `json:"index"`
	Date      string      `json:"date"`
	Time      string      `json:"time"`
	IndexJson interface{} `json:"index_json"`
}

