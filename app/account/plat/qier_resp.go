package plat

import "encoding/json"

type QierResp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type QierProfit struct {
	Date           string      `json:"date"`
	AdvertAmount   json.Number `json:"advert_amount"`
	VideoAmount    json.Number `json:"video_amount"`
	GiftAmount     json.Number `json:"gift_amount"`
	ActivityAmount json.Number `json:"activity_amount"`
	TotalAmount    json.Number `json:"total_amount"`
}

type QierProfitResp struct {
	QierResp
	Data []QierProfit `json:"data"`
}

type QierStatistic struct {
	StatisticDate   string `json:"statistic_date"`
	Exposure        int64  `json:"exposure"`
	Read            int64  `json:"read"`
	Relay           int64  `json:"relay"`
	Collect         int    `json:"collect"`
	ExposureArticle int    `json:"exposure_article"`
}

type QierStatisticResp struct {
	QierResp
	Data struct {
		Total     int             `json:"total"`
		TotalPage int             `json:"totalPage"`
		Statistic []QierStatistic `json:"statistic"`
	} `json:"data"`
}

type QierVideoStat struct {
	Date               string      `json:"date"`
	NewDailyPlayPv     json.Number `json:"new_daily_play_pv"`
	InewsKuaibaoPlayPv json.Number `json:"inews_kuaibao_play_pv"`
	OOtherPlayPv       json.Number `json:"o_other_play_pv"`
	QbPlayPv           json.Number `json:"qb_play_pv"`
	KuaibaoPlayPv      json.Number `json:"kuaibao_play_pv"`
	InewsPlayPv        json.Number `json:"inews_play_pv"`
	LivePlayPv         json.Number `json:"live_play_pv"`
	QzonePlayPv        json.Number `json:"qzone_play_pv"`
}

type QierVideoStatResp struct {
	QierResp
	Data struct {
		Total int             `json:"total"`
		List  []QierVideoStat `json:"list"`
	} `json:"data"`
}

type QierIndex struct {
	Date       string
	Total      json.Number `json:"total"`
	Favorite   json.Number `json:"favorite"`
	Creativity json.Number `json:"creativity"`
	Activity   json.Number `json:"activity"`
	Influence  json.Number `json:"influence"`
	Reward     json.Number `json:"reward"`
}

type QierIndexResp struct {
	QierResp
	Data struct {
		Result []QierIndex `json:"result"`
	} `json:"data"`
}


type QierList struct {
	Data struct{
		Articles []struct{
			ArticleId string `json:"articleId"`
			Title string `json:"title"`
			Url string `json:"url"`
			Date string `json:"pubTime"`
			Status string `json:"status"`

			RecommentCount int `json:"exposure"`
			ReadCount int `json:"read"`
			CommentCount int `json:"relay"`
			FavCount int `json:"collect"`
		} `json:"statistic"`
	} `json:"data"`
}

type QierPays struct {
	Data struct{
		List []struct{
			CreationTime string `json:"creation_time"`
			TotalAmount string `json:"total_amount"`
			State string `json:"state"`
		} `json:"list"`
	} `json:"data"`
}