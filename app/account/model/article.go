package model

import (
	"time"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
	"fmt"
)

type AccountArticle struct {
	Id int  `orm:"column(id)" json:"id"`

	AccountId   int `orm:"column(account_id);index" json:"account_id"`
	AccountName string `orm:"column(account_name);index" json:"account_name"`
	Plat string `orm:"column(plat);index" json:"plat"`

	ArticleId   string `orm:"column(article_id);size(128);unique" json:"article_id"`
	Title       string `orm:"column(title)" json:"title"`
	Url         string `orm:"column(url)" json:"url"`
	Time        time.Time `orm:"column(time);index" json:"time"`
	Status      int 		`orm:"column(status)" json:"status"`   // 1 已发布 2 待发布 3 审核中 4 发布失败 5 已下线
	FailMsg     string `orm:"column(fail_msg)" json:"fail_msg"` //文章相似 文章重复

	RecommentCount int `orm:"column(recomment_count)" json:"recomment_count"`
	ReadCount int `orm:"column(read_count)" json:"read_count"`
	CommentCount int `orm:"column(comment_count)" json:"comment_count"`
	FavCount int `orm:"column(fav_count)" json:"fav_count"`

	UpdateTime time.Time `orm:"column(update_time)" json:"update_time"`
}


func InsertOrUpdate(article AccountArticle) {
	o := orm.NewOrm()
	readSql := `select * from account_article where article_id = ?`
	newArticle := AccountArticle{}
	article.UpdateTime = time.Now()
	if err := o.Raw(readSql,article.ArticleId).QueryRow(&newArticle);err !=nil{
		_,err := o.Insert(&article)
		if err != nil{
			beego.Error(err)
			return
		}
	}

	article.Id = newArticle.Id
	_,err := o.Update(&article)
	if err != nil{
		beego.Error(err)
		return
	}
}


func ListAccountArticle()  ([]AccountArticle,error){
	o := orm.NewOrm()
	accountArticles :=  []AccountArticle{}
	s := "select * from account_article"
	_,err := o.Raw(s).QueryRows(&accountArticles)
	return accountArticles,err
}

func ListAccountArticleByPage(date string,accountId int,page int,size int)  (accountArticles []AccountArticle,count int,err error){
	o := orm.NewOrm()
	if accountId == 0{
		if len(date) > 0{
			s := fmt.Sprintf("select * from account_article where DATE_FORMAT(time, '%s') = '%s' order by TO_DAYS(time) desc,read_count desc limit %d,%d","%Y-%m-%d",date, (page-1)*size, size)
			fmt.Println(s)
			_, err = o.Raw(s).QueryRows(&accountArticles)
			if err != nil {
				return nil, 0, err
			}
			sql := fmt.Sprintf(`select count(*) from account_article where DATE_FORMAT(time, '%s') = '%s' `,"%Y-%m-%d",date)
			err = o.Raw(sql).QueryRow(&count)
			if err != nil {
				return nil, 0, err
			}
		}else {
			s := "select * from account_article order by TO_DAYS(time) desc,read_count desc limit ?,?"
			_, err = o.Raw(s, (page-1)*size, size).QueryRows(&accountArticles)
			if err != nil {
				return nil, 0, err
			}
			sql := `select count(*) from account_article`
			err = o.Raw(sql).QueryRow(&count)
			if err != nil {
				return nil, 0, err
			}
		}
	}else{
		if len(date) > 0{
			s := fmt.Sprintf("select * from account_article where account_id = ? and DATE_FORMAT(time, '%s') = ? order by TO_DAYS(time) desc,read_count desc limit ?,?","%Y-%m-%d")
			_,err = o.Raw(s,accountId,date, (page-1)*size, size).QueryRows(&accountArticles)
			if err != nil{
				return nil,0,err
			}
			sql := fmt.Sprintf(`select count(*) from account_article where account_id = ? and DATE_FORMAT(time, '%s') = ?`,"%Y-%m-%d")
			err = o.Raw(sql,accountId,date).QueryRow(&count)
			if err != nil{
				return nil,0,err
			}
		}else{
			s := "select * from account_article where account_id = ? order by TO_DAYS(time) desc,read_count desc limit ?,?"
			_,err = o.Raw(s,accountId, (page-1)*size, size).QueryRows(&accountArticles)
			if err != nil{
				return nil,0,err
			}
			sql := `select count(*) from account_article where account_id = ?`
			err = o.Raw(sql,accountId).QueryRow(&count)
			if err != nil{
				return nil,0,err
			}
		}

	}

	return accountArticles,count,err
}


