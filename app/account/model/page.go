package model

type Page struct {
	Page     int         `json:"page"`
	PageSize int         `json:"page_size"`
	Count    int         `json:"count"`
	Data     interface{} `json:"data"`
}
