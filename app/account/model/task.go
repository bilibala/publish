package model

import (
	"time"
	"github.com/astaxie/beego/orm"
)

type Task struct {
	Id int `json:"id"`
	Title string `json:"title"`
	Account Account `json:"account"`
	Status int `json:"status"`
	Result string `json:"result"`
	CreateTime time.Time `json:"create_time"`
}

type Article struct {
	Id int       `orm:"column(id)" json:"article_id"`
	Title string `orm:"column(title)" json:"title"`
	Url string   `orm:"column(url);unique;" json:"url"`
	Type string  `orm:"column(type)" json:"type"`
	Status int `orm:"column(status)" json:"status"`
	CreateTime time.Time `orm:"column(create_time)" json:"create_time"`
}

func GetArticle(ty string) (article Article,err error){
	o := orm.NewOrm()
	if err := o.Begin();err != nil{
		return article,err
	}

	sql := `select * from article where type = ? and status = 0 order by id desc limit 1 for update`
	if err = o.Raw(sql,ty).QueryRow(&article);err != nil{
		o.Rollback()
		return article,err
	}
	article.Status = 1
	if _,err = o.Update(&article);err != nil{
		o.Rollback()
		return article,err
	}
	o.Commit()
	return article,nil
}


type AutoTask struct {
	Id int `orm:"column(id)" json:"id"`

	AccountId int `orm:"column(account_id)" json:"account_id"`
	Account *Account `orm:"-" json:"account"`

	ArticleCat string `orm:"column(article_cat)" json:"article_cat"`
	PubCat string `orm:"column(pub_cat)" json:"pub_cat"`

	AllCount   int `orm:"column(all_count)" json:"all_count"`
	LessCount  int `orm:"column(less_count)" json:"less_count"`
	Status     int `orm:"column(status)" json:"status"`

	CreateTime time.Time `orm:"column(create_time)" json:"create_time"`
	EndTime    time.Time `orm:"column(end_time);null" json:"end_time"`
}

func GetNeedPubAutoTask() (AutoTask, error) {
	pub := AutoTask{}
	o := orm.NewOrm()
	err := o.Begin()
	if err != nil {
		o.Rollback()
		return pub, err
	}

	err = o.Raw("select * from auto_task where status = ? limit 1 for update", 0).QueryRow(&pub)
	if err != nil {
		o.Rollback()
		return pub, err
	}

	pub.Status = 1
	_, err = o.Update(&pub, "status")
	if err != nil {
		o.Rollback()
		return pub, err
	}

	err = o.Commit()
	if err != nil {
		o.Rollback()
		return pub, err
	}

	return pub, nil
}

func AddAutoTask(autoTask *AutoTask) error{
	orm := orm.NewOrm()
	_,err := orm.Insert(autoTask)
	return err
}


func UpdateAutoTask(autoTask *AutoTask) error{
	orm := orm.NewOrm()
	_,err := orm.Update(autoTask)
	return err
}

func ListAutoTask()  ([]AutoTask,error){
	o := orm.NewOrm()
	autoTasks :=  []AutoTask{}
	s := "select * from auto_task"
	_,err := o.Raw(s).QueryRows(&autoTasks)
	return autoTasks,err
}

func ListAutoTaskByPage(page int,size int)  (autoTasks []AutoTask,count int,err error){
	o := orm.NewOrm()
	s := "select * from auto_task order by id desc limit ?,?"
	_,err = o.Raw(s, (page-1)*size, size).QueryRows(&autoTasks)
	if err != nil{
		return nil,0,err
	}
	sql := `select count(*) from auto_task`
	err = o.Raw(sql).QueryRow(&count)
	if err != nil{
		return nil,0,err
	}
	return autoTasks,count,err
}

func DeleteAutoTask(id int) error{
	o := orm.NewOrm()
	if _,err := o.Delete(&AutoTask{Id:id},"id");err != nil{
		return err
	}
	return nil
}

func GetAutoTaskById(id int) (*AutoTask,error){
	o := orm.NewOrm()
	autoTask := &AutoTask{Id:id}
	if err := o.Read(autoTask,"id");err != nil{
		return nil,err
	}
	return autoTask,nil
}