package main

import (
	"publish/common/baiduCheck"
	"github.com/astaxie/beego"
	"fmt"
)

func main() {
	urls := []string{
		"http://www.huabian.com/shenghuo/20180413/225126.html",
		"http://www.huabian.com/shenghuo/20180413/225124.html",
		"http://www.huabian.com/shenghuo/20180413/225123.html",
		"http://www.huabian.com/shenghuo/20180413/225019.html",
		"http://www.huabian.com/shenghuo/20180413/225018.html",
		"http://www.huabian.com/shenghuo/20180412/224915.html",
		"http://www.huabian.com/shenghuo/20180412/224875.html",
		"http://www.huabian.com/shenghuo/20180411/224608.html",
		"http://www.huabian.com/shenghuo/20180409/224054.html",
		"http://www.huabian.com/shenghuo/20180409/224053.html",
		"http://www.huabian.com/mingxing/20180413/225214.html",
		"http://www.huabian.com/mingxing/20180413/225212.html",
		"http://www.huabian.com/mingxing/20180413/225209.html",
		"http://www.huabian.com/mingxing/20180413/225170.html",
		"http://www.huabian.com/mingxing/20180413/225169.html",
	}

	pass := []string{}

	for _,url := range urls{
		if baiduCheck.DefaultCheck.Check("",url){
			pass = append(pass,url)
		}
	}

	for _,url := range pass{
		beego.Info(fmt.Sprintf(`"%s",`,url))
	}
}

