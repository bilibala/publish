package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/astaxie/beego"
	"publish/app/pub-agent/worker"
)

func main() {
	whiteWords := beego.AppConfig.Strings("white_words")
	if len(whiteWords) <= 10{
		panic("没有配置敏感词汇库")
	}


	remote := beego.AppConfig.String("remote_master")
	work := worker.StartWorker(remote)
	beego.Info("Agent 启动")

	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch

	work.Stop()

}
