package account

import (
	"testing"
	"publish/pub-lib"
	"github.com/astaxie/beego"
)

func TestAccount_Cookie(t *testing.T) {
	user := Dayu_Pan
	p := pub_lib.NewPlatform(user.Plat,user.Cookie,user.PlatParams)
	if err := p.CheckCookie();err != nil{
		beego.Error(err,user.Plat,user.Username)
	}
}


func TestAccount_AllCookie(t *testing.T) {
	for _,user := range Accounts{
		p := pub_lib.NewPlatform(user.Plat,user.Cookie,user.PlatParams)
		if err := p.CheckCookie();err != nil{
			beego.Error(user.Plat,err.Error(),user.Username)
		}else{
			beego.Info(user.Plat," Cookie 【有用】：",user.Username)
		}
	}
}
